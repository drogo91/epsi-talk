# Angular

*Un Framework Typescript*

## Introduction

Angular est un framework permettant aisément de faire des applications web. Ce framework utilise le langage Typescript depuis sa version 2, en effet dans sa première version, le langage associé fut le javascript. Actuellement, la dernière mise à jour 5.2.1 apporte une meilleure gestion des Regex et une optimisation de lecture d’erreur dans un cas précis. 

Angular arrive sur sa 9èmeversion (septembre / octobre 2019 ), cette version comportera un nouveau moteurde rendu ainsi qu’une optimisation de la réduction de la taille des applications, améliorant également la productivité de développement des applis web. Néanmoins, ici, nous allons parler d’Angular dans son ensemble avec son fonctionnement. Tout d’abord, nous allons nous concentrer sur la composition du framework, puis sur ses spécificités et pour finir nous nous concentrerons sur des exemples afin d’avoir des bases saines.

## Composition 

Angular se définit par sa modularité et sa complexité mais également pour ses facilités en termes de réalisation de code. En effet, on retrouve le système de *data binding, de components, de directives, d’injectors et de modules*. Tous ces mots peuvent paraitre barbare, cependant, on va revenir dessus pour bien comprendre.

### Qu’est-ce que le data binding ? et à quoi sert-il avec Angular ?

Le data binding permet de synchroniser la vue et le modèle (je vous renvoi vers la documentation sur le MVC pour mieux comprendre le concept de vue et de modèle).  
Le data binding est un élément essentiel pour les framework tel qu’Angular utilisant le système d’application sur une seule page. Effectivement, en restant sur la meme vue et en changeant les données grâce au data binding, cela va synchroniser le template (DOM = Document object model) et le component.

### Qu’est-ce qu’un component ? A quoi sert-il également ?

Les components sont les éléments de base de l'interface utilisateur d'une application. Une application Angular contient un arbre de component. Un component contrôle des vues. Il permet de gérer l’affichage selon ce qu’il souhaite afficher, comme par exemple une liste d’objets, ou une recette de cuisine.

### Qu’est-ce qu’une directive ? A quoi sert-elle ?

Une directive est un moyen simple et rapide d’ajouter un élément à différents endroits de l’application, on identifie les directives natives avec le préfixe « ```ng``` ». Les directives non natives sont nommées sous le format «```namespaceDirectiveName```» On peut comparer cela à un schéma que l’on reproduit où l’on veut sur l’interface. Angular possède des directives de bases très utiles tel que le «```ngController```» permettant d’attacher un contrôleur à la vue, le «```ngApp```»  surement la plus importante) qui permet d’initialiser l’application. Un module utilisé pour avoir une application possèdera forcément un «```ngApp```» sans son ```<body\>```.

```js
var myModule = angular.module(...);
myModule.directive('namespaceDirectiveName', function factory(injectables) { 

    var directiveDefinitionObject = {
        restrict: string,
        priority: number,
        template: string,
        templateUrl: string,
        replace: bool,
        transclude: bool,
        scope: bool or object,
        controller: function controllerConstructor($scope,
                                                          $element,
                                                          $attrs,
                                                          $transclude),

        require: string,
        link: function postLink(scope, iElement, iAttrs) { ... }, 
        compile: function compile(tElement, tAttrs, transclude) {
            return {
                pre: function preLink(scope, iElement, iAttrs, controller) { ... }, 
                post: function postLink(scope, iElement, iAttrs, controller) { ... }
            }
        }
    };
    return directiveDefinitionObject; 
});
```

On peut voir les différentes fonctionnalités et utilités d’une directive ici, avec par exemple la propriété « restrict » qui permet de choisir la forme de la déclaration de la directive dans le code html, il y a également la propriété « templates » (souvent utilisée) qui permet de remplacer un élément par un template au lieu de faire du copié collé (DRY). Après, tout cela n’est que la surface et une petite explication afin de comprendre le fonctionnement d’angular. Ce framework propose beaucoup plus de choses à découvrir !

### Qu’est-ce qu’une injection de dépendance ? Son utilité ?

L’injection de dépendances permet d’appeler les dépendances en utilisant des « providers ». Le provider dit à l’injector comment créer le service. Ils vont uniquement appeler les dépendances dont vous aurez besoin lors de l’exécution. On récupère ensuite l’objet provenant de la dépendance. On gagne ainsi en performance puisqu’on appelle uniquement les dépendances dont nous avons besoin (dans notre cas, des components, des services, etc…). Ce qui est un énorme avantage dans l’utilisation du framework. Les injections se font par le biais d’une API qui diffère selon la version d’utilisation d’Angular, soit ReflectiveInjectorAPI soit StaticInjector.

### Qu’est-ce qu’un module ? et… vous connaissez le refrain ?

Comme cela a été expliqué au tout début, les applications Angular sont modulaires. C’est-à-dire qu’elle est composée de modules. Une application comporte au minimum un module, ce dernier s’appelle par convention AppModule (le root module), les autres modules s’y attachant, correspondant à des fonctionnalités métier, des méthodes techniques ou tout ce qui peut améliorer et aérer le code, font de l’application un système modulaire. Cependant, les modules peuvent être différent. Il existe deux normes distinctes, l’ES6 et celle d’Angular, qui peuvent s’utiliser pour les modules, mais leurs fonctions sont différentes. Un module sous ES6 est finalement un fichier qui exporte des données qui seront utilisées après. Tandis qu’un module Angular regroupe des components Angular en une singularité, apportant une cohésion au sein de l’application.

Pour identifier un module angular, celui-ci se caractérise par le fait qu’il porte ```@NgModule```, cette fonction modifie une classe Js pour y ajouter des informations. Comme ça Angular connaitra le rôle de la classe et de gérer les propriétés. Tout comme avec les directives, ```@NgModule``` comporte un objet contenant des propriétés spécifiques telles que : declaration ; exports ; imports ; providers ; bootstrap ; schemas ; entryComponent.

On peut illustrer cela en commençant une application de base avec Angular-CLI (Mais c’est quoi Angular-CLI ? On y reviendra plus tard). Tout d’abord il va falloir installer les dépendances de base ainsi que le squelette du projet pour qu’il puisse fonctionner.

On va suivre à la lettre les commandes suivantes :  
*(exécutez votre IDE / terminal en tant qu’administrateur)*  

 ```bash
npm i -g \@angular/cli
ng new NomDeLappli 
cd NomDeLappli  
ng serve
```

Vous aurez un projet de base directement, et oui ! Pour regarder ce que cela donne sur un navigateur, il faudra aller à l’adresse : <http://localhost:4200/>

Cependant, un projet vide ne sert à pas à grand-chose, il va falloir rajouter des components et des modules pour étoffer cette application. Je vous laisse fouiller dans les tréfonds de la documentation, car il y a beaucoup de choses intéressantes !

### Mais c’est quoi Angular-CLI alors ?

CLI pour command line interface, Angular-CLI est un outil qui permet d’initialiser, développer, builder et maintenir son application Angular. Tout se passe avec des lignes de commandes à partir d’un terminal ! Néanmoins, les versions d’Angular-CLI sont nombreuses, et donc les commandes
également. Il faudra se référer sur la documentation de la version que l’on pourra voir dans le package-lock.json crée lors du début d’un projet avec cet outil.
