# Le schéma MVC
*Modèle Vue Contrôleur*

## Introduction :

Le **schéma MVC** est une organisation du code découpant ce dernier en 3 parties distinctes. Celles-ci sont : Le **modèle**, qui correspond aux objets, ici des objets Java ; La **vue** est l’interface qu’à l’utilisateur lorsqu’il utilise l’application ; Le **contrôleur** est l’objet qui relie les deux autres parties dynamiquement lorsque l’utilisateur fait une action, c’est-à-dire que lorsque l’utilisateur modifie un champ dans une boite de texte sur l’interface, l’objet comportant les informations de la texte box comme attribut se verra modifier ses données selon l’insertion/modification effectuée. Ces parties seront réexpliquées ultérieurement.

## Schéma du modèle :

| ![Schéma-MVC.jpg](media/132e9017dc5792a7aa92813b8a51b603.png) | 
|:--:| 
| *Schéma fonctionnel du modèle MVC* |

| ![Architecture.jpg](media/36b4626c65c10049ad81afcb08358e00.png) | 
|:--:| 
| *Architecture du modèle* |

On peut voir que l’architecture du modèle comporte des nominations différentes de celle du schéma. En effet, les DAOs (Data access object) correspondent au modèle car elles contiennent les informations de l’objet de la base de données. De plus, les DTOs (Data transfert object) correspondent aux données converties (dans le fichier User_converter) pour qu’elles soient accessible pas la vue (notons que dans certains cas cette transition n’est pas nécessaire). Sinon le Controller et la View sont les termes anglophones pour un contrôleur et une vue.

## Définitions / fonctions :

### Le Modèle :

Le modèle est un objet, il contient les informations qui sont utilisées dans la vue. Généralement, les objets sont reliés à une base de données, ils ont donc leurs attributs similaires aux données de la base.

### La Vue :

Comme cela a été expliqué dans l’introduction, la vue correspond à l’interface qu’à l’utilisateur et avec laquelle il peut interagir, cette partie correspond principalement au front.

### Le Contrôleur :

Le contrôleur lie les vues avec les données. Lorsqu’une vue est modifiée, le contrôleur met à jour les données dans la base de données et actualise ensuite la vue avec les nouvelles données. Ce scénario correspond principalement à une update de donnée. On en déduit que le contrôleur gère le modèle en l’initialisant pour la vue.
