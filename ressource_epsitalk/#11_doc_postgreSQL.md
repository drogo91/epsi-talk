# Compte Rendu - PostgreSQL

## Présentation

PostgreSQL est un logiciel libre de gestion de base de données relationnelle et objet. Cela signifie que l’information est transmise sous la forme d’objet.

## Installation

### Installation du serveur PostgreSQL

Mise en place sous Windows avec la version 11.1 à partir de l’installeur trouvé sur le site PostgreSQL.

### Installation de l’interface d’administration

Utilisation de l’outil pgAdmin avec la version 4 qui passe par une interface web. L’installation se fait à partir de l’installeur trouvé sur le site pgAdmin avec la version 3.6 de Windows.

## Mise en place à partir de pgAdmin

Dans un premier temps il faut établir la connexion avec le serveur PostgreSQL. Dans ce cas précis, notre serveur se trouve en localhost et utilise le port par défaut qui est le 5432. Il n’y a plus qu’à saisir le nom d’utilisateur et le mot de passe. On peut également mettre en place une connexion via SSH ou SSL pour plus de sécurité.

Une fois la connexion effectuée on peut créer une base de données et l’administrer. Lors de la création de la base de données on peut ajouter une template qui nous permettra de calquer sa nouvelle base de données sur une existante.

A partir de là, on peut accéder aux fonctionnalités principales.

| ![AddTable.jpg](media/015ad4b8575adb7563806215a29dddc0.png) | 
|:--:| 
| ![AddTable2.jpg](media/14663784814b760cb4746740b4f4546f.png) |
| *Ajouter une table* |

| ![AddContrainte.jpg](media/4298437a2d1bb38e683116e26aa29926.png) | 
|:--:| 
| ![AddContrainte2.jpg](media/89fdb8bfacb302223af0aea3d4e39d3b.png) |
| *Ajouter des contraintes* |

| ![NewQuery.jpg](media/732169d4c4964fc86df999d68d74d8a3.png) | 
|:--:| 
| *Faire une requête SQL* |

## Utilisation des dump

Les dump servent à sauvegarder une base de donnée sous un fichier.  Ce dernier peut avoir plusieurs format dont les .dump et les .sql. On va détailler la méthode ligne de commande Windows) et en passant par l'interface pgAdmin.
La procédure est la suivante : On sauvegarde une base de donnée test sous un fichier sql avant de supprimer cette dernière. Puis on crée une nouvelle base de donnée qu'on initialise avec la sauvegarde précédemment faite. 

### Ligne de commande

| ![cmd.jpg](media/cmd-postgre2.PNG) | 
|:--:| 
| ![cmd2.jpg](media/cmd-postgre1.PNG) |
| *Utilisation des dump en ligne de commande (cmd)* |

### pgAdmin 

| ![pgAdmin.jpg](media/pgAdmin-postgre.PNG) | 
|:--:| 
| ![pgAdmin2.jpg](media/pgAdmin-postgre2.PNG) |
| ![pgAdmin3.jpg](media/pgAdmin-postgre3.PNG) |
| ![pgAdmin4.jpg](media/pgAdmin-postgre4.PNG) |
| *Utilisation des dump via pgAdmin 4* |

## Utilisation d’une ORM

Comme dit précédemment, Postgre est un SGBDRO (Système de base de données relationnel orienté objet), c’est-à-dire qu’il aborde une logique d’administration proche de celle de la  programmation objet.

D’un point de vu programmation objet, on peut avoir recours à un ORM (Object-Relationnal mapping) fin de manipuler la donnée à travers un objet et de simplifier son utilisation. Cela représente une couche séparant l’applicatif de la base de données et permet donc l’interaction entre les deux. Les ORM sont des librairies comme par exemple Doctrine pour Symfony.
