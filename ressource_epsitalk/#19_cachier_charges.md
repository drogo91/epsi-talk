
# Cahier des charges

Association Epsi Talk

## Contexte :

L&#39;association Epsi Talk a besoin d&#39;une vitrine publique permettant de promouvoir l&#39;activité de l&#39;organisation. 
Il souhaite donc un site web public permettant d&#39;accéder aux évènements futurs et passés ainsi que les détails liés. 
De plus, ce site devra contenir des articles de veilles technologiques. L&#39;association souhaite également pouvoir gérer plus efficacement et 
rapidement l&#39;organisation et la communication de leurs conférences via une nouvelle interface web qui sera cette fois-ci privée.

## Périmètre :

Leur objectif est d&#39;obtenir de la visibilité auprès d&#39;entreprises, de professionnels et d&#39;étudiants. Le projet implique les entreprises ayant intervenues 
tout comme celles qui interviendront dans le futur, il implique également les personnes faisant parti de l&#39;association pour la partie du site privée. 
Le projet est limité aux contacts d&#39;Epsi Talk et aux visiteurs.

## Fonctionnement :

Le site web public devra donc contenir les conférences à venir mais également l&#39;historique des toutes les conférences organisées par l&#39;association. 
Ils devront également pouvoir accéder à un résumé, un titre et une image qui permettront de définir la conférence. Les utilisateurs pourront également s&#39;inscrire à 
un événement. De plus, il y aura la possibilité par le biais de cette interface de devenir sponsor qui amènera donc à une fiche de contact qui sera envoyée aux personnes 
responsables. Enfin, le site pourra également contenir des articles de veille technologique ajoutés par les utilisateurs agréés.

 Le site web privé devra permettre de créer, supprimer ou modifier un évènement. Ces évènements peuvent être de deux natures, une conférence ou un salon. 
 Dans le premier cas elle ne sera liée qu&#39;à une seule salle, sinon le salon sera lié à plusieurs salles et un salon pourra contenir ou non plusieurs conférences. 
 Une conférence est liée à un intervenant lui-même lié à une organisation. Ces intervenants peuvent être sponsor et dans ce cas, on devra connaître les montants perçus. 
 Pour une conférence, on devra enregistrer une checklist comprenant tous les objets qu&#39;on aura besoin pour cette dernière en plus d&#39;un créneau horaire. 
 Toutes les dépenses d&#39;un événement devront être enregistrées et avoir un motif ainsi que la possibilité de lier un document correspondant à un reçu.

