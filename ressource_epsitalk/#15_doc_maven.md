# Compte-Rendu Maven

## Présentation

Maven est un outil de construction de projet Java. Il permet de gérer de nombreuses tâches comme la compilation, la gestion des dépendances d’un projet ou la génération de documentation relatif au projet. A noter que ses fonctionnalités sont extensibles par l’utilisation d’un système de plugins.

## Installation et mise en place sous Windows

Le seul prérequis est d’avoir un JDK (Java Développement Kit) téléchargeable sur le site de Java.

Après avoir installé le dossier compressé sur le site Maven, on peut le décompresser à l’endroit de son choix. La particularité de Maven sous Windows étant la nécessité d’ajouter manuellement les variables d’environnement utilisé par Maven pour son bon fonctionnement.

Il faudra donc en ajouter deux : ```MAVEN_HOME``` et ```JAVA_HOME``` correspondant respectivement à l’emplacement de notre dossier Maven et celui de notre java. Une fois cela fait on pourra lancer la commande ```mvn -v``` pour vérifier de son bon fonctionnement qui devrait retourner une note sur la version actuelle de Maven.

Pour plus de détails : *https://www.mkyong.com/maven/how-to-install-maven-in-windows/*

## Principes de Maven

| ![archi.jpg](media/f49a112ca2939f05cb0051d0afb7c71e.gif) | 
|:--:| 
| *Architecure d'une projet Maven* |

Maven s’appuie sur une architecture de fichier standardisé qui permet la meilleure compréhension d’un projet par quelqu’un ayant déjà une expérience sur Maven. La structure standard d’un projet peut être défini comme tel :

Maven s’appuie sur quelques principes clés qui sont les suivants : le fichier ‘POM’, le cycle de vie, les dépendances, les plugins et l’artefact.

Le fichier POM (Project Object Model) contient la description du projet. En effet il contient les dépendances, la version, le nom du projet ou encore son id.

Le cycle de vie sont les différentes phases du cycle qu’une application sera mené à rencontrer. On peut déterminer les principales parties du cycle de vie :

-   validate 

    -   vérifie que le POM est correct et complet

-   compile 

    -   compile les sources du projet qui se trouvent dans ```src/main/java``` et place les fichiers compilés (.class) dans *target*

-   test 

    -   compile les tests qui se trouvent dans *src/test/java* et lance l'intégralité des tests, les tests compilés sont placés dans ```target```

-   package 

    -   génère un package de l'application sous format ```.jar```

-   install 

    -   utilise le package généré à la phase package et l'ajoute au dépôt local. À utiliser pour une bibliothèque : elle peut dès lors être utilisée comme dépendance pour un autre projet.

-   site

    -   créer une interface web informative sur le contenu et la définition du projet.

-   clean 

    -   détruit le répertoire ```target```

Les dépendances de Maven sont les différentes librairies qui seront implémentés pour le projet, ces dernières inclus une notion de portée qui correspond à une partie du cycle de vie. Par exemple, une librairie déployée uniquement pour le test unitaire ne sera pas retenu lors de l’export du projet.

Les plugins ne sont par à confondre avec les dépendances, ces derniers servant directement le bon fonctionnement de Maven là où les dépendances ne sont que des classes ajoutées au code source.

L’artefact est le résultat de l’export d’un projet Maven, usuellement sous format ‘.jar’. Celui-ci peut alors être déployé dans d’autres projet Maven.

*Sources :*
https://dcabasson.developpez.com/articles/java/maven/introduction-maven2/
