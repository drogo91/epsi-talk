# Documentation git

## Bonnes pratiques

Une branche doit être créer par fonctionnalité, elle contiendra donc les différents commit lier à cette branche et une fois celle-ci fini, elle doit être soumise à une merge request directement depuis GitLab.

Pour la nomenclature, elle est précisée dans le wiki du GitLab.

## Commandes

```bash
git clone git@gitlab.com:drogo91/yourproject.git
    Initialiser un dépôt local à partir d’un dépôt distant en ssh

git checkout NomBranche 
    Permet de changer de branche*

git checkout -b NomBranche
     Permet de créer une branche et de s’y placer

git status
    Permet de savoir toutes les différences depuis le dernier commit push

git add NomFichier
    Permet d’ajouter des fichiers à un commit

git commit 
    Permet de créer un commit

git push NomBranche
    Permet de push le commit vers la branche voulue

git rebase NomBranche 
    Permet rebaser le dépôt local sur une branche
```
## Faire une marge request

| ![Merge.jpg](media/2831a2d18a54f930722c214cc1f36a1d.png) | 
|:--:| 
| ![Merge2.jpg](media/6d6fdb7caa3f95107cee421adcbbe79b.png) |
| ![Merge3.jpg](media/f047894292bc8149d2c93a0429e3c472.png) |
| *Marche pour faire une merge request* |