PGDMP                         w            epsitalk    11.0    11.1 �               0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false                       0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false                       0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false                       1262    24576    epsitalk    DATABASE     �   CREATE DATABASE epsitalk WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'French_France.1252' LC_CTYPE = 'French_France.1252';
    DROP DATABASE epsitalk;
             postgres    false            �            1259    24577    article    TABLE     �   CREATE TABLE public.article (
    id_article integer NOT NULL,
    id_event integer NOT NULL,
    title character varying(50) NOT NULL,
    containt character varying(10000) NOT NULL
);
    DROP TABLE public.article;
       public         postgres    false            �            1259    24583    article_id_article_seq    SEQUENCE     �   CREATE SEQUENCE public.article_id_article_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.article_id_article_seq;
       public       postgres    false    196                       0    0    article_id_article_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.article_id_article_seq OWNED BY public.article.id_article;
            public       postgres    false    197            �            1259    24585    article_id_event_seq    SEQUENCE     �   CREATE SEQUENCE public.article_id_event_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.article_id_event_seq;
       public       postgres    false    196                       0    0    article_id_event_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.article_id_event_seq OWNED BY public.article.id_event;
            public       postgres    false    198            �            1259    24587    event    TABLE     C  CREATE TABLE public.event (
    id integer NOT NULL,
    "dateStart" date NOT NULL,
    id_speaker integer,
    name character varying(100) NOT NULL,
    description character varying(250) NOT NULL,
    "hourStart" time(4) without time zone NOT NULL,
    "hourEnd" time(4) without time zone NOT NULL,
    "dateEnd" date
);
    DROP TABLE public.event;
       public         postgres    false            �            1259    24590    event-event    TABLE     y   CREATE TABLE public."event-event" (
    "id-event_salon" integer NOT NULL,
    "id-event_conference" integer NOT NULL
);
 !   DROP TABLE public."event-event";
       public         postgres    false            �            1259    24593 #   event-event_id-event_conference_seq    SEQUENCE     �   CREATE SEQUENCE public."event-event_id-event_conference_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 <   DROP SEQUENCE public."event-event_id-event_conference_seq";
       public       postgres    false    200                       0    0 #   event-event_id-event_conference_seq    SEQUENCE OWNED BY     q   ALTER SEQUENCE public."event-event_id-event_conference_seq" OWNED BY public."event-event"."id-event_conference";
            public       postgres    false    201            �            1259    24595    event-event_id-event_salon_seq    SEQUENCE     �   CREATE SEQUENCE public."event-event_id-event_salon_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public."event-event_id-event_salon_seq";
       public       postgres    false    200                       0    0    event-event_id-event_salon_seq    SEQUENCE OWNED BY     g   ALTER SEQUENCE public."event-event_id-event_salon_seq" OWNED BY public."event-event"."id-event_salon";
            public       postgres    false    202            �            1259    24597    event_id_seq    SEQUENCE     �   CREATE SEQUENCE public.event_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.event_id_seq;
       public       postgres    false    199                       0    0    event_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.event_id_seq OWNED BY public.event.id;
            public       postgres    false    203            �            1259    24599    event_id_speaker_seq    SEQUENCE     �   CREATE SEQUENCE public.event_id_speaker_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.event_id_speaker_seq;
       public       postgres    false    199                        0    0    event_id_speaker_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.event_id_speaker_seq OWNED BY public.event.id_speaker;
            public       postgres    false    204            �            1259    24601    expense    TABLE     �   CREATE TABLE public.expense (
    id_expense integer NOT NULL,
    id_event integer NOT NULL,
    name character varying(100) NOT NULL,
    receipt character varying(200)
);
    DROP TABLE public.expense;
       public         postgres    false            �            1259    24604    expenses_id_event_seq    SEQUENCE     �   CREATE SEQUENCE public.expenses_id_event_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.expenses_id_event_seq;
       public       postgres    false    205            !           0    0    expenses_id_event_seq    SEQUENCE OWNED BY     N   ALTER SEQUENCE public.expenses_id_event_seq OWNED BY public.expense.id_event;
            public       postgres    false    206            �            1259    24606    expenses_id_expense_seq    SEQUENCE     �   CREATE SEQUENCE public.expenses_id_expense_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.expenses_id_expense_seq;
       public       postgres    false    205            "           0    0    expenses_id_expense_seq    SEQUENCE OWNED BY     R   ALTER SEQUENCE public.expenses_id_expense_seq OWNED BY public.expense.id_expense;
            public       postgres    false    207            �            1259    24608    mailingList    TABLE     w   CREATE TABLE public."mailingList" (
    "id_mailingList" integer NOT NULL,
    title character varying(60) NOT NULL
);
 !   DROP TABLE public."mailingList";
       public         postgres    false            �            1259    24611    mailingList_id_mailingList_seq    SEQUENCE     �   CREATE SEQUENCE public."mailingList_id_mailingList_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public."mailingList_id_mailingList_seq";
       public       postgres    false    208            #           0    0    mailingList_id_mailingList_seq    SEQUENCE OWNED BY     g   ALTER SEQUENCE public."mailingList_id_mailingList_seq" OWNED BY public."mailingList"."id_mailingList";
            public       postgres    false    209            �            1259    24613    material    TABLE     }   CREATE TABLE public.material (
    id_material integer NOT NULL,
    id_room integer NOT NULL,
    name character varying
);
    DROP TABLE public.material;
       public         postgres    false            �            1259    24619    material-event    TABLE     j   CREATE TABLE public."material-event" (
    id_event integer NOT NULL,
    id_material integer NOT NULL
);
 $   DROP TABLE public."material-event";
       public         postgres    false            �            1259    24622    material-event_id_event_seq    SEQUENCE     �   CREATE SEQUENCE public."material-event_id_event_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public."material-event_id_event_seq";
       public       postgres    false    211            $           0    0    material-event_id_event_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public."material-event_id_event_seq" OWNED BY public."material-event".id_event;
            public       postgres    false    212            �            1259    24624    material-event_id_material_seq    SEQUENCE     �   CREATE SEQUENCE public."material-event_id_material_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public."material-event_id_material_seq";
       public       postgres    false    211            %           0    0    material-event_id_material_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public."material-event_id_material_seq" OWNED BY public."material-event".id_material;
            public       postgres    false    213            �            1259    24626    material_id_material_seq    SEQUENCE     �   CREATE SEQUENCE public.material_id_material_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.material_id_material_seq;
       public       postgres    false    210            &           0    0    material_id_material_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.material_id_material_seq OWNED BY public.material.id_material;
            public       postgres    false    214            �            1259    24628    material_id_room_seq    SEQUENCE     �   CREATE SEQUENCE public.material_id_room_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.material_id_room_seq;
       public       postgres    false    210            '           0    0    material_id_room_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.material_id_room_seq OWNED BY public.material.id_room;
            public       postgres    false    215            �            1259    24630    organization    TABLE     �   CREATE TABLE public.organization (
    id_organization integer NOT NULL,
    sponsor boolean NOT NULL,
    effective integer,
    name character varying,
    mail character varying
);
     DROP TABLE public.organization;
       public         postgres    false            �            1259    24636    organization-mailingList    TABLE     �   CREATE TABLE public."organization-mailingList" (
    id_organization integer NOT NULL,
    "id_mailingList" integer NOT NULL
);
 .   DROP TABLE public."organization-mailingList";
       public         postgres    false            �            1259    24639 +   organization-mailingList_id_mailingList_seq    SEQUENCE     �   CREATE SEQUENCE public."organization-mailingList_id_mailingList_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 D   DROP SEQUENCE public."organization-mailingList_id_mailingList_seq";
       public       postgres    false    217            (           0    0 +   organization-mailingList_id_mailingList_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public."organization-mailingList_id_mailingList_seq" OWNED BY public."organization-mailingList"."id_mailingList";
            public       postgres    false    218            �            1259    24641 ,   organization-mailingList_id_organization_seq    SEQUENCE     �   CREATE SEQUENCE public."organization-mailingList_id_organization_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 E   DROP SEQUENCE public."organization-mailingList_id_organization_seq";
       public       postgres    false    217            )           0    0 ,   organization-mailingList_id_organization_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public."organization-mailingList_id_organization_seq" OWNED BY public."organization-mailingList".id_organization;
            public       postgres    false    219            �            1259    24643     organization_id_organization_seq    SEQUENCE     �   CREATE SEQUENCE public.organization_id_organization_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.organization_id_organization_seq;
       public       postgres    false    216            *           0    0     organization_id_organization_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.organization_id_organization_seq OWNED BY public.organization.id_organization;
            public       postgres    false    220            �            1259    24645    room    TABLE     \   CREATE TABLE public.room (
    id_room integer NOT NULL,
    name character varying(100)
);
    DROP TABLE public.room;
       public         postgres    false            �            1259    24648 
   room-event    TABLE     b   CREATE TABLE public."room-event" (
    id_event integer NOT NULL,
    id_room integer NOT NULL
);
     DROP TABLE public."room-event";
       public         postgres    false            �            1259    24651    room-event_id_event_seq    SEQUENCE     �   CREATE SEQUENCE public."room-event_id_event_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public."room-event_id_event_seq";
       public       postgres    false    222            +           0    0    room-event_id_event_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public."room-event_id_event_seq" OWNED BY public."room-event".id_event;
            public       postgres    false    223            �            1259    24653    room-event_id_room_seq    SEQUENCE     �   CREATE SEQUENCE public."room-event_id_room_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public."room-event_id_room_seq";
       public       postgres    false    222            ,           0    0    room-event_id_room_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public."room-event_id_room_seq" OWNED BY public."room-event".id_room;
            public       postgres    false    224            �            1259    24655    room_id_room_seq    SEQUENCE     �   CREATE SEQUENCE public.room_id_room_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.room_id_room_seq;
       public       postgres    false    221            -           0    0    room_id_room_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.room_id_room_seq OWNED BY public.room.id_room;
            public       postgres    false    225            �            1259    24657    speaker    TABLE     �   CREATE TABLE public.speaker (
    id_speaker integer NOT NULL,
    id_organization integer NOT NULL,
    name character varying,
    surname character varying,
    mail character varying
);
    DROP TABLE public.speaker;
       public         postgres    false            �            1259    24663    speaker-event    TABLE     h   CREATE TABLE public."speaker-event" (
    id_event integer NOT NULL,
    id_speaker integer NOT NULL
);
 #   DROP TABLE public."speaker-event";
       public         postgres    false            �            1259    24666    speaker-event_id_event_seq    SEQUENCE     �   CREATE SEQUENCE public."speaker-event_id_event_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public."speaker-event_id_event_seq";
       public       postgres    false    227            .           0    0    speaker-event_id_event_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public."speaker-event_id_event_seq" OWNED BY public."speaker-event".id_event;
            public       postgres    false    228            �            1259    24668    speaker-event_id_speaker_seq    SEQUENCE     �   CREATE SEQUENCE public."speaker-event_id_speaker_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public."speaker-event_id_speaker_seq";
       public       postgres    false    227            /           0    0    speaker-event_id_speaker_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public."speaker-event_id_speaker_seq" OWNED BY public."speaker-event".id_speaker;
            public       postgres    false    229            �            1259    24670    speaker-mailingList    TABLE     v   CREATE TABLE public."speaker-mailingList" (
    id_speaker integer NOT NULL,
    "id_mailingList" integer NOT NULL
);
 )   DROP TABLE public."speaker-mailingList";
       public         postgres    false            �            1259    24673 &   speaker-mailingList_id_mailingList_seq    SEQUENCE     �   CREATE SEQUENCE public."speaker-mailingList_id_mailingList_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public."speaker-mailingList_id_mailingList_seq";
       public       postgres    false    230            0           0    0 &   speaker-mailingList_id_mailingList_seq    SEQUENCE OWNED BY     w   ALTER SEQUENCE public."speaker-mailingList_id_mailingList_seq" OWNED BY public."speaker-mailingList"."id_mailingList";
            public       postgres    false    231            �            1259    24675 "   speaker-mailingList_id_speaker_seq    SEQUENCE     �   CREATE SEQUENCE public."speaker-mailingList_id_speaker_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE public."speaker-mailingList_id_speaker_seq";
       public       postgres    false    230            1           0    0 "   speaker-mailingList_id_speaker_seq    SEQUENCE OWNED BY     m   ALTER SEQUENCE public."speaker-mailingList_id_speaker_seq" OWNED BY public."speaker-mailingList".id_speaker;
            public       postgres    false    232            �            1259    24677    speaker-users    TABLE     h   CREATE TABLE public."speaker-users" (
    id_users integer NOT NULL,
    id_speaker integer NOT NULL
);
 #   DROP TABLE public."speaker-users";
       public         postgres    false            �            1259    24680    speaker-users_id_speaker_seq    SEQUENCE     �   CREATE SEQUENCE public."speaker-users_id_speaker_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public."speaker-users_id_speaker_seq";
       public       postgres    false    233            2           0    0    speaker-users_id_speaker_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public."speaker-users_id_speaker_seq" OWNED BY public."speaker-users".id_speaker;
            public       postgres    false    234            �            1259    24682    speaker-users_id_users_seq    SEQUENCE     �   CREATE SEQUENCE public."speaker-users_id_users_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public."speaker-users_id_users_seq";
       public       postgres    false    233            3           0    0    speaker-users_id_users_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public."speaker-users_id_users_seq" OWNED BY public."speaker-users".id_users;
            public       postgres    false    235            �            1259    24684    speaker_id_organization_seq    SEQUENCE     �   CREATE SEQUENCE public.speaker_id_organization_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.speaker_id_organization_seq;
       public       postgres    false    226            4           0    0    speaker_id_organization_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.speaker_id_organization_seq OWNED BY public.speaker.id_organization;
            public       postgres    false    236            �            1259    24686    speaker_id_speaker_seq    SEQUENCE     �   CREATE SEQUENCE public.speaker_id_speaker_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.speaker_id_speaker_seq;
       public       postgres    false    226            5           0    0    speaker_id_speaker_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.speaker_id_speaker_seq OWNED BY public.speaker.id_speaker;
            public       postgres    false    237            �            1259    24688    task    TABLE     �   CREATE TABLE public.task (
    id_task integer NOT NULL,
    id_event integer NOT NULL,
    title character varying,
    containt character varying
);
    DROP TABLE public.task;
       public         postgres    false            �            1259    24694    task_id_event_seq    SEQUENCE     �   CREATE SEQUENCE public.task_id_event_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.task_id_event_seq;
       public       postgres    false    238            6           0    0    task_id_event_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.task_id_event_seq OWNED BY public.task.id_event;
            public       postgres    false    239            �            1259    24696    task_id_task_seq    SEQUENCE     �   CREATE SEQUENCE public.task_id_task_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.task_id_task_seq;
       public       postgres    false    238            7           0    0    task_id_task_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.task_id_task_seq OWNED BY public.task.id_task;
            public       postgres    false    240            �            1259    24698    theme    TABLE     Y   CREATE TABLE public.theme (
    id_theme integer NOT NULL,
    name character varying
);
    DROP TABLE public.theme;
       public         postgres    false            �            1259    24704    theme-event    TABLE     d   CREATE TABLE public."theme-event" (
    id_theme integer NOT NULL,
    id_event integer NOT NULL
);
 !   DROP TABLE public."theme-event";
       public         postgres    false            �            1259    24707    theme-event_id_event_seq    SEQUENCE     �   CREATE SEQUENCE public."theme-event_id_event_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public."theme-event_id_event_seq";
       public       postgres    false    242            8           0    0    theme-event_id_event_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public."theme-event_id_event_seq" OWNED BY public."theme-event".id_event;
            public       postgres    false    243            �            1259    24709    theme-event_id_theme_seq    SEQUENCE     �   CREATE SEQUENCE public."theme-event_id_theme_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public."theme-event_id_theme_seq";
       public       postgres    false    242            9           0    0    theme-event_id_theme_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public."theme-event_id_theme_seq" OWNED BY public."theme-event".id_theme;
            public       postgres    false    244            �            1259    24711    themes_id_themes_seq    SEQUENCE     �   CREATE SEQUENCE public.themes_id_themes_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.themes_id_themes_seq;
       public       postgres    false    241            :           0    0    themes_id_themes_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.themes_id_themes_seq OWNED BY public.theme.id_theme;
            public       postgres    false    245            �            1259    24713    user    TABLE     �   CREATE TABLE public."user" (
    id integer NOT NULL,
    mail character varying(200) NOT NULL,
    name character varying(70) NOT NULL,
    surname character varying(70) NOT NULL,
    password character varying(350) NOT NULL
);
    DROP TABLE public."user";
       public         postgres    false            �            1259    24719    users_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public       postgres    false    246            ;           0    0    users_id_seq    SEQUENCE OWNED BY     >   ALTER SEQUENCE public.users_id_seq OWNED BY public."user".id;
            public       postgres    false    247                       2604    24898    article id_article    DEFAULT     x   ALTER TABLE ONLY public.article ALTER COLUMN id_article SET DEFAULT nextval('public.article_id_article_seq'::regclass);
 A   ALTER TABLE public.article ALTER COLUMN id_article DROP DEFAULT;
       public       postgres    false    197    196                       2604    24899    article id_event    DEFAULT     t   ALTER TABLE ONLY public.article ALTER COLUMN id_event SET DEFAULT nextval('public.article_id_event_seq'::regclass);
 ?   ALTER TABLE public.article ALTER COLUMN id_event DROP DEFAULT;
       public       postgres    false    198    196                       2604    24900    event id    DEFAULT     d   ALTER TABLE ONLY public.event ALTER COLUMN id SET DEFAULT nextval('public.event_id_seq'::regclass);
 7   ALTER TABLE public.event ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    203    199                       2604    24901    event id_speaker    DEFAULT     t   ALTER TABLE ONLY public.event ALTER COLUMN id_speaker SET DEFAULT nextval('public.event_id_speaker_seq'::regclass);
 ?   ALTER TABLE public.event ALTER COLUMN id_speaker DROP DEFAULT;
       public       postgres    false    204    199                       2604    24902    event-event id-event_salon    DEFAULT     �   ALTER TABLE ONLY public."event-event" ALTER COLUMN "id-event_salon" SET DEFAULT nextval('public."event-event_id-event_salon_seq"'::regclass);
 M   ALTER TABLE public."event-event" ALTER COLUMN "id-event_salon" DROP DEFAULT;
       public       postgres    false    202    200                       2604    24903    event-event id-event_conference    DEFAULT     �   ALTER TABLE ONLY public."event-event" ALTER COLUMN "id-event_conference" SET DEFAULT nextval('public."event-event_id-event_conference_seq"'::regclass);
 R   ALTER TABLE public."event-event" ALTER COLUMN "id-event_conference" DROP DEFAULT;
       public       postgres    false    201    200                       2604    24904    expense id_expense    DEFAULT     y   ALTER TABLE ONLY public.expense ALTER COLUMN id_expense SET DEFAULT nextval('public.expenses_id_expense_seq'::regclass);
 A   ALTER TABLE public.expense ALTER COLUMN id_expense DROP DEFAULT;
       public       postgres    false    207    205                       2604    24905    expense id_event    DEFAULT     u   ALTER TABLE ONLY public.expense ALTER COLUMN id_event SET DEFAULT nextval('public.expenses_id_event_seq'::regclass);
 ?   ALTER TABLE public.expense ALTER COLUMN id_event DROP DEFAULT;
       public       postgres    false    206    205                       2604    24906    mailingList id_mailingList    DEFAULT     �   ALTER TABLE ONLY public."mailingList" ALTER COLUMN "id_mailingList" SET DEFAULT nextval('public."mailingList_id_mailingList_seq"'::regclass);
 M   ALTER TABLE public."mailingList" ALTER COLUMN "id_mailingList" DROP DEFAULT;
       public       postgres    false    209    208                       2604    24907    material id_material    DEFAULT     |   ALTER TABLE ONLY public.material ALTER COLUMN id_material SET DEFAULT nextval('public.material_id_material_seq'::regclass);
 C   ALTER TABLE public.material ALTER COLUMN id_material DROP DEFAULT;
       public       postgres    false    214    210                       2604    24908    material id_room    DEFAULT     t   ALTER TABLE ONLY public.material ALTER COLUMN id_room SET DEFAULT nextval('public.material_id_room_seq'::regclass);
 ?   ALTER TABLE public.material ALTER COLUMN id_room DROP DEFAULT;
       public       postgres    false    215    210                       2604    24909    material-event id_event    DEFAULT     �   ALTER TABLE ONLY public."material-event" ALTER COLUMN id_event SET DEFAULT nextval('public."material-event_id_event_seq"'::regclass);
 H   ALTER TABLE public."material-event" ALTER COLUMN id_event DROP DEFAULT;
       public       postgres    false    212    211                       2604    24910    material-event id_material    DEFAULT     �   ALTER TABLE ONLY public."material-event" ALTER COLUMN id_material SET DEFAULT nextval('public."material-event_id_material_seq"'::regclass);
 K   ALTER TABLE public."material-event" ALTER COLUMN id_material DROP DEFAULT;
       public       postgres    false    213    211                       2604    24911    organization id_organization    DEFAULT     �   ALTER TABLE ONLY public.organization ALTER COLUMN id_organization SET DEFAULT nextval('public.organization_id_organization_seq'::regclass);
 K   ALTER TABLE public.organization ALTER COLUMN id_organization DROP DEFAULT;
       public       postgres    false    220    216                       2604    24912 (   organization-mailingList id_organization    DEFAULT     �   ALTER TABLE ONLY public."organization-mailingList" ALTER COLUMN id_organization SET DEFAULT nextval('public."organization-mailingList_id_organization_seq"'::regclass);
 Y   ALTER TABLE public."organization-mailingList" ALTER COLUMN id_organization DROP DEFAULT;
       public       postgres    false    219    217                       2604    24913 '   organization-mailingList id_mailingList    DEFAULT     �   ALTER TABLE ONLY public."organization-mailingList" ALTER COLUMN "id_mailingList" SET DEFAULT nextval('public."organization-mailingList_id_mailingList_seq"'::regclass);
 Z   ALTER TABLE public."organization-mailingList" ALTER COLUMN "id_mailingList" DROP DEFAULT;
       public       postgres    false    218    217                       2604    24914    room id_room    DEFAULT     l   ALTER TABLE ONLY public.room ALTER COLUMN id_room SET DEFAULT nextval('public.room_id_room_seq'::regclass);
 ;   ALTER TABLE public.room ALTER COLUMN id_room DROP DEFAULT;
       public       postgres    false    225    221                       2604    24915    room-event id_event    DEFAULT     ~   ALTER TABLE ONLY public."room-event" ALTER COLUMN id_event SET DEFAULT nextval('public."room-event_id_event_seq"'::regclass);
 D   ALTER TABLE public."room-event" ALTER COLUMN id_event DROP DEFAULT;
       public       postgres    false    223    222                       2604    24916    room-event id_room    DEFAULT     |   ALTER TABLE ONLY public."room-event" ALTER COLUMN id_room SET DEFAULT nextval('public."room-event_id_room_seq"'::regclass);
 C   ALTER TABLE public."room-event" ALTER COLUMN id_room DROP DEFAULT;
       public       postgres    false    224    222                       2604    24917    speaker id_speaker    DEFAULT     x   ALTER TABLE ONLY public.speaker ALTER COLUMN id_speaker SET DEFAULT nextval('public.speaker_id_speaker_seq'::regclass);
 A   ALTER TABLE public.speaker ALTER COLUMN id_speaker DROP DEFAULT;
       public       postgres    false    237    226                        2604    24918    speaker id_organization    DEFAULT     �   ALTER TABLE ONLY public.speaker ALTER COLUMN id_organization SET DEFAULT nextval('public.speaker_id_organization_seq'::regclass);
 F   ALTER TABLE public.speaker ALTER COLUMN id_organization DROP DEFAULT;
       public       postgres    false    236    226            !           2604    24919    speaker-event id_event    DEFAULT     �   ALTER TABLE ONLY public."speaker-event" ALTER COLUMN id_event SET DEFAULT nextval('public."speaker-event_id_event_seq"'::regclass);
 G   ALTER TABLE public."speaker-event" ALTER COLUMN id_event DROP DEFAULT;
       public       postgres    false    228    227            "           2604    24920    speaker-event id_speaker    DEFAULT     �   ALTER TABLE ONLY public."speaker-event" ALTER COLUMN id_speaker SET DEFAULT nextval('public."speaker-event_id_speaker_seq"'::regclass);
 I   ALTER TABLE public."speaker-event" ALTER COLUMN id_speaker DROP DEFAULT;
       public       postgres    false    229    227            #           2604    24921    speaker-mailingList id_speaker    DEFAULT     �   ALTER TABLE ONLY public."speaker-mailingList" ALTER COLUMN id_speaker SET DEFAULT nextval('public."speaker-mailingList_id_speaker_seq"'::regclass);
 O   ALTER TABLE public."speaker-mailingList" ALTER COLUMN id_speaker DROP DEFAULT;
       public       postgres    false    232    230            $           2604    24922 "   speaker-mailingList id_mailingList    DEFAULT     �   ALTER TABLE ONLY public."speaker-mailingList" ALTER COLUMN "id_mailingList" SET DEFAULT nextval('public."speaker-mailingList_id_mailingList_seq"'::regclass);
 U   ALTER TABLE public."speaker-mailingList" ALTER COLUMN "id_mailingList" DROP DEFAULT;
       public       postgres    false    231    230            %           2604    24923    speaker-users id_users    DEFAULT     �   ALTER TABLE ONLY public."speaker-users" ALTER COLUMN id_users SET DEFAULT nextval('public."speaker-users_id_users_seq"'::regclass);
 G   ALTER TABLE public."speaker-users" ALTER COLUMN id_users DROP DEFAULT;
       public       postgres    false    235    233            &           2604    24924    speaker-users id_speaker    DEFAULT     �   ALTER TABLE ONLY public."speaker-users" ALTER COLUMN id_speaker SET DEFAULT nextval('public."speaker-users_id_speaker_seq"'::regclass);
 I   ALTER TABLE public."speaker-users" ALTER COLUMN id_speaker DROP DEFAULT;
       public       postgres    false    234    233            '           2604    24925    task id_task    DEFAULT     l   ALTER TABLE ONLY public.task ALTER COLUMN id_task SET DEFAULT nextval('public.task_id_task_seq'::regclass);
 ;   ALTER TABLE public.task ALTER COLUMN id_task DROP DEFAULT;
       public       postgres    false    240    238            (           2604    24926    task id_event    DEFAULT     n   ALTER TABLE ONLY public.task ALTER COLUMN id_event SET DEFAULT nextval('public.task_id_event_seq'::regclass);
 <   ALTER TABLE public.task ALTER COLUMN id_event DROP DEFAULT;
       public       postgres    false    239    238            )           2604    24927    theme id_theme    DEFAULT     r   ALTER TABLE ONLY public.theme ALTER COLUMN id_theme SET DEFAULT nextval('public.themes_id_themes_seq'::regclass);
 =   ALTER TABLE public.theme ALTER COLUMN id_theme DROP DEFAULT;
       public       postgres    false    245    241            *           2604    24928    theme-event id_theme    DEFAULT     �   ALTER TABLE ONLY public."theme-event" ALTER COLUMN id_theme SET DEFAULT nextval('public."theme-event_id_theme_seq"'::regclass);
 E   ALTER TABLE public."theme-event" ALTER COLUMN id_theme DROP DEFAULT;
       public       postgres    false    244    242            +           2604    24929    theme-event id_event    DEFAULT     �   ALTER TABLE ONLY public."theme-event" ALTER COLUMN id_event SET DEFAULT nextval('public."theme-event_id_event_seq"'::regclass);
 E   ALTER TABLE public."theme-event" ALTER COLUMN id_event DROP DEFAULT;
       public       postgres    false    243    242            ,           2604    24930    user id    DEFAULT     e   ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 8   ALTER TABLE public."user" ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    247    246            �          0    24577    article 
   TABLE DATA               H   COPY public.article (id_article, id_event, title, containt) FROM stdin;
    public       postgres    false    196   '�       �          0    24587    event 
   TABLE DATA               r   COPY public.event (id, "dateStart", id_speaker, name, description, "hourStart", "hourEnd", "dateEnd") FROM stdin;
    public       postgres    false    199   i�       �          0    24590    event-event 
   TABLE DATA               P   COPY public."event-event" ("id-event_salon", "id-event_conference") FROM stdin;
    public       postgres    false    200   �       �          0    24601    expense 
   TABLE DATA               F   COPY public.expense (id_expense, id_event, name, receipt) FROM stdin;
    public       postgres    false    205   ;�       �          0    24608    mailingList 
   TABLE DATA               @   COPY public."mailingList" ("id_mailingList", title) FROM stdin;
    public       postgres    false    208   s�       �          0    24613    material 
   TABLE DATA               >   COPY public.material (id_material, id_room, name) FROM stdin;
    public       postgres    false    210   ��       �          0    24619    material-event 
   TABLE DATA               A   COPY public."material-event" (id_event, id_material) FROM stdin;
    public       postgres    false    211   ��       �          0    24630    organization 
   TABLE DATA               W   COPY public.organization (id_organization, sponsor, effective, name, mail) FROM stdin;
    public       postgres    false    216   ��       �          0    24636    organization-mailingList 
   TABLE DATA               W   COPY public."organization-mailingList" (id_organization, "id_mailingList") FROM stdin;
    public       postgres    false    217   S�       �          0    24645    room 
   TABLE DATA               -   COPY public.room (id_room, name) FROM stdin;
    public       postgres    false    221   y�       �          0    24648 
   room-event 
   TABLE DATA               9   COPY public."room-event" (id_event, id_room) FROM stdin;
    public       postgres    false    222   ��       �          0    24657    speaker 
   TABLE DATA               S   COPY public.speaker (id_speaker, id_organization, name, surname, mail) FROM stdin;
    public       postgres    false    226   ��                  0    24663    speaker-event 
   TABLE DATA               ?   COPY public."speaker-event" (id_event, id_speaker) FROM stdin;
    public       postgres    false    227   -�                 0    24670    speaker-mailingList 
   TABLE DATA               M   COPY public."speaker-mailingList" (id_speaker, "id_mailingList") FROM stdin;
    public       postgres    false    230   J�                 0    24677    speaker-users 
   TABLE DATA               ?   COPY public."speaker-users" (id_users, id_speaker) FROM stdin;
    public       postgres    false    233   p�                 0    24688    task 
   TABLE DATA               B   COPY public.task (id_task, id_event, title, containt) FROM stdin;
    public       postgres    false    238   ��                 0    24698    theme 
   TABLE DATA               /   COPY public.theme (id_theme, name) FROM stdin;
    public       postgres    false    241   ��                 0    24704    theme-event 
   TABLE DATA               ;   COPY public."theme-event" (id_theme, id_event) FROM stdin;
    public       postgres    false    242   *                 0    24713    user 
   TABLE DATA               C   COPY public."user" (id, mail, name, surname, password) FROM stdin;
    public       postgres    false    246   T       <           0    0    article_id_article_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.article_id_article_seq', 2, true);
            public       postgres    false    197            =           0    0    article_id_event_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.article_id_event_seq', 1, false);
            public       postgres    false    198            >           0    0 #   event-event_id-event_conference_seq    SEQUENCE SET     T   SELECT pg_catalog.setval('public."event-event_id-event_conference_seq"', 1, false);
            public       postgres    false    201            ?           0    0    event-event_id-event_salon_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public."event-event_id-event_salon_seq"', 1, false);
            public       postgres    false    202            @           0    0    event_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.event_id_seq', 5, true);
            public       postgres    false    203            A           0    0    event_id_speaker_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.event_id_speaker_seq', 1, true);
            public       postgres    false    204            B           0    0    expenses_id_event_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.expenses_id_event_seq', 1, false);
            public       postgres    false    206            C           0    0    expenses_id_expense_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.expenses_id_expense_seq', 1, true);
            public       postgres    false    207            D           0    0    mailingList_id_mailingList_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public."mailingList_id_mailingList_seq"', 1, true);
            public       postgres    false    209            E           0    0    material-event_id_event_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public."material-event_id_event_seq"', 1, false);
            public       postgres    false    212            F           0    0    material-event_id_material_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public."material-event_id_material_seq"', 1, false);
            public       postgres    false    213            G           0    0    material_id_material_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.material_id_material_seq', 2, true);
            public       postgres    false    214            H           0    0    material_id_room_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.material_id_room_seq', 1, false);
            public       postgres    false    215            I           0    0 +   organization-mailingList_id_mailingList_seq    SEQUENCE SET     \   SELECT pg_catalog.setval('public."organization-mailingList_id_mailingList_seq"', 1, false);
            public       postgres    false    218            J           0    0 ,   organization-mailingList_id_organization_seq    SEQUENCE SET     ]   SELECT pg_catalog.setval('public."organization-mailingList_id_organization_seq"', 1, false);
            public       postgres    false    219            K           0    0     organization_id_organization_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.organization_id_organization_seq', 2, true);
            public       postgres    false    220            L           0    0    room-event_id_event_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public."room-event_id_event_seq"', 1, false);
            public       postgres    false    223            M           0    0    room-event_id_room_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public."room-event_id_room_seq"', 1, false);
            public       postgres    false    224            N           0    0    room_id_room_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.room_id_room_seq', 2, true);
            public       postgres    false    225            O           0    0    speaker-event_id_event_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public."speaker-event_id_event_seq"', 1, false);
            public       postgres    false    228            P           0    0    speaker-event_id_speaker_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public."speaker-event_id_speaker_seq"', 1, false);
            public       postgres    false    229            Q           0    0 &   speaker-mailingList_id_mailingList_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public."speaker-mailingList_id_mailingList_seq"', 1, false);
            public       postgres    false    231            R           0    0 "   speaker-mailingList_id_speaker_seq    SEQUENCE SET     S   SELECT pg_catalog.setval('public."speaker-mailingList_id_speaker_seq"', 1, false);
            public       postgres    false    232            S           0    0    speaker-users_id_speaker_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public."speaker-users_id_speaker_seq"', 1, false);
            public       postgres    false    234            T           0    0    speaker-users_id_users_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public."speaker-users_id_users_seq"', 1, false);
            public       postgres    false    235            U           0    0    speaker_id_organization_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.speaker_id_organization_seq', 1, false);
            public       postgres    false    236            V           0    0    speaker_id_speaker_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.speaker_id_speaker_seq', 5, true);
            public       postgres    false    237            W           0    0    task_id_event_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.task_id_event_seq', 1, false);
            public       postgres    false    239            X           0    0    task_id_task_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.task_id_task_seq', 3, true);
            public       postgres    false    240            Y           0    0    theme-event_id_event_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public."theme-event_id_event_seq"', 1, false);
            public       postgres    false    243            Z           0    0    theme-event_id_theme_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public."theme-event_id_theme_seq"', 1, false);
            public       postgres    false    244            [           0    0    themes_id_themes_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.themes_id_themes_seq', 2, true);
            public       postgres    false    245            \           0    0    users_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.users_id_seq', 1, true);
            public       postgres    false    247            .           2606    24755    article article_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.article
    ADD CONSTRAINT article_pkey PRIMARY KEY (id_article);
 >   ALTER TABLE ONLY public.article DROP CONSTRAINT article_pkey;
       public         postgres    false    196            1           2606    24757    event event_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.event
    ADD CONSTRAINT event_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.event DROP CONSTRAINT event_pkey;
       public         postgres    false    199            5           2606    24759    expense expenses_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.expense
    ADD CONSTRAINT expenses_pkey PRIMARY KEY (id_expense);
 ?   ALTER TABLE ONLY public.expense DROP CONSTRAINT expenses_pkey;
       public         postgres    false    205            8           2606    24761    mailingList mailingList_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public."mailingList"
    ADD CONSTRAINT "mailingList_pkey" PRIMARY KEY ("id_mailingList");
 J   ALTER TABLE ONLY public."mailingList" DROP CONSTRAINT "mailingList_pkey";
       public         postgres    false    208            ;           2606    24763    material material_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.material
    ADD CONSTRAINT material_pkey PRIMARY KEY (id_material);
 @   ALTER TABLE ONLY public.material DROP CONSTRAINT material_pkey;
       public         postgres    false    210            >           2606    24765    organization organization_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.organization
    ADD CONSTRAINT organization_pkey PRIMARY KEY (id_organization);
 H   ALTER TABLE ONLY public.organization DROP CONSTRAINT organization_pkey;
       public         postgres    false    216            @           2606    24767    room room_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY public.room
    ADD CONSTRAINT room_pkey PRIMARY KEY (id_room);
 8   ALTER TABLE ONLY public.room DROP CONSTRAINT room_pkey;
       public         postgres    false    221            E           2606    24769    speaker speaker_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.speaker
    ADD CONSTRAINT speaker_pkey PRIMARY KEY (id_speaker);
 >   ALTER TABLE ONLY public.speaker DROP CONSTRAINT speaker_pkey;
       public         postgres    false    226            L           2606    24771    task task_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY public.task
    ADD CONSTRAINT task_pkey PRIMARY KEY (id_task);
 8   ALTER TABLE ONLY public.task DROP CONSTRAINT task_pkey;
       public         postgres    false    238            N           2606    24773    theme themes_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY public.theme
    ADD CONSTRAINT themes_pkey PRIMARY KEY (id_theme);
 ;   ALTER TABLE ONLY public.theme DROP CONSTRAINT themes_pkey;
       public         postgres    false    241            R           2606    24775    user users_pkey 
   CONSTRAINT     O   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 ;   ALTER TABLE ONLY public."user" DROP CONSTRAINT users_pkey;
       public         postgres    false    246            6           1259    24776    fki_event-room    INDEX     H   CREATE INDEX "fki_event-room" ON public.expense USING btree (id_event);
 $   DROP INDEX public."fki_event-room";
       public         postgres    false    205            2           1259    24777    fki_event_conference    INDEX     _   CREATE INDEX fki_event_conference ON public."event-event" USING btree ("id-event_conference");
 (   DROP INDEX public.fki_event_conference;
       public         postgres    false    200            /           1259    24778    fki_frk-article_event    INDEX     O   CREATE INDEX "fki_frk-article_event" ON public.article USING btree (id_event);
 +   DROP INDEX public."fki_frk-article_event";
       public         postgres    false    196            A           1259    24779    fki_frk-event    INDEX     L   CREATE INDEX "fki_frk-event" ON public."room-event" USING btree (id_event);
 #   DROP INDEX public."fki_frk-event";
       public         postgres    false    222            3           1259    24780    fki_frk-event_salon    INDEX     [   CREATE INDEX "fki_frk-event_salon" ON public."event-event" USING btree ("id-event_salon");
 )   DROP INDEX public."fki_frk-event_salon";
       public         postgres    false    200            F           1259    24781    fki_frk-id_event    INDEX     R   CREATE INDEX "fki_frk-id_event" ON public."speaker-event" USING btree (id_event);
 &   DROP INDEX public."fki_frk-id_event";
       public         postgres    false    227            B           1259    24782    fki_frk-room    INDEX     J   CREATE INDEX "fki_frk-room" ON public."room-event" USING btree (id_room);
 "   DROP INDEX public."fki_frk-room";
       public         postgres    false    222            J           1259    24783    fki_frk-task_event    INDEX     I   CREATE INDEX "fki_frk-task_event" ON public.task USING btree (id_event);
 (   DROP INDEX public."fki_frk-task_event";
       public         postgres    false    238            O           1259    24784    fki_id_event    INDEX     J   CREATE INDEX fki_id_event ON public."theme-event" USING btree (id_event);
     DROP INDEX public.fki_id_event;
       public         postgres    false    242            H           1259    24785    fki_id_mailingList    INDEX     b   CREATE INDEX "fki_id_mailingList" ON public."speaker-mailingList" USING btree ("id_mailingList");
 (   DROP INDEX public."fki_id_mailingList";
       public         postgres    false    230            <           1259    24786    fki_id_material    INDEX     S   CREATE INDEX fki_id_material ON public."material-event" USING btree (id_material);
 #   DROP INDEX public.fki_id_material;
       public         postgres    false    211            C           1259    24787    fki_id_organization    INDEX     R   CREATE INDEX fki_id_organization ON public.speaker USING btree (id_organization);
 '   DROP INDEX public.fki_id_organization;
       public         postgres    false    226            G           1259    24788    fki_id_speaker    INDEX     P   CREATE INDEX fki_id_speaker ON public."speaker-event" USING btree (id_speaker);
 "   DROP INDEX public.fki_id_speaker;
       public         postgres    false    227            P           1259    24789    fki_id_theme    INDEX     J   CREATE INDEX fki_id_theme ON public."theme-event" USING btree (id_theme);
     DROP INDEX public.fki_id_theme;
       public         postgres    false    242            I           1259    24790    fki_id_users    INDEX     L   CREATE INDEX fki_id_users ON public."speaker-users" USING btree (id_users);
     DROP INDEX public.fki_id_users;
       public         postgres    false    233            9           1259    24791    fki_material-room    INDEX     K   CREATE INDEX "fki_material-room" ON public.material USING btree (id_room);
 '   DROP INDEX public."fki_material-room";
       public         postgres    false    210            T           2606    24792    event-event event_conference    FK CONSTRAINT     �   ALTER TABLE ONLY public."event-event"
    ADD CONSTRAINT event_conference FOREIGN KEY ("id-event_conference") REFERENCES public.event(id);
 H   ALTER TABLE ONLY public."event-event" DROP CONSTRAINT event_conference;
       public       postgres    false    200    2865    199            V           2606    24797    expense expense-room    FK CONSTRAINT     v   ALTER TABLE ONLY public.expense
    ADD CONSTRAINT "expense-room" FOREIGN KEY (id_event) REFERENCES public.event(id);
 @   ALTER TABLE ONLY public.expense DROP CONSTRAINT "expense-room";
       public       postgres    false    205    2865    199            S           2606    24802    article frk-article_event    FK CONSTRAINT     {   ALTER TABLE ONLY public.article
    ADD CONSTRAINT "frk-article_event" FOREIGN KEY (id_event) REFERENCES public.event(id);
 E   ALTER TABLE ONLY public.article DROP CONSTRAINT "frk-article_event";
       public       postgres    false    196    2865    199            \           2606    24807    room-event frk-event    FK CONSTRAINT     x   ALTER TABLE ONLY public."room-event"
    ADD CONSTRAINT "frk-event" FOREIGN KEY (id_event) REFERENCES public.event(id);
 B   ALTER TABLE ONLY public."room-event" DROP CONSTRAINT "frk-event";
       public       postgres    false    2865    199    222            U           2606    24812    event-event frk-event_salon    FK CONSTRAINT     �   ALTER TABLE ONLY public."event-event"
    ADD CONSTRAINT "frk-event_salon" FOREIGN KEY ("id-event_salon") REFERENCES public.event(id);
 I   ALTER TABLE ONLY public."event-event" DROP CONSTRAINT "frk-event_salon";
       public       postgres    false    199    2865    200            ]           2606    24817    room-event frk-room    FK CONSTRAINT     z   ALTER TABLE ONLY public."room-event"
    ADD CONSTRAINT "frk-room" FOREIGN KEY (id_room) REFERENCES public.room(id_room);
 A   ALTER TABLE ONLY public."room-event" DROP CONSTRAINT "frk-room";
       public       postgres    false    221    2880    222            e           2606    24822    task frk-task_event    FK CONSTRAINT     u   ALTER TABLE ONLY public.task
    ADD CONSTRAINT "frk-task_event" FOREIGN KEY (id_event) REFERENCES public.event(id);
 ?   ALTER TABLE ONLY public.task DROP CONSTRAINT "frk-task_event";
       public       postgres    false    2865    238    199            _           2606    24827    speaker-event id_event    FK CONSTRAINT     x   ALTER TABLE ONLY public."speaker-event"
    ADD CONSTRAINT id_event FOREIGN KEY (id_event) REFERENCES public.event(id);
 B   ALTER TABLE ONLY public."speaker-event" DROP CONSTRAINT id_event;
       public       postgres    false    2865    199    227            f           2606    24832    theme-event id_event    FK CONSTRAINT     v   ALTER TABLE ONLY public."theme-event"
    ADD CONSTRAINT id_event FOREIGN KEY (id_event) REFERENCES public.event(id);
 @   ALTER TABLE ONLY public."theme-event" DROP CONSTRAINT id_event;
       public       postgres    false    2865    199    242            X           2606    24837    material-event id_event    FK CONSTRAINT     y   ALTER TABLE ONLY public."material-event"
    ADD CONSTRAINT id_event FOREIGN KEY (id_event) REFERENCES public.event(id);
 C   ALTER TABLE ONLY public."material-event" DROP CONSTRAINT id_event;
       public       postgres    false    199    2865    211            a           2606    24842 "   speaker-mailingList id_mailingList    FK CONSTRAINT     �   ALTER TABLE ONLY public."speaker-mailingList"
    ADD CONSTRAINT "id_mailingList" FOREIGN KEY ("id_mailingList") REFERENCES public."mailingList"("id_mailingList");
 P   ALTER TABLE ONLY public."speaker-mailingList" DROP CONSTRAINT "id_mailingList";
       public       postgres    false    208    230    2872            Z           2606    24847 '   organization-mailingList id_mailingList    FK CONSTRAINT     �   ALTER TABLE ONLY public."organization-mailingList"
    ADD CONSTRAINT "id_mailingList" FOREIGN KEY ("id_mailingList") REFERENCES public."mailingList"("id_mailingList");
 U   ALTER TABLE ONLY public."organization-mailingList" DROP CONSTRAINT "id_mailingList";
       public       postgres    false    217    208    2872            Y           2606    24852    material-event id_material    FK CONSTRAINT     �   ALTER TABLE ONLY public."material-event"
    ADD CONSTRAINT id_material FOREIGN KEY (id_material) REFERENCES public.material(id_material);
 F   ALTER TABLE ONLY public."material-event" DROP CONSTRAINT id_material;
       public       postgres    false    210    211    2875            ^           2606    24857    speaker id_organization    FK CONSTRAINT     �   ALTER TABLE ONLY public.speaker
    ADD CONSTRAINT id_organization FOREIGN KEY (id_organization) REFERENCES public.organization(id_organization);
 A   ALTER TABLE ONLY public.speaker DROP CONSTRAINT id_organization;
       public       postgres    false    226    2878    216            [           2606    24862 (   organization-mailingList id_organization    FK CONSTRAINT     �   ALTER TABLE ONLY public."organization-mailingList"
    ADD CONSTRAINT id_organization FOREIGN KEY (id_organization) REFERENCES public.organization(id_organization);
 T   ALTER TABLE ONLY public."organization-mailingList" DROP CONSTRAINT id_organization;
       public       postgres    false    216    2878    217            `           2606    24867    speaker-event id_speaker    FK CONSTRAINT     �   ALTER TABLE ONLY public."speaker-event"
    ADD CONSTRAINT id_speaker FOREIGN KEY (id_speaker) REFERENCES public.speaker(id_speaker);
 D   ALTER TABLE ONLY public."speaker-event" DROP CONSTRAINT id_speaker;
       public       postgres    false    2885    227    226            c           2606    24872    speaker-users id_speaker    FK CONSTRAINT     �   ALTER TABLE ONLY public."speaker-users"
    ADD CONSTRAINT id_speaker FOREIGN KEY (id_speaker) REFERENCES public.speaker(id_speaker);
 D   ALTER TABLE ONLY public."speaker-users" DROP CONSTRAINT id_speaker;
       public       postgres    false    2885    233    226            b           2606    24877    speaker-mailingList id_speaker    FK CONSTRAINT     �   ALTER TABLE ONLY public."speaker-mailingList"
    ADD CONSTRAINT id_speaker FOREIGN KEY (id_speaker) REFERENCES public.speaker(id_speaker);
 J   ALTER TABLE ONLY public."speaker-mailingList" DROP CONSTRAINT id_speaker;
       public       postgres    false    230    226    2885            g           2606    24882    theme-event id_theme    FK CONSTRAINT     |   ALTER TABLE ONLY public."theme-event"
    ADD CONSTRAINT id_theme FOREIGN KEY (id_theme) REFERENCES public.theme(id_theme);
 @   ALTER TABLE ONLY public."theme-event" DROP CONSTRAINT id_theme;
       public       postgres    false    241    242    2894            d           2606    24887    speaker-users id_users    FK CONSTRAINT     y   ALTER TABLE ONLY public."speaker-users"
    ADD CONSTRAINT id_users FOREIGN KEY (id_users) REFERENCES public."user"(id);
 B   ALTER TABLE ONLY public."speaker-users" DROP CONSTRAINT id_users;
       public       postgres    false    2898    233    246            W           2606    24892    material material-room    FK CONSTRAINT     {   ALTER TABLE ONLY public.material
    ADD CONSTRAINT "material-room" FOREIGN KEY (id_room) REFERENCES public.room(id_room);
 B   ALTER TABLE ONLY public.material DROP CONSTRAINT "material-room";
       public       postgres    false    221    210    2880            �   2   x�3�4��/-*,��TH�,Q8�<Q�8��X�P���Ǚ_������ I=b      �   �   x�e�;
�0�Y>��NM��P�d��5KHLu-��ܩ���j�#CA �ç����P�@	��xB�'h%�-:�^Ӓ3t:� ��hq�!�z/~�3p}$��������7��p��c
���ꛟ���S��#ޟ�Yr?h`�RZi�3Ƽ ψ9�      �      x�3�4�2�4b���� ��      �   (   x�3�4�44�300�N�I�,JUHIUp�I������ y=�      �      x�3�L������� 9&      �   "   x�3�4�tN�M-J�2�4�IL�I����� R��      �      x�3�4�2�4�2cC.c4~� UCo      �   O   x�3�,�4405��/(J.I-�L�L��+IL.q(����Ҋ��8�8�*sKRKRS󊁬�̜T�z��=... \#	      �      x�3�4�2bC(���� 7      �      x�3��H��I�2�440������ +
�      �      x������ � �      �   \   x�=�1
�0��99L�=A]����	��JRoo\ܾދ�h��h�jo
�p�������KE8�EZ������
�+[�Tl@�|�-0���"S             x������ � �            x�3�4�2bC(���� 7            x�3�4�2�4b���� 3         W   x�Ʊ�0�:��'���(�i��B�;a~���4��մ��舛大��.����ӉY��������*��/=�TE��9�j�            x�3�L�,�2�t�O�N-����� 6��            x�3�4�2�4�2bC$v� 4�y         4   x�3�,��M���+K�L�LuH-(�,I���K+��p��e8���b���� Њ     