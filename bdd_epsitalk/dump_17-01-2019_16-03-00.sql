PGDMP                          w            epsitalk    11.1    11.1 �    �	           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            �	           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            �	           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            �	           1262    16677    epsitalk    DATABASE     �   CREATE DATABASE epsitalk WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'French_France.1252' LC_CTYPE = 'French_France.1252';
    DROP DATABASE epsitalk;
             postgres    false            �            1259    16787    article    TABLE     �   CREATE TABLE public.article (
    id_article integer NOT NULL,
    id_event integer NOT NULL,
    title character varying(50) NOT NULL,
    containt character varying(10000) NOT NULL
);
    DROP TABLE public.article;
       public         postgres    false            �            1259    16783    article_id_article_seq    SEQUENCE     �   CREATE SEQUENCE public.article_id_article_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.article_id_article_seq;
       public       postgres    false    216            �	           0    0    article_id_article_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.article_id_article_seq OWNED BY public.article.id_article;
            public       postgres    false    214            �            1259    16785    article_id_event_seq    SEQUENCE     �   CREATE SEQUENCE public.article_id_event_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.article_id_event_seq;
       public       postgres    false    216            �	           0    0    article_id_event_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.article_id_event_seq OWNED BY public.article.id_event;
            public       postgres    false    215            �            1259    16701    event    TABLE     (  CREATE TABLE public.event (
    id integer NOT NULL,
    date date NOT NULL,
    id_speaker integer,
    name character varying(100) NOT NULL,
    description character varying(250) NOT NULL,
    "hourStart" time(4) without time zone NOT NULL,
    "hourEnd" time(4) without time zone NOT NULL
);
    DROP TABLE public.event;
       public         postgres    false            �            1259    16870    event-event    TABLE     y   CREATE TABLE public."event-event" (
    "id-event_salon" integer NOT NULL,
    "id-event_conference" integer NOT NULL
);
 !   DROP TABLE public."event-event";
       public         postgres    false            �            1259    16868 #   event-event_id-event_conference_seq    SEQUENCE     �   CREATE SEQUENCE public."event-event_id-event_conference_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 <   DROP SEQUENCE public."event-event_id-event_conference_seq";
       public       postgres    false    229            �	           0    0 #   event-event_id-event_conference_seq    SEQUENCE OWNED BY     q   ALTER SEQUENCE public."event-event_id-event_conference_seq" OWNED BY public."event-event"."id-event_conference";
            public       postgres    false    228            �            1259    16866    event-event_id-event_salon_seq    SEQUENCE     �   CREATE SEQUENCE public."event-event_id-event_salon_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public."event-event_id-event_salon_seq";
       public       postgres    false    229            �	           0    0    event-event_id-event_salon_seq    SEQUENCE OWNED BY     g   ALTER SEQUENCE public."event-event_id-event_salon_seq" OWNED BY public."event-event"."id-event_salon";
            public       postgres    false    227            �            1259    16697    event_id_seq    SEQUENCE     �   CREATE SEQUENCE public.event_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.event_id_seq;
       public       postgres    false    200            �	           0    0    event_id_seq    SEQUENCE OWNED BY     =   ALTER SEQUENCE public.event_id_seq OWNED BY public.event.id;
            public       postgres    false    198            �            1259    16699    event_id_speaker_seq    SEQUENCE     �   CREATE SEQUENCE public.event_id_speaker_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.event_id_speaker_seq;
       public       postgres    false    200            �	           0    0    event_id_speaker_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.event_id_speaker_seq OWNED BY public.event.id_speaker;
            public       postgres    false    199            �            1259    16747    expense    TABLE     �   CREATE TABLE public.expense (
    id_expense integer NOT NULL,
    id_event integer NOT NULL,
    name character varying(100) NOT NULL,
    receipt character varying(200)
);
    DROP TABLE public.expense;
       public         postgres    false            �            1259    16745    expenses_id_event_seq    SEQUENCE     �   CREATE SEQUENCE public.expenses_id_event_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ,   DROP SEQUENCE public.expenses_id_event_seq;
       public       postgres    false    208            �	           0    0    expenses_id_event_seq    SEQUENCE OWNED BY     N   ALTER SEQUENCE public.expenses_id_event_seq OWNED BY public.expense.id_event;
            public       postgres    false    207            �            1259    16743    expenses_id_expense_seq    SEQUENCE     �   CREATE SEQUENCE public.expenses_id_expense_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 .   DROP SEQUENCE public.expenses_id_expense_seq;
       public       postgres    false    208            �	           0    0    expenses_id_expense_seq    SEQUENCE OWNED BY     R   ALTER SEQUENCE public.expenses_id_expense_seq OWNED BY public.expense.id_expense;
            public       postgres    false    206            �            1259    16824    mailingList    TABLE     w   CREATE TABLE public."mailingList" (
    "id_mailingList" integer NOT NULL,
    title character varying(60) NOT NULL
);
 !   DROP TABLE public."mailingList";
       public         postgres    false            �            1259    16822    mailingList_id_mailingList_seq    SEQUENCE     �   CREATE SEQUENCE public."mailingList_id_mailingList_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public."mailingList_id_mailingList_seq";
       public       postgres    false    223            �	           0    0    mailingList_id_mailingList_seq    SEQUENCE OWNED BY     g   ALTER SEQUENCE public."mailingList_id_mailingList_seq" OWNED BY public."mailingList"."id_mailingList";
            public       postgres    false    222            �            1259    16724    material    TABLE     }   CREATE TABLE public.material (
    id_material integer NOT NULL,
    id_room integer NOT NULL,
    name character varying
);
    DROP TABLE public.material;
       public         postgres    false            �            1259    17034    material-event    TABLE     j   CREATE TABLE public."material-event" (
    id_event integer NOT NULL,
    id_material integer NOT NULL
);
 $   DROP TABLE public."material-event";
       public         postgres    false            �            1259    17030    material-event_id_event_seq    SEQUENCE     �   CREATE SEQUENCE public."material-event_id_event_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 4   DROP SEQUENCE public."material-event_id_event_seq";
       public       postgres    false    247            �	           0    0    material-event_id_event_seq    SEQUENCE OWNED BY     _   ALTER SEQUENCE public."material-event_id_event_seq" OWNED BY public."material-event".id_event;
            public       postgres    false    245            �            1259    17032    material-event_id_material_seq    SEQUENCE     �   CREATE SEQUENCE public."material-event_id_material_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public."material-event_id_material_seq";
       public       postgres    false    247            �	           0    0    material-event_id_material_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public."material-event_id_material_seq" OWNED BY public."material-event".id_material;
            public       postgres    false    246            �            1259    16722    material_id_material_seq    SEQUENCE     �   CREATE SEQUENCE public.material_id_material_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.material_id_material_seq;
       public       postgres    false    204            �	           0    0    material_id_material_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.material_id_material_seq OWNED BY public.material.id_material;
            public       postgres    false    203            �            1259    16733    material_id_room_seq    SEQUENCE     �   CREATE SEQUENCE public.material_id_room_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.material_id_room_seq;
       public       postgres    false    204            �	           0    0    material_id_room_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.material_id_room_seq OWNED BY public.material.id_room;
            public       postgres    false    205            �            1259    16813    organization    TABLE     �   CREATE TABLE public.organization (
    id_organization integer NOT NULL,
    sponsor boolean NOT NULL,
    effective integer,
    name character varying,
    mail character varying
);
     DROP TABLE public.organization;
       public         postgres    false            �            1259    16995    organization-mailingList    TABLE     �   CREATE TABLE public."organization-mailingList" (
    id_organization integer NOT NULL,
    "id_mailingList" integer NOT NULL
);
 .   DROP TABLE public."organization-mailingList";
       public         postgres    false            �            1259    16993 +   organization-mailingList_id_mailingList_seq    SEQUENCE     �   CREATE SEQUENCE public."organization-mailingList_id_mailingList_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 D   DROP SEQUENCE public."organization-mailingList_id_mailingList_seq";
       public       postgres    false    244            �	           0    0 +   organization-mailingList_id_mailingList_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public."organization-mailingList_id_mailingList_seq" OWNED BY public."organization-mailingList"."id_mailingList";
            public       postgres    false    243            �            1259    16991 ,   organization-mailingList_id_organization_seq    SEQUENCE     �   CREATE SEQUENCE public."organization-mailingList_id_organization_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 E   DROP SEQUENCE public."organization-mailingList_id_organization_seq";
       public       postgres    false    244            �	           0    0 ,   organization-mailingList_id_organization_seq    SEQUENCE OWNED BY     �   ALTER SEQUENCE public."organization-mailingList_id_organization_seq" OWNED BY public."organization-mailingList".id_organization;
            public       postgres    false    242            �            1259    16811     organization_id_organization_seq    SEQUENCE     �   CREATE SEQUENCE public.organization_id_organization_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.organization_id_organization_seq;
       public       postgres    false    221            �	           0    0     organization_id_organization_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.organization_id_organization_seq OWNED BY public.organization.id_organization;
            public       postgres    false    220            �            1259    16713    room    TABLE     \   CREATE TABLE public.room (
    id_room integer NOT NULL,
    name character varying(100)
);
    DROP TABLE public.room;
       public         postgres    false            �            1259    16849 
   room-event    TABLE     b   CREATE TABLE public."room-event" (
    id_event integer NOT NULL,
    id_room integer NOT NULL
);
     DROP TABLE public."room-event";
       public         postgres    false            �            1259    16845    room-event_id_event_seq    SEQUENCE     �   CREATE SEQUENCE public."room-event_id_event_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 0   DROP SEQUENCE public."room-event_id_event_seq";
       public       postgres    false    226            �	           0    0    room-event_id_event_seq    SEQUENCE OWNED BY     W   ALTER SEQUENCE public."room-event_id_event_seq" OWNED BY public."room-event".id_event;
            public       postgres    false    224            �            1259    16847    room-event_id_room_seq    SEQUENCE     �   CREATE SEQUENCE public."room-event_id_room_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public."room-event_id_room_seq";
       public       postgres    false    226            �	           0    0    room-event_id_room_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public."room-event_id_room_seq" OWNED BY public."room-event".id_room;
            public       postgres    false    225            �            1259    16711    room_id_room_seq    SEQUENCE     �   CREATE SEQUENCE public.room_id_room_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.room_id_room_seq;
       public       postgres    false    202            �	           0    0    room_id_room_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.room_id_room_seq OWNED BY public.room.id_room;
            public       postgres    false    201            �            1259    16772    speaker    TABLE     �   CREATE TABLE public.speaker (
    id_speaker integer NOT NULL,
    id_organization integer NOT NULL,
    name character varying,
    surname character varying,
    mail character varying
);
    DROP TABLE public.speaker;
       public         postgres    false            �            1259    16922    speaker-event    TABLE     h   CREATE TABLE public."speaker-event" (
    id_event integer NOT NULL,
    id_speaker integer NOT NULL
);
 #   DROP TABLE public."speaker-event";
       public         postgres    false            �            1259    16918    speaker-event_id_event_seq    SEQUENCE     �   CREATE SEQUENCE public."speaker-event_id_event_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public."speaker-event_id_event_seq";
       public       postgres    false    235            �	           0    0    speaker-event_id_event_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public."speaker-event_id_event_seq" OWNED BY public."speaker-event".id_event;
            public       postgres    false    233            �            1259    16920    speaker-event_id_speaker_seq    SEQUENCE     �   CREATE SEQUENCE public."speaker-event_id_speaker_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public."speaker-event_id_speaker_seq";
       public       postgres    false    235            �	           0    0    speaker-event_id_speaker_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public."speaker-event_id_speaker_seq" OWNED BY public."speaker-event".id_speaker;
            public       postgres    false    234            �            1259    16975    speaker-mailingList    TABLE     v   CREATE TABLE public."speaker-mailingList" (
    id_speaker integer NOT NULL,
    "id_mailingList" integer NOT NULL
);
 )   DROP TABLE public."speaker-mailingList";
       public         postgres    false            �            1259    16973 &   speaker-mailingList_id_mailingList_seq    SEQUENCE     �   CREATE SEQUENCE public."speaker-mailingList_id_mailingList_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ?   DROP SEQUENCE public."speaker-mailingList_id_mailingList_seq";
       public       postgres    false    241            �	           0    0 &   speaker-mailingList_id_mailingList_seq    SEQUENCE OWNED BY     w   ALTER SEQUENCE public."speaker-mailingList_id_mailingList_seq" OWNED BY public."speaker-mailingList"."id_mailingList";
            public       postgres    false    240            �            1259    16971 "   speaker-mailingList_id_speaker_seq    SEQUENCE     �   CREATE SEQUENCE public."speaker-mailingList_id_speaker_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 ;   DROP SEQUENCE public."speaker-mailingList_id_speaker_seq";
       public       postgres    false    241            �	           0    0 "   speaker-mailingList_id_speaker_seq    SEQUENCE OWNED BY     m   ALTER SEQUENCE public."speaker-mailingList_id_speaker_seq" OWNED BY public."speaker-mailingList".id_speaker;
            public       postgres    false    239            �            1259    16948    speaker-users    TABLE     h   CREATE TABLE public."speaker-users" (
    id_users integer NOT NULL,
    id_speaker integer NOT NULL
);
 #   DROP TABLE public."speaker-users";
       public         postgres    false            �            1259    16946    speaker-users_id_speaker_seq    SEQUENCE     �   CREATE SEQUENCE public."speaker-users_id_speaker_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 5   DROP SEQUENCE public."speaker-users_id_speaker_seq";
       public       postgres    false    238            �	           0    0    speaker-users_id_speaker_seq    SEQUENCE OWNED BY     a   ALTER SEQUENCE public."speaker-users_id_speaker_seq" OWNED BY public."speaker-users".id_speaker;
            public       postgres    false    237            �            1259    16944    speaker-users_id_users_seq    SEQUENCE     �   CREATE SEQUENCE public."speaker-users_id_users_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 3   DROP SEQUENCE public."speaker-users_id_users_seq";
       public       postgres    false    238            �	           0    0    speaker-users_id_users_seq    SEQUENCE OWNED BY     ]   ALTER SEQUENCE public."speaker-users_id_users_seq" OWNED BY public."speaker-users".id_users;
            public       postgres    false    236            �            1259    16770    speaker_id_organization_seq    SEQUENCE     �   CREATE SEQUENCE public.speaker_id_organization_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 2   DROP SEQUENCE public.speaker_id_organization_seq;
       public       postgres    false    213            �	           0    0    speaker_id_organization_seq    SEQUENCE OWNED BY     [   ALTER SEQUENCE public.speaker_id_organization_seq OWNED BY public.speaker.id_organization;
            public       postgres    false    212            �            1259    16768    speaker_id_speaker_seq    SEQUENCE     �   CREATE SEQUENCE public.speaker_id_speaker_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.speaker_id_speaker_seq;
       public       postgres    false    213            �	           0    0    speaker_id_speaker_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.speaker_id_speaker_seq OWNED BY public.speaker.id_speaker;
            public       postgres    false    211            �            1259    16801    task    TABLE     �   CREATE TABLE public.task (
    id_task integer NOT NULL,
    id_event integer NOT NULL,
    title character varying,
    containt character varying
);
    DROP TABLE public.task;
       public         postgres    false            �            1259    16799    task_id_event_seq    SEQUENCE     �   CREATE SEQUENCE public.task_id_event_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 (   DROP SEQUENCE public.task_id_event_seq;
       public       postgres    false    219            �	           0    0    task_id_event_seq    SEQUENCE OWNED BY     G   ALTER SEQUENCE public.task_id_event_seq OWNED BY public.task.id_event;
            public       postgres    false    218            �            1259    16797    task_id_task_seq    SEQUENCE     �   CREATE SEQUENCE public.task_id_task_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.task_id_task_seq;
       public       postgres    false    219            �	           0    0    task_id_task_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.task_id_task_seq OWNED BY public.task.id_task;
            public       postgres    false    217            �            1259    16759    theme    TABLE     Y   CREATE TABLE public.theme (
    id_theme integer NOT NULL,
    name character varying
);
    DROP TABLE public.theme;
       public         postgres    false            �            1259    16891    theme-event    TABLE     d   CREATE TABLE public."theme-event" (
    id_theme integer NOT NULL,
    id_event integer NOT NULL
);
 !   DROP TABLE public."theme-event";
       public         postgres    false            �            1259    16889    theme-event_id_event_seq    SEQUENCE     �   CREATE SEQUENCE public."theme-event_id_event_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public."theme-event_id_event_seq";
       public       postgres    false    232            �	           0    0    theme-event_id_event_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public."theme-event_id_event_seq" OWNED BY public."theme-event".id_event;
            public       postgres    false    231            �            1259    16887    theme-event_id_theme_seq    SEQUENCE     �   CREATE SEQUENCE public."theme-event_id_theme_seq"
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 1   DROP SEQUENCE public."theme-event_id_theme_seq";
       public       postgres    false    232            �	           0    0    theme-event_id_theme_seq    SEQUENCE OWNED BY     Y   ALTER SEQUENCE public."theme-event_id_theme_seq" OWNED BY public."theme-event".id_theme;
            public       postgres    false    230            �            1259    16757    themes_id_themes_seq    SEQUENCE     �   CREATE SEQUENCE public.themes_id_themes_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.themes_id_themes_seq;
       public       postgres    false    210            �	           0    0    themes_id_themes_seq    SEQUENCE OWNED BY     K   ALTER SEQUENCE public.themes_id_themes_seq OWNED BY public.theme.id_theme;
            public       postgres    false    209            �            1259    16680    user    TABLE     �   CREATE TABLE public."user" (
    id integer NOT NULL,
    mail character varying(200) NOT NULL,
    name character varying(70) NOT NULL,
    surname character varying(70) NOT NULL,
    password character varying(350) NOT NULL
);
    DROP TABLE public."user";
       public         postgres    false            �            1259    16678    users_id_seq    SEQUENCE     �   CREATE SEQUENCE public.users_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 #   DROP SEQUENCE public.users_id_seq;
       public       postgres    false    197            �	           0    0    users_id_seq    SEQUENCE OWNED BY     >   ALTER SEQUENCE public.users_id_seq OWNED BY public."user".id;
            public       postgres    false    196            �           2604    16790    article id_article    DEFAULT     x   ALTER TABLE ONLY public.article ALTER COLUMN id_article SET DEFAULT nextval('public.article_id_article_seq'::regclass);
 A   ALTER TABLE public.article ALTER COLUMN id_article DROP DEFAULT;
       public       postgres    false    214    216    216            �           2604    16791    article id_event    DEFAULT     t   ALTER TABLE ONLY public.article ALTER COLUMN id_event SET DEFAULT nextval('public.article_id_event_seq'::regclass);
 ?   ALTER TABLE public.article ALTER COLUMN id_event DROP DEFAULT;
       public       postgres    false    215    216    216            �           2604    16704    event id    DEFAULT     d   ALTER TABLE ONLY public.event ALTER COLUMN id SET DEFAULT nextval('public.event_id_seq'::regclass);
 7   ALTER TABLE public.event ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    198    200    200            �           2604    16705    event id_speaker    DEFAULT     t   ALTER TABLE ONLY public.event ALTER COLUMN id_speaker SET DEFAULT nextval('public.event_id_speaker_seq'::regclass);
 ?   ALTER TABLE public.event ALTER COLUMN id_speaker DROP DEFAULT;
       public       postgres    false    199    200    200            �           2604    16873    event-event id-event_salon    DEFAULT     �   ALTER TABLE ONLY public."event-event" ALTER COLUMN "id-event_salon" SET DEFAULT nextval('public."event-event_id-event_salon_seq"'::regclass);
 M   ALTER TABLE public."event-event" ALTER COLUMN "id-event_salon" DROP DEFAULT;
       public       postgres    false    227    229    229            �           2604    16874    event-event id-event_conference    DEFAULT     �   ALTER TABLE ONLY public."event-event" ALTER COLUMN "id-event_conference" SET DEFAULT nextval('public."event-event_id-event_conference_seq"'::regclass);
 R   ALTER TABLE public."event-event" ALTER COLUMN "id-event_conference" DROP DEFAULT;
       public       postgres    false    228    229    229            �           2604    16750    expense id_expense    DEFAULT     y   ALTER TABLE ONLY public.expense ALTER COLUMN id_expense SET DEFAULT nextval('public.expenses_id_expense_seq'::regclass);
 A   ALTER TABLE public.expense ALTER COLUMN id_expense DROP DEFAULT;
       public       postgres    false    208    206    208            �           2604    16751    expense id_event    DEFAULT     u   ALTER TABLE ONLY public.expense ALTER COLUMN id_event SET DEFAULT nextval('public.expenses_id_event_seq'::regclass);
 ?   ALTER TABLE public.expense ALTER COLUMN id_event DROP DEFAULT;
       public       postgres    false    208    207    208            �           2604    16827    mailingList id_mailingList    DEFAULT     �   ALTER TABLE ONLY public."mailingList" ALTER COLUMN "id_mailingList" SET DEFAULT nextval('public."mailingList_id_mailingList_seq"'::regclass);
 M   ALTER TABLE public."mailingList" ALTER COLUMN "id_mailingList" DROP DEFAULT;
       public       postgres    false    222    223    223            �           2604    16727    material id_material    DEFAULT     |   ALTER TABLE ONLY public.material ALTER COLUMN id_material SET DEFAULT nextval('public.material_id_material_seq'::regclass);
 C   ALTER TABLE public.material ALTER COLUMN id_material DROP DEFAULT;
       public       postgres    false    203    204    204            �           2604    16735    material id_room    DEFAULT     t   ALTER TABLE ONLY public.material ALTER COLUMN id_room SET DEFAULT nextval('public.material_id_room_seq'::regclass);
 ?   ALTER TABLE public.material ALTER COLUMN id_room DROP DEFAULT;
       public       postgres    false    205    204            �           2604    17037    material-event id_event    DEFAULT     �   ALTER TABLE ONLY public."material-event" ALTER COLUMN id_event SET DEFAULT nextval('public."material-event_id_event_seq"'::regclass);
 H   ALTER TABLE public."material-event" ALTER COLUMN id_event DROP DEFAULT;
       public       postgres    false    247    245    247            �           2604    17038    material-event id_material    DEFAULT     �   ALTER TABLE ONLY public."material-event" ALTER COLUMN id_material SET DEFAULT nextval('public."material-event_id_material_seq"'::regclass);
 K   ALTER TABLE public."material-event" ALTER COLUMN id_material DROP DEFAULT;
       public       postgres    false    246    247    247            �           2604    16816    organization id_organization    DEFAULT     �   ALTER TABLE ONLY public.organization ALTER COLUMN id_organization SET DEFAULT nextval('public.organization_id_organization_seq'::regclass);
 K   ALTER TABLE public.organization ALTER COLUMN id_organization DROP DEFAULT;
       public       postgres    false    221    220    221            �           2604    16998 (   organization-mailingList id_organization    DEFAULT     �   ALTER TABLE ONLY public."organization-mailingList" ALTER COLUMN id_organization SET DEFAULT nextval('public."organization-mailingList_id_organization_seq"'::regclass);
 Y   ALTER TABLE public."organization-mailingList" ALTER COLUMN id_organization DROP DEFAULT;
       public       postgres    false    244    242    244            �           2604    16999 '   organization-mailingList id_mailingList    DEFAULT     �   ALTER TABLE ONLY public."organization-mailingList" ALTER COLUMN "id_mailingList" SET DEFAULT nextval('public."organization-mailingList_id_mailingList_seq"'::regclass);
 Z   ALTER TABLE public."organization-mailingList" ALTER COLUMN "id_mailingList" DROP DEFAULT;
       public       postgres    false    244    243    244            �           2604    16716    room id_room    DEFAULT     l   ALTER TABLE ONLY public.room ALTER COLUMN id_room SET DEFAULT nextval('public.room_id_room_seq'::regclass);
 ;   ALTER TABLE public.room ALTER COLUMN id_room DROP DEFAULT;
       public       postgres    false    202    201    202            �           2604    16852    room-event id_event    DEFAULT     ~   ALTER TABLE ONLY public."room-event" ALTER COLUMN id_event SET DEFAULT nextval('public."room-event_id_event_seq"'::regclass);
 D   ALTER TABLE public."room-event" ALTER COLUMN id_event DROP DEFAULT;
       public       postgres    false    226    224    226            �           2604    16853    room-event id_room    DEFAULT     |   ALTER TABLE ONLY public."room-event" ALTER COLUMN id_room SET DEFAULT nextval('public."room-event_id_room_seq"'::regclass);
 C   ALTER TABLE public."room-event" ALTER COLUMN id_room DROP DEFAULT;
       public       postgres    false    225    226    226            �           2604    16775    speaker id_speaker    DEFAULT     x   ALTER TABLE ONLY public.speaker ALTER COLUMN id_speaker SET DEFAULT nextval('public.speaker_id_speaker_seq'::regclass);
 A   ALTER TABLE public.speaker ALTER COLUMN id_speaker DROP DEFAULT;
       public       postgres    false    213    211    213            �           2604    16776    speaker id_organization    DEFAULT     �   ALTER TABLE ONLY public.speaker ALTER COLUMN id_organization SET DEFAULT nextval('public.speaker_id_organization_seq'::regclass);
 F   ALTER TABLE public.speaker ALTER COLUMN id_organization DROP DEFAULT;
       public       postgres    false    212    213    213            �           2604    16925    speaker-event id_event    DEFAULT     �   ALTER TABLE ONLY public."speaker-event" ALTER COLUMN id_event SET DEFAULT nextval('public."speaker-event_id_event_seq"'::regclass);
 G   ALTER TABLE public."speaker-event" ALTER COLUMN id_event DROP DEFAULT;
       public       postgres    false    233    235    235            �           2604    16926    speaker-event id_speaker    DEFAULT     �   ALTER TABLE ONLY public."speaker-event" ALTER COLUMN id_speaker SET DEFAULT nextval('public."speaker-event_id_speaker_seq"'::regclass);
 I   ALTER TABLE public."speaker-event" ALTER COLUMN id_speaker DROP DEFAULT;
       public       postgres    false    235    234    235            �           2604    16978    speaker-mailingList id_speaker    DEFAULT     �   ALTER TABLE ONLY public."speaker-mailingList" ALTER COLUMN id_speaker SET DEFAULT nextval('public."speaker-mailingList_id_speaker_seq"'::regclass);
 O   ALTER TABLE public."speaker-mailingList" ALTER COLUMN id_speaker DROP DEFAULT;
       public       postgres    false    239    241    241            �           2604    16979 "   speaker-mailingList id_mailingList    DEFAULT     �   ALTER TABLE ONLY public."speaker-mailingList" ALTER COLUMN "id_mailingList" SET DEFAULT nextval('public."speaker-mailingList_id_mailingList_seq"'::regclass);
 U   ALTER TABLE public."speaker-mailingList" ALTER COLUMN "id_mailingList" DROP DEFAULT;
       public       postgres    false    241    240    241            �           2604    16951    speaker-users id_users    DEFAULT     �   ALTER TABLE ONLY public."speaker-users" ALTER COLUMN id_users SET DEFAULT nextval('public."speaker-users_id_users_seq"'::regclass);
 G   ALTER TABLE public."speaker-users" ALTER COLUMN id_users DROP DEFAULT;
       public       postgres    false    238    236    238            �           2604    16952    speaker-users id_speaker    DEFAULT     �   ALTER TABLE ONLY public."speaker-users" ALTER COLUMN id_speaker SET DEFAULT nextval('public."speaker-users_id_speaker_seq"'::regclass);
 I   ALTER TABLE public."speaker-users" ALTER COLUMN id_speaker DROP DEFAULT;
       public       postgres    false    237    238    238            �           2604    16804    task id_task    DEFAULT     l   ALTER TABLE ONLY public.task ALTER COLUMN id_task SET DEFAULT nextval('public.task_id_task_seq'::regclass);
 ;   ALTER TABLE public.task ALTER COLUMN id_task DROP DEFAULT;
       public       postgres    false    217    219    219            �           2604    16805    task id_event    DEFAULT     n   ALTER TABLE ONLY public.task ALTER COLUMN id_event SET DEFAULT nextval('public.task_id_event_seq'::regclass);
 <   ALTER TABLE public.task ALTER COLUMN id_event DROP DEFAULT;
       public       postgres    false    218    219    219            �           2604    16762    theme id_theme    DEFAULT     r   ALTER TABLE ONLY public.theme ALTER COLUMN id_theme SET DEFAULT nextval('public.themes_id_themes_seq'::regclass);
 =   ALTER TABLE public.theme ALTER COLUMN id_theme DROP DEFAULT;
       public       postgres    false    210    209    210            �           2604    16894    theme-event id_theme    DEFAULT     �   ALTER TABLE ONLY public."theme-event" ALTER COLUMN id_theme SET DEFAULT nextval('public."theme-event_id_theme_seq"'::regclass);
 E   ALTER TABLE public."theme-event" ALTER COLUMN id_theme DROP DEFAULT;
       public       postgres    false    232    230    232            �           2604    16895    theme-event id_event    DEFAULT     �   ALTER TABLE ONLY public."theme-event" ALTER COLUMN id_event SET DEFAULT nextval('public."theme-event_id_event_seq"'::regclass);
 E   ALTER TABLE public."theme-event" ALTER COLUMN id_event DROP DEFAULT;
       public       postgres    false    232    231    232            �           2604    16683    user id    DEFAULT     e   ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.users_id_seq'::regclass);
 8   ALTER TABLE public."user" ALTER COLUMN id DROP DEFAULT;
       public       postgres    false    196    197    197            r	          0    16787    article 
   TABLE DATA               H   COPY public.article (id_article, id_event, title, containt) FROM stdin;
    public       postgres    false    216   ��       b	          0    16701    event 
   TABLE DATA               `   COPY public.event (id, date, id_speaker, name, description, "hourStart", "hourEnd") FROM stdin;
    public       postgres    false    200   <�       	          0    16870    event-event 
   TABLE DATA               P   COPY public."event-event" ("id-event_salon", "id-event_conference") FROM stdin;
    public       postgres    false    229   ��       j	          0    16747    expense 
   TABLE DATA               F   COPY public.expense (id_expense, id_event, name, receipt) FROM stdin;
    public       postgres    false    208   	�       y	          0    16824    mailingList 
   TABLE DATA               @   COPY public."mailingList" ("id_mailingList", title) FROM stdin;
    public       postgres    false    223   A�       f	          0    16724    material 
   TABLE DATA               >   COPY public.material (id_material, id_room, name) FROM stdin;
    public       postgres    false    204   d�       �	          0    17034    material-event 
   TABLE DATA               A   COPY public."material-event" (id_event, id_material) FROM stdin;
    public       postgres    false    247   ��       w	          0    16813    organization 
   TABLE DATA               W   COPY public.organization (id_organization, sponsor, effective, name, mail) FROM stdin;
    public       postgres    false    221   ��       �	          0    16995    organization-mailingList 
   TABLE DATA               W   COPY public."organization-mailingList" (id_organization, "id_mailingList") FROM stdin;
    public       postgres    false    244   �       d	          0    16713    room 
   TABLE DATA               -   COPY public.room (id_room, name) FROM stdin;
    public       postgres    false    202   A�       |	          0    16849 
   room-event 
   TABLE DATA               9   COPY public."room-event" (id_event, id_room) FROM stdin;
    public       postgres    false    226   l�       o	          0    16772    speaker 
   TABLE DATA               S   COPY public.speaker (id_speaker, id_organization, name, surname, mail) FROM stdin;
    public       postgres    false    213   ��       �	          0    16922    speaker-event 
   TABLE DATA               ?   COPY public."speaker-event" (id_event, id_speaker) FROM stdin;
    public       postgres    false    235   ��       �	          0    16975    speaker-mailingList 
   TABLE DATA               M   COPY public."speaker-mailingList" (id_speaker, "id_mailingList") FROM stdin;
    public       postgres    false    241          �	          0    16948    speaker-users 
   TABLE DATA               ?   COPY public."speaker-users" (id_users, id_speaker) FROM stdin;
    public       postgres    false    238   5       u	          0    16801    task 
   TABLE DATA               B   COPY public.task (id_task, id_event, title, containt) FROM stdin;
    public       postgres    false    219   Z       l	          0    16759    theme 
   TABLE DATA               /   COPY public.theme (id_theme, name) FROM stdin;
    public       postgres    false    210   �       �	          0    16891    theme-event 
   TABLE DATA               ;   COPY public."theme-event" (id_theme, id_event) FROM stdin;
    public       postgres    false    232   �       _	          0    16680    user 
   TABLE DATA               C   COPY public."user" (id, mail, name, surname, password) FROM stdin;
    public       postgres    false    197         �	           0    0    article_id_article_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.article_id_article_seq', 2, true);
            public       postgres    false    214            �	           0    0    article_id_event_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.article_id_event_seq', 1, false);
            public       postgres    false    215            �	           0    0 #   event-event_id-event_conference_seq    SEQUENCE SET     T   SELECT pg_catalog.setval('public."event-event_id-event_conference_seq"', 1, false);
            public       postgres    false    228            �	           0    0    event-event_id-event_salon_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public."event-event_id-event_salon_seq"', 1, false);
            public       postgres    false    227            �	           0    0    event_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.event_id_seq', 5, true);
            public       postgres    false    198            �	           0    0    event_id_speaker_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.event_id_speaker_seq', 1, true);
            public       postgres    false    199            �	           0    0    expenses_id_event_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.expenses_id_event_seq', 1, false);
            public       postgres    false    207            �	           0    0    expenses_id_expense_seq    SEQUENCE SET     E   SELECT pg_catalog.setval('public.expenses_id_expense_seq', 1, true);
            public       postgres    false    206            �	           0    0    mailingList_id_mailingList_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public."mailingList_id_mailingList_seq"', 1, true);
            public       postgres    false    222            �	           0    0    material-event_id_event_seq    SEQUENCE SET     L   SELECT pg_catalog.setval('public."material-event_id_event_seq"', 1, false);
            public       postgres    false    245            �	           0    0    material-event_id_material_seq    SEQUENCE SET     O   SELECT pg_catalog.setval('public."material-event_id_material_seq"', 1, false);
            public       postgres    false    246            �	           0    0    material_id_material_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.material_id_material_seq', 2, true);
            public       postgres    false    203            �	           0    0    material_id_room_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.material_id_room_seq', 1, false);
            public       postgres    false    205            �	           0    0 +   organization-mailingList_id_mailingList_seq    SEQUENCE SET     \   SELECT pg_catalog.setval('public."organization-mailingList_id_mailingList_seq"', 1, false);
            public       postgres    false    243            �	           0    0 ,   organization-mailingList_id_organization_seq    SEQUENCE SET     ]   SELECT pg_catalog.setval('public."organization-mailingList_id_organization_seq"', 1, false);
            public       postgres    false    242            �	           0    0     organization_id_organization_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.organization_id_organization_seq', 2, true);
            public       postgres    false    220            �	           0    0    room-event_id_event_seq    SEQUENCE SET     H   SELECT pg_catalog.setval('public."room-event_id_event_seq"', 1, false);
            public       postgres    false    224            �	           0    0    room-event_id_room_seq    SEQUENCE SET     G   SELECT pg_catalog.setval('public."room-event_id_room_seq"', 1, false);
            public       postgres    false    225            �	           0    0    room_id_room_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.room_id_room_seq', 2, true);
            public       postgres    false    201            �	           0    0    speaker-event_id_event_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public."speaker-event_id_event_seq"', 1, false);
            public       postgres    false    233            �	           0    0    speaker-event_id_speaker_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public."speaker-event_id_speaker_seq"', 1, false);
            public       postgres    false    234            �	           0    0 &   speaker-mailingList_id_mailingList_seq    SEQUENCE SET     W   SELECT pg_catalog.setval('public."speaker-mailingList_id_mailingList_seq"', 1, false);
            public       postgres    false    240            �	           0    0 "   speaker-mailingList_id_speaker_seq    SEQUENCE SET     S   SELECT pg_catalog.setval('public."speaker-mailingList_id_speaker_seq"', 1, false);
            public       postgres    false    239            �	           0    0    speaker-users_id_speaker_seq    SEQUENCE SET     M   SELECT pg_catalog.setval('public."speaker-users_id_speaker_seq"', 1, false);
            public       postgres    false    237            �	           0    0    speaker-users_id_users_seq    SEQUENCE SET     K   SELECT pg_catalog.setval('public."speaker-users_id_users_seq"', 1, false);
            public       postgres    false    236            �	           0    0    speaker_id_organization_seq    SEQUENCE SET     J   SELECT pg_catalog.setval('public.speaker_id_organization_seq', 1, false);
            public       postgres    false    212            �	           0    0    speaker_id_speaker_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.speaker_id_speaker_seq', 2, true);
            public       postgres    false    211            �	           0    0    task_id_event_seq    SEQUENCE SET     @   SELECT pg_catalog.setval('public.task_id_event_seq', 1, false);
            public       postgres    false    218            �	           0    0    task_id_task_seq    SEQUENCE SET     >   SELECT pg_catalog.setval('public.task_id_task_seq', 3, true);
            public       postgres    false    217            �	           0    0    theme-event_id_event_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public."theme-event_id_event_seq"', 1, false);
            public       postgres    false    231            �	           0    0    theme-event_id_theme_seq    SEQUENCE SET     I   SELECT pg_catalog.setval('public."theme-event_id_theme_seq"', 1, false);
            public       postgres    false    230            �	           0    0    themes_id_themes_seq    SEQUENCE SET     B   SELECT pg_catalog.setval('public.themes_id_themes_seq', 2, true);
            public       postgres    false    209            �	           0    0    users_id_seq    SEQUENCE SET     :   SELECT pg_catalog.setval('public.users_id_seq', 1, true);
            public       postgres    false    196            �           2606    16796    article article_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.article
    ADD CONSTRAINT article_pkey PRIMARY KEY (id_article);
 >   ALTER TABLE ONLY public.article DROP CONSTRAINT article_pkey;
       public         postgres    false    216            �           2606    16710    event event_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.event
    ADD CONSTRAINT event_pkey PRIMARY KEY (id);
 :   ALTER TABLE ONLY public.event DROP CONSTRAINT event_pkey;
       public         postgres    false    200            �           2606    16756    expense expenses_pkey 
   CONSTRAINT     [   ALTER TABLE ONLY public.expense
    ADD CONSTRAINT expenses_pkey PRIMARY KEY (id_expense);
 ?   ALTER TABLE ONLY public.expense DROP CONSTRAINT expenses_pkey;
       public         postgres    false    208            �           2606    16832    mailingList mailingList_pkey 
   CONSTRAINT     l   ALTER TABLE ONLY public."mailingList"
    ADD CONSTRAINT "mailingList_pkey" PRIMARY KEY ("id_mailingList");
 J   ALTER TABLE ONLY public."mailingList" DROP CONSTRAINT "mailingList_pkey";
       public         postgres    false    223            �           2606    16732    material material_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.material
    ADD CONSTRAINT material_pkey PRIMARY KEY (id_material);
 @   ALTER TABLE ONLY public.material DROP CONSTRAINT material_pkey;
       public         postgres    false    204            �           2606    16821    organization organization_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.organization
    ADD CONSTRAINT organization_pkey PRIMARY KEY (id_organization);
 H   ALTER TABLE ONLY public.organization DROP CONSTRAINT organization_pkey;
       public         postgres    false    221            �           2606    16721    room room_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY public.room
    ADD CONSTRAINT room_pkey PRIMARY KEY (id_room);
 8   ALTER TABLE ONLY public.room DROP CONSTRAINT room_pkey;
       public         postgres    false    202            �           2606    16781    speaker speaker_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.speaker
    ADD CONSTRAINT speaker_pkey PRIMARY KEY (id_speaker);
 >   ALTER TABLE ONLY public.speaker DROP CONSTRAINT speaker_pkey;
       public         postgres    false    213            �           2606    16810    task task_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY public.task
    ADD CONSTRAINT task_pkey PRIMARY KEY (id_task);
 8   ALTER TABLE ONLY public.task DROP CONSTRAINT task_pkey;
       public         postgres    false    219            �           2606    16767    theme themes_pkey 
   CONSTRAINT     U   ALTER TABLE ONLY public.theme
    ADD CONSTRAINT themes_pkey PRIMARY KEY (id_theme);
 ;   ALTER TABLE ONLY public.theme DROP CONSTRAINT themes_pkey;
       public         postgres    false    210            �           2606    16688    user users_pkey 
   CONSTRAINT     O   ALTER TABLE ONLY public."user"
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);
 ;   ALTER TABLE ONLY public."user" DROP CONSTRAINT users_pkey;
       public         postgres    false    197            �           1259    16838    fki_event-room    INDEX     H   CREATE INDEX "fki_event-room" ON public.expense USING btree (id_event);
 $   DROP INDEX public."fki_event-room";
       public         postgres    false    208            �           1259    16886    fki_event_conference    INDEX     _   CREATE INDEX fki_event_conference ON public."event-event" USING btree ("id-event_conference");
 (   DROP INDEX public.fki_event_conference;
       public         postgres    false    229            �           1259    17061    fki_frk-article_event    INDEX     O   CREATE INDEX "fki_frk-article_event" ON public.article USING btree (id_event);
 +   DROP INDEX public."fki_frk-article_event";
       public         postgres    false    216            �           1259    16865    fki_frk-event    INDEX     L   CREATE INDEX "fki_frk-event" ON public."room-event" USING btree (id_event);
 #   DROP INDEX public."fki_frk-event";
       public         postgres    false    226            �           1259    16880    fki_frk-event_salon    INDEX     [   CREATE INDEX "fki_frk-event_salon" ON public."event-event" USING btree ("id-event_salon");
 )   DROP INDEX public."fki_frk-event_salon";
       public         postgres    false    229            �           1259    16937    fki_frk-id_event    INDEX     R   CREATE INDEX "fki_frk-id_event" ON public."speaker-event" USING btree (id_event);
 &   DROP INDEX public."fki_frk-id_event";
       public         postgres    false    235            �           1259    16859    fki_frk-room    INDEX     J   CREATE INDEX "fki_frk-room" ON public."room-event" USING btree (id_room);
 "   DROP INDEX public."fki_frk-room";
       public         postgres    false    226            �           1259    17055    fki_frk-task_event    INDEX     I   CREATE INDEX "fki_frk-task_event" ON public.task USING btree (id_event);
 (   DROP INDEX public."fki_frk-task_event";
       public         postgres    false    219            �           1259    16907    fki_id_event    INDEX     J   CREATE INDEX fki_id_event ON public."theme-event" USING btree (id_event);
     DROP INDEX public.fki_id_event;
       public         postgres    false    232            �           1259    16990    fki_id_mailingList    INDEX     b   CREATE INDEX "fki_id_mailingList" ON public."speaker-mailingList" USING btree ("id_mailingList");
 (   DROP INDEX public."fki_id_mailingList";
       public         postgres    false    241            �           1259    17049    fki_id_material    INDEX     S   CREATE INDEX fki_id_material ON public."material-event" USING btree (id_material);
 #   DROP INDEX public.fki_id_material;
       public         postgres    false    247            �           1259    16970    fki_id_organization    INDEX     R   CREATE INDEX fki_id_organization ON public.speaker USING btree (id_organization);
 '   DROP INDEX public.fki_id_organization;
       public         postgres    false    213            �           1259    16943    fki_id_speaker    INDEX     P   CREATE INDEX fki_id_speaker ON public."speaker-event" USING btree (id_speaker);
 "   DROP INDEX public.fki_id_speaker;
       public         postgres    false    235            �           1259    16901    fki_id_theme    INDEX     J   CREATE INDEX fki_id_theme ON public."theme-event" USING btree (id_theme);
     DROP INDEX public.fki_id_theme;
       public         postgres    false    232            �           1259    16964    fki_id_users    INDEX     L   CREATE INDEX fki_id_users ON public."speaker-users" USING btree (id_users);
     DROP INDEX public.fki_id_users;
       public         postgres    false    238            �           1259    16844    fki_material-room    INDEX     K   CREATE INDEX "fki_material-room" ON public.material USING btree (id_room);
 '   DROP INDEX public."fki_material-room";
       public         postgres    false    204            �           2606    16881    event-event event_conference    FK CONSTRAINT     �   ALTER TABLE ONLY public."event-event"
    ADD CONSTRAINT event_conference FOREIGN KEY ("id-event_conference") REFERENCES public.event(id);
 H   ALTER TABLE ONLY public."event-event" DROP CONSTRAINT event_conference;
       public       postgres    false    2221    200    229            �           2606    16833    expense expense-room    FK CONSTRAINT     v   ALTER TABLE ONLY public.expense
    ADD CONSTRAINT "expense-room" FOREIGN KEY (id_event) REFERENCES public.event(id);
 @   ALTER TABLE ONLY public.expense DROP CONSTRAINT "expense-room";
       public       postgres    false    208    200    2221            �           2606    17056    article frk-article_event    FK CONSTRAINT     {   ALTER TABLE ONLY public.article
    ADD CONSTRAINT "frk-article_event" FOREIGN KEY (id_event) REFERENCES public.event(id);
 E   ALTER TABLE ONLY public.article DROP CONSTRAINT "frk-article_event";
       public       postgres    false    2221    216    200            �           2606    16860    room-event frk-event    FK CONSTRAINT     x   ALTER TABLE ONLY public."room-event"
    ADD CONSTRAINT "frk-event" FOREIGN KEY (id_event) REFERENCES public.event(id);
 B   ALTER TABLE ONLY public."room-event" DROP CONSTRAINT "frk-event";
       public       postgres    false    200    2221    226            �           2606    16875    event-event frk-event_salon    FK CONSTRAINT     �   ALTER TABLE ONLY public."event-event"
    ADD CONSTRAINT "frk-event_salon" FOREIGN KEY ("id-event_salon") REFERENCES public.event(id);
 I   ALTER TABLE ONLY public."event-event" DROP CONSTRAINT "frk-event_salon";
       public       postgres    false    229    200    2221            �           2606    16854    room-event frk-room    FK CONSTRAINT     z   ALTER TABLE ONLY public."room-event"
    ADD CONSTRAINT "frk-room" FOREIGN KEY (id_room) REFERENCES public.room(id_room);
 A   ALTER TABLE ONLY public."room-event" DROP CONSTRAINT "frk-room";
       public       postgres    false    202    2223    226            �           2606    17050    task frk-task_event    FK CONSTRAINT     u   ALTER TABLE ONLY public.task
    ADD CONSTRAINT "frk-task_event" FOREIGN KEY (id_event) REFERENCES public.event(id);
 ?   ALTER TABLE ONLY public.task DROP CONSTRAINT "frk-task_event";
       public       postgres    false    2221    200    219            �           2606    16927    speaker-event id_event    FK CONSTRAINT     x   ALTER TABLE ONLY public."speaker-event"
    ADD CONSTRAINT id_event FOREIGN KEY (id_event) REFERENCES public.event(id);
 B   ALTER TABLE ONLY public."speaker-event" DROP CONSTRAINT id_event;
       public       postgres    false    2221    235    200            �           2606    17025    theme-event id_event    FK CONSTRAINT     v   ALTER TABLE ONLY public."theme-event"
    ADD CONSTRAINT id_event FOREIGN KEY (id_event) REFERENCES public.event(id);
 @   ALTER TABLE ONLY public."theme-event" DROP CONSTRAINT id_event;
       public       postgres    false    200    2221    232            �           2606    17039    material-event id_event    FK CONSTRAINT     y   ALTER TABLE ONLY public."material-event"
    ADD CONSTRAINT id_event FOREIGN KEY (id_event) REFERENCES public.event(id);
 C   ALTER TABLE ONLY public."material-event" DROP CONSTRAINT id_event;
       public       postgres    false    200    247    2221            �           2606    16985 "   speaker-mailingList id_mailingList    FK CONSTRAINT     �   ALTER TABLE ONLY public."speaker-mailingList"
    ADD CONSTRAINT "id_mailingList" FOREIGN KEY ("id_mailingList") REFERENCES public."mailingList"("id_mailingList");
 P   ALTER TABLE ONLY public."speaker-mailingList" DROP CONSTRAINT "id_mailingList";
       public       postgres    false    2244    223    241            �           2606    17005 '   organization-mailingList id_mailingList    FK CONSTRAINT     �   ALTER TABLE ONLY public."organization-mailingList"
    ADD CONSTRAINT "id_mailingList" FOREIGN KEY ("id_mailingList") REFERENCES public."mailingList"("id_mailingList");
 U   ALTER TABLE ONLY public."organization-mailingList" DROP CONSTRAINT "id_mailingList";
       public       postgres    false    2244    223    244            �           2606    17044    material-event id_material    FK CONSTRAINT     �   ALTER TABLE ONLY public."material-event"
    ADD CONSTRAINT id_material FOREIGN KEY (id_material) REFERENCES public.material(id_material);
 F   ALTER TABLE ONLY public."material-event" DROP CONSTRAINT id_material;
       public       postgres    false    204    247    2226            �           2606    16965    speaker id_organization    FK CONSTRAINT     �   ALTER TABLE ONLY public.speaker
    ADD CONSTRAINT id_organization FOREIGN KEY (id_organization) REFERENCES public.organization(id_organization);
 A   ALTER TABLE ONLY public.speaker DROP CONSTRAINT id_organization;
       public       postgres    false    213    221    2242            �           2606    17000 (   organization-mailingList id_organization    FK CONSTRAINT     �   ALTER TABLE ONLY public."organization-mailingList"
    ADD CONSTRAINT id_organization FOREIGN KEY (id_organization) REFERENCES public.organization(id_organization);
 T   ALTER TABLE ONLY public."organization-mailingList" DROP CONSTRAINT id_organization;
       public       postgres    false    2242    221    244            �           2606    16938    speaker-event id_speaker    FK CONSTRAINT     �   ALTER TABLE ONLY public."speaker-event"
    ADD CONSTRAINT id_speaker FOREIGN KEY (id_speaker) REFERENCES public.speaker(id_speaker);
 D   ALTER TABLE ONLY public."speaker-event" DROP CONSTRAINT id_speaker;
       public       postgres    false    235    2234    213            �           2606    16954    speaker-users id_speaker    FK CONSTRAINT     �   ALTER TABLE ONLY public."speaker-users"
    ADD CONSTRAINT id_speaker FOREIGN KEY (id_speaker) REFERENCES public.speaker(id_speaker);
 D   ALTER TABLE ONLY public."speaker-users" DROP CONSTRAINT id_speaker;
       public       postgres    false    238    2234    213            �           2606    16980    speaker-mailingList id_speaker    FK CONSTRAINT     �   ALTER TABLE ONLY public."speaker-mailingList"
    ADD CONSTRAINT id_speaker FOREIGN KEY (id_speaker) REFERENCES public.speaker(id_speaker);
 J   ALTER TABLE ONLY public."speaker-mailingList" DROP CONSTRAINT id_speaker;
       public       postgres    false    241    213    2234            �           2606    16896    theme-event id_theme    FK CONSTRAINT     |   ALTER TABLE ONLY public."theme-event"
    ADD CONSTRAINT id_theme FOREIGN KEY (id_theme) REFERENCES public.theme(id_theme);
 @   ALTER TABLE ONLY public."theme-event" DROP CONSTRAINT id_theme;
       public       postgres    false    2231    210    232            �           2606    16959    speaker-users id_users    FK CONSTRAINT     y   ALTER TABLE ONLY public."speaker-users"
    ADD CONSTRAINT id_users FOREIGN KEY (id_users) REFERENCES public."user"(id);
 B   ALTER TABLE ONLY public."speaker-users" DROP CONSTRAINT id_users;
       public       postgres    false    238    197    2219            �           2606    16839    material material-room    FK CONSTRAINT     {   ALTER TABLE ONLY public.material
    ADD CONSTRAINT "material-room" FOREIGN KEY (id_room) REFERENCES public.room(id_room);
 B   ALTER TABLE ONLY public.material DROP CONSTRAINT "material-room";
       public       postgres    false    2223    204    202            r	   2   x�3�4��/-*,��TH�,Q8�<Q�8��X�P���Ǚ_������ I=b      b	   �   x�e�A
�0D�?��+WV��� nz�nJ�ǘI�;y/f��a`��7�@��Ri��@�xE7d�9Y���� y)z�ز�8:\\L,!p���vZ�נ����b�.�j�r�������PN�>��3Hf �㶠'���37{      	      x�3�4�2�4����� P�      j	   (   x�3�4�44�300�N�I�,JUHIUp�I������ y=�      y	      x�3�L������� 9&      f	   "   x�3�4�tN�M-J�2�4�IL�I����� R��      �	      x�3�4�2�4�2cC�=... ��      w	   O   x�3�,�4405��/(J.I-�L�L��+IL.q(����Ҋ��8�8�*sKRKRS󊁬�̜T�z��=... \#	      �	      x�3�4�2�=... ��      d	      x�3��H��I�2�440������ +
�      |	      x������ � �      o	   \   x�=�1
�0��99L�=A]����	��JRoo\ܾދ�h��h�jo
�p�������KE8�EZ������
�+[�Tl@�|�-0���"S      �	      x������ � �      �	      x�3�4�2�=... ��      �	      x�3�4�2�4����� ��      u	   W   x�Ʊ�0�:��'���(�i��B�;a~���4��մ��舛大��.����ӉY��������*��/=�TE��9�j�      l	      x�3�L�,�2�t�O�N-����� 6��      �	      x�3�4�2�4�2�=... f      _	   4   x�3�,��M���+K�L�LuH-(�,I���K+��p��e8���b���� Њ     