package fr.psytalknantes.psytalknantesserver.repository;

import fr.psytalknantes.psytalknantesserver.repository.dao.ExpensesDAO;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.*;
public class ExpensesRepository {

    public List<ExpensesDAO> getDAOExpenses() {
        final Logger logger = LogManager.getLogger(ExpensesRepository.class);
        logger.info("Get expenses : on.");
        List<ExpensesDAO> ExpensesDaos = new ArrayList<ExpensesDAO>();

        /*
            Here we call the Repository to get the Expensess
            The repo return a ResultSet that we interpret to get the data
        */
        try
        {
            final ResultSet rs = Connect.Query("Select * from expense;");
            while(rs.next()) {
                ExpensesDAO ExpensesDao = new ExpensesDAO(){{
                    this.setId(rs.getInt("id_expense"));
                    this.setName(rs.getString("name"));
                    this.setReceipt(rs.getString("receipt"));
                }};
                ExpensesDaos.add(ExpensesDao);

            }
            logger.info("Get expenses : done.");
            return ExpensesDaos;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            logger.error("Get expenses : failed.");
            return null;
        }
    }

}

