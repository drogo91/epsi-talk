package fr.psytalknantes.psytalknantesserver.repository;

import fr.psytalknantes.psytalknantesserver.repository.dao.EventDAO;
import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.*;
public class EventRepository {

    public List<EventDAO> getDAOEvent() {
        final Logger logger = LogManager.getLogger(EventRepository.class);
        logger.info("Get events : on.");
        List<EventDAO> EventDaos = new ArrayList<EventDAO>();
        /*
            Here we call the Repository to get the Events
            The repo return a ResultSet that we interpret to get the data
        */
        try
        {
            final ResultSet rs = Connect.Query("Select * from event;");
            while(rs.next()) {
                EventDAO EventDao = new EventDAO(){{
                    this.setId_event(rs.getInt("id"));
                    this.setId_speaker(rs.getInt("id_speaker"));
                    this.setDateStart(rs.getString("dateStart"));
                    this.setDateEnd(rs.getString("dateEnd"));
                    this.setDescription(rs.getString("description"));
                    this.setHourStart(rs.getString("HourStart"));
                    this.setHourEnd(rs.getString("HourEnd"));
                    this.setName(rs.getString("name"));
                }};
                EventDaos.add(EventDao);

            }
            logger.info("Get events : done.");
            return EventDaos;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            logger.error("Get events : failed.");
            return null;
        }
    }

    public void postEvent(String req){
        JSONObject params = new JSONObject(req);
        if (!params.isNull("speaker")) {
            JSONObject speaker = params.getJSONObject("speaker");
                    Connect.voidQuery("INSERT INTO public.Event(" +
                            "\"dateStart\", id_speaker, name, description, \"hourStart\", \"hourEnd\")" +
                            " VALUES ('" +
                            params.get("dateStart") + "', " +
                            speaker.get("id_speaker") + ", '" +
                            params.get("name") + "', '" +
                            params.get("description") + "', '" +
                            params.get("hourStart") + "', '" +
                            params.get("hourEnd") + "');");
        } else {
            Connect.voidQuery("INSERT INTO public.Event(" +
                    "\"dateStart\", \"dateEnd\", name, description, \"hourStart\", \"hourEnd\")" +
                    " VALUES ('" +
                    params.get("dateStart") + "', '" +
                    params.get("dateEnd") + "', '" +
                    params.get("name") + "', '" +
                    params.get("description") + "', '" +
                    params.get("hourStart") + "', '" +
                    params.get("hourEnd") + "');");
        }
    }

    public void putEvent(String req){
        JSONObject params = new JSONObject(req);
        String dateEnd;
        if(!params.isNull("dateEnd")) {
            dateEnd = "', \"dateEnd\"='" + params.get("dateEnd");
        } else {
            dateEnd = "";
        }
        if (!params.isNull("id_speaker")) {
            JSONObject speaker = params.getJSONObject("id_speaker");
            Connect.voidQuery("UPDATE public.Event " +
                    "SET id= " + params.get("id_event") +
                    ", name='" + params.get("name") +
                    "', description='" + params.get("description") +
                    "', \"dateStart\"='" + params.get("dateStart") +
                    dateEnd +
                    "', id_speaker=" + speaker.get("id_speaker") +
                    ", \"hourStart\"='" + params.get("hourStart") +
                    "', \"hourEnd\"='" + params.get("hourEnd") + "' " +
                    "WHERE id=" + params.get("id_event") + ";");
        } else {
            Connect.voidQuery("UPDATE public.Event " +
                    "SET id= " + params.get("id_event") +
                    ", name='" + params.get("name") +
                    "', description='" + params.get("description") +
                    "', \"dateStart\"='" + params.get("dateStart") +
                    dateEnd +
                    ", \"hourStart\"='" + params.get("hourStart") +
                    "', \"hourEnd\"='" + params.get("hourEnd") + "' " +
                    "WHERE id=" + params.get("id_event") + ";");
        }
    }

    public void deleteEvent(int id){
        Connect.voidQuery("DELETE FROM public.Event " +
                "WHERE id=" + id + ";");
    }

}

