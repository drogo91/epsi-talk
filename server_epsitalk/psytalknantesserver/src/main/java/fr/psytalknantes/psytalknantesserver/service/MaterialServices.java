package fr.psytalknantes.psytalknantesserver.service;

import fr.psytalknantes.psytalknantesserver.repository.dao.MaterialDAO;
import fr.psytalknantes.psytalknantesserver.repository.MaterialRepository;
import fr.psytalknantes.psytalknantesserver.service.converter.MaterialConverter;
import fr.psytalknantes.psytalknantesserver.service.dto.MaterialDTO;

import java.util.List;

public class MaterialServices {

    MaterialRepository _MaterialRepository = new MaterialRepository();
    MaterialConverter _MaterialConverter = new MaterialConverter();

    public List<MaterialDTO> getAllMaterial() {
        List<MaterialDAO> MaterialDaos = this._MaterialRepository.getDAOMaterial();
        return this._MaterialConverter.ConvertMaterial(MaterialDaos);
    }

}
