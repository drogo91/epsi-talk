package fr.psytalknantes.psytalknantesserver.service.converter;

import java.util.ArrayList;
import java.util.List;
import fr.psytalknantes.psytalknantesserver.repository.dao.TaskDAO;
import fr.psytalknantes.psytalknantesserver.service.dto.TaskDTO;

public class TaskConverter {

    public List<TaskDTO> ConvertTask(List<TaskDAO> TaskDaos) {
        List<TaskDTO> TaskDtos = new ArrayList<TaskDTO>();
        for(final TaskDAO TaskDao : TaskDaos){
            TaskDTO TaskDto = new TaskDTO(){{
               this.setId(TaskDao.getId());
               this.setContaint(TaskDao.getContaint());
                this.setTitle(TaskDao.getTitle());
            }};
            TaskDtos.add(TaskDto);
        }

        return TaskDtos;
    }

}
