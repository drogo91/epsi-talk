package fr.psytalknantes.psytalknantesserver.repository;

import fr.psytalknantes.psytalknantesserver.repository.dao.TaskDAO;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.*;
public class TaskRepository {

    public List<TaskDAO> getDAOTask() {
        final Logger logger = LogManager.getLogger(TaskRepository.class);
        logger.info("Get tasks : on.");
        List<TaskDAO> TaskDaos = new ArrayList<TaskDAO>();

        /*
            Here we call the Repository to get the Tasks
            The repo return a ResultSet that we interpret to get the data
        */
        try
        {
            final ResultSet rs = Connect.Query("Select * from task;");
            while(rs.next()) {
                TaskDAO TaskDao = new TaskDAO(){{
                    this.setId(rs.getInt("id_Task"));
                    this.setContaint(rs.getString("containt"));
                    this.setTitle(rs.getString("title"));
                }};
                TaskDaos.add(TaskDao);

            }
            logger.info("Get tasks : done.");
            return TaskDaos;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            logger.error("Get tasks : failed.");
            return null;
        }
    }

}

