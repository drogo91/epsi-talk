package fr.psytalknantes.psytalknantesserver.service.converter;

import java.util.ArrayList;
import java.util.List;
import fr.psytalknantes.psytalknantesserver.repository.dao.MailingListDAO;
import fr.psytalknantes.psytalknantesserver.service.dto.MailingListDTO;

public class MailingListConverter {

    public List<MailingListDTO> ConvertMailingList(List<MailingListDAO> MailingListDaos){
        List<MailingListDTO> MailingListDtos = new ArrayList<MailingListDTO>();
        for(final MailingListDAO MailingListDao : MailingListDaos) {
            MailingListDTO MailingListDto = new MailingListDTO(){{
               this.setId(MailingListDao.getId());
               this.setTitle(MailingListDao.getTitle());
            }};
            MailingListDtos.add(MailingListDto);
        }

        return MailingListDtos;
    }

}
