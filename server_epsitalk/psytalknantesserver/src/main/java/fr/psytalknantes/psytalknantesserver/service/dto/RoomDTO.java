package fr.psytalknantes.psytalknantesserver.service.dto;

import lombok.Data;

@Data
public class RoomDTO {

    private int id;
    private String name;

}
