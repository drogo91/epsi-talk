package fr.psytalknantes.psytalknantesserver.repository;

import fr.psytalknantes.psytalknantesserver.repository.dao.ArticleDAO;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.*;
public class ArticleRepository {

    public List<ArticleDAO> getDAOArticle() {
        final Logger logger = LogManager.getLogger(ArticleRepository.class);
        logger.info("Get articles : on.");
        List<ArticleDAO> ArticleDaos = new ArrayList<ArticleDAO>();

        /*
            Here we call the Repository to get the Articles
            The repo return a ResultSet that we interpret to get the data
        */
        try
        {
            final ResultSet rs = Connect.Query("Select * from article;");
            while(rs.next()) {
                ArticleDAO ArticleDao = new ArticleDAO(){{
                    this.setId(rs.getInt("id_article"));
                    this.setTitle(rs.getString("title"));
                    this.setContaint(rs.getString("containt"));
                }};
                ArticleDaos.add(ArticleDao);

            }
            logger.info("Get articles : done.");
            return ArticleDaos;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            logger.error("Get articles : failed.");
            return null;
        }
    }

}

