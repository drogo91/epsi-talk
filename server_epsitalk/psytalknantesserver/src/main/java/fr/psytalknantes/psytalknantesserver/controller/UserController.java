package fr.psytalknantes.psytalknantesserver.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import fr.psytalknantes.psytalknantesserver.service.dto.UserDTO;
import fr.psytalknantes.psytalknantesserver.service.UserServices;
import java.util.List;

@Api( description="User class, GET")
@RestController
public class UserController {

    UserServices _UserServices = new UserServices();

    @ApiOperation(value = "Get all users")
    @CrossOrigin(origins = "*" , allowedHeaders = "*")
    @RequestMapping(value="/users", method= RequestMethod.GET)
    public List<UserDTO> getUsers() {
        return this._UserServices.getAllUsers();
    }

}
