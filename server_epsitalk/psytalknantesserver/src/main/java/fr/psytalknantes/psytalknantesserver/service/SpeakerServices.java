package fr.psytalknantes.psytalknantesserver.service;

import fr.psytalknantes.psytalknantesserver.repository.dao.SpeakerDAO;
import fr.psytalknantes.psytalknantesserver.repository.SpeakerRepository;
import fr.psytalknantes.psytalknantesserver.service.converter.SpeakerConverter;
import fr.psytalknantes.psytalknantesserver.service.dto.SpeakerDTO;

import java.util.List;

public class SpeakerServices {

    SpeakerRepository _SpeakerRepository = new SpeakerRepository();
    SpeakerConverter _SpeakerConverter = new SpeakerConverter();

    public List<SpeakerDTO> getAllSpeaker() {
        List<SpeakerDAO> SpeakerDaos = this._SpeakerRepository.getDAOSpeaker();
        return this._SpeakerConverter.ConvertSpeaker(SpeakerDaos);
    }

    public SpeakerDTO getSpeaker(final int id) {
        SpeakerDAO speakerDao = this._SpeakerRepository.getDaoSpeaker(id);
        return this._SpeakerConverter.ConvertSpeaker(speakerDao);
    }

    public void postSpeaker(String req){
        this._SpeakerRepository.postSpeaker(req);
    }

    public void putSpeaker(String req){
        this._SpeakerRepository.putSpeaker(req);
    }

    public void deleteSpeaker(int id){
        this._SpeakerRepository.deleteSpeaker(id);
    }

}
