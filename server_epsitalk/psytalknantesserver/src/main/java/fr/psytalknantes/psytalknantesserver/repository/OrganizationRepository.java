package fr.psytalknantes.psytalknantesserver.repository;

import fr.psytalknantes.psytalknantesserver.repository.dao.OrganizationDAO;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.*;
public class OrganizationRepository {

    public List<OrganizationDAO> getDAOOrganization() {
        final Logger logger = LogManager.getLogger(OrganizationRepository.class);
        logger.info("Get organizations : on.");
        List<OrganizationDAO> OrganizationDaos = new ArrayList<OrganizationDAO>();

        /*
            Here we call the Repository to get the Organizations
            The repo return a ResultSet that we interpret to get the data
        */
        try
        {
            final ResultSet rs = Connect.Query("Select * from organization;");
            while(rs.next()) {
                OrganizationDAO OrganizationDao = new OrganizationDAO(){{
                    this.setId_organization(rs.getInt("id_Organization"));
                    this.setName(rs.getString("name"));
                    this.setEffective(rs.getInt("effective"));
                    this.setMail(rs.getString("mail"));
                    this.setSponsor(rs.getString("sponsor"));
                }};
                OrganizationDaos.add(OrganizationDao);

            }
            logger.info("Get organizations : done.");
            return OrganizationDaos;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            logger.error("Get organizations : failed.");
            return null;
        }
    }

    public OrganizationDAO getDAOOrganization(int id) {
        OrganizationDAO OrganizationDao = new OrganizationDAO();

        /*
            Here we call the Repository to get the Organizations
            The repo return a ResultSet that we interpret to get the data
        */
        try
        {
            final ResultSet rs = Connect.Query("Select * from organization where id_organization=" + id +";");
            while(rs.next()) {
                OrganizationDao.setId_organization(rs.getInt("id_Organization"));
                OrganizationDao.setName(rs.getString("name"));
                OrganizationDao.setEffective(rs.getInt("effective"));
                OrganizationDao.setMail(rs.getString("mail"));
                OrganizationDao.setSponsor(rs.getString("sponsor"));
            }
            return OrganizationDao;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            return null;
        }
    }

}

