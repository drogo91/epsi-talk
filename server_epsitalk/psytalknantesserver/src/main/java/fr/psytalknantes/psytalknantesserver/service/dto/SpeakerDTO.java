package fr.psytalknantes.psytalknantesserver.service.dto;

import lombok.Data;

@Data
public class SpeakerDTO {

    private int id_speaker;
    private String name;
    private String surname;
    private String mail;
    private OrganizationDTO organization;

}
