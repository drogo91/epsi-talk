package fr.psytalknantes.psytalknantesserver.service.converter;

import java.util.ArrayList;
import java.util.List;
import fr.psytalknantes.psytalknantesserver.repository.dao.ThemeDAO;
import fr.psytalknantes.psytalknantesserver.service.dto.ThemeDTO;

public class ThemeConverter {

    public List<ThemeDTO> ConvertTheme(List<ThemeDAO> ThemeDaos) {
        List<ThemeDTO> ThemeDtos = new ArrayList<ThemeDTO>();
        for(final ThemeDAO ThemeDao : ThemeDaos){
            ThemeDTO ThemeDto = new ThemeDTO(){{
               this.setId(ThemeDao.getId());
               this.setName(ThemeDao.getName());
            }};
            ThemeDtos.add(ThemeDto);
        }

        return ThemeDtos;
    }

}
