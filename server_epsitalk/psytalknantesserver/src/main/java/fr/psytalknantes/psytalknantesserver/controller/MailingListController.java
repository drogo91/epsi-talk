package fr.psytalknantes.psytalknantesserver.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import fr.psytalknantes.psytalknantesserver.service.dto.MailingListDTO;
import fr.psytalknantes.psytalknantesserver.service.MailingListServices;
import java.util.List;

@Api( description="MailingList class, GET")
@RestController
public class MailingListController {

    MailingListServices _MailingListServices = new MailingListServices();

    @ApiOperation(value = "Get all mailingLists")
    @CrossOrigin(origins = "*" , allowedHeaders = "*")
    @RequestMapping(value="/mailingLists", method= RequestMethod.GET)
    public List<MailingListDTO> getMailingLists() {
        return this._MailingListServices.getAllMailingList();
    }

}
