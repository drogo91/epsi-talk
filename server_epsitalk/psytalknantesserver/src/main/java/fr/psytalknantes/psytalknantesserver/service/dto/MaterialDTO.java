package fr.psytalknantes.psytalknantesserver.service.dto;

import lombok.Data;

@Data
public class MaterialDTO {

    private int id;
    private String name;

}
