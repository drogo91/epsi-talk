package fr.psytalknantes.psytalknantesserver.service;

import fr.psytalknantes.psytalknantesserver.repository.dao.ExpensesDAO;
import fr.psytalknantes.psytalknantesserver.repository.ExpensesRepository;
import fr.psytalknantes.psytalknantesserver.service.converter.ExpensesConverter;
import fr.psytalknantes.psytalknantesserver.service.dto.ExpensesDTO;

import java.util.List;

public class ExpensesServices {

    ExpensesRepository _ExpensesRepository = new ExpensesRepository();
    ExpensesConverter _ExpensesConverter = new ExpensesConverter();

    public List<ExpensesDTO> getAllExpenses() {
        List<ExpensesDAO> ExpensesDaos = this._ExpensesRepository.getDAOExpenses();
        return this._ExpensesConverter.ConvertExpenses(ExpensesDaos);
    }

}
