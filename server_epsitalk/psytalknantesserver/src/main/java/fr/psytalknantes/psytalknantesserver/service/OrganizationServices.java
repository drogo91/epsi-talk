package fr.psytalknantes.psytalknantesserver.service;

import fr.psytalknantes.psytalknantesserver.repository.dao.OrganizationDAO;
import fr.psytalknantes.psytalknantesserver.repository.OrganizationRepository;
import fr.psytalknantes.psytalknantesserver.service.converter.OrganizationConverter;
import fr.psytalknantes.psytalknantesserver.service.dto.OrganizationDTO;

import java.util.List;

public class OrganizationServices {

    OrganizationRepository _OrganizationRepository = new OrganizationRepository();
    OrganizationConverter _OrganizationConverter = new OrganizationConverter();

    public List<OrganizationDTO> getAllOrganization() {
        List<OrganizationDAO> OrganizationDaos = this._OrganizationRepository.getDAOOrganization();
        return this._OrganizationConverter.ConvertOrganization(OrganizationDaos);
    }

    public OrganizationDTO getOrganization(int id) {
        OrganizationDAO OrganizationDao = this._OrganizationRepository.getDAOOrganization(id);
        return this._OrganizationConverter.ConvertOrganization(OrganizationDao);
    }

}
