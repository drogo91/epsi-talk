package fr.psytalknantes.psytalknantesserver.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import fr.psytalknantes.psytalknantesserver.service.dto.TaskDTO;
import fr.psytalknantes.psytalknantesserver.service.TaskServices;
import java.util.List;

@Api( description="Task class, GET")
@RestController
public class TaskController {

    TaskServices _TaskServices = new TaskServices();

    @ApiOperation(value = "Get all tasks")
    @CrossOrigin(origins = "*" , allowedHeaders = "*")
    @RequestMapping(value="/tasks", method= RequestMethod.GET)
    public List<TaskDTO> getTasks() {
        return this._TaskServices.getAllTask();
    }

}
