package fr.psytalknantes.psytalknantesserver.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import fr.psytalknantes.psytalknantesserver.service.dto.ThemeDTO;
import fr.psytalknantes.psytalknantesserver.service.ThemeServices;
import java.util.List;

@Api( description="Theme class, GET")
@RestController
public class ThemeController {

    ThemeServices _ThemeServices = new ThemeServices();

    @ApiOperation(value = "Get all themes")
    @CrossOrigin(origins = "*" , allowedHeaders = "*")
    @RequestMapping(value="/themes", method= RequestMethod.GET)
    public List<ThemeDTO> getThemes() {
        return this._ThemeServices.getAllTheme();
    }

}
