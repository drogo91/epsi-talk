package fr.psytalknantes.psytalknantesserver.service.dto;

import lombok.Data;

@Data
public class ExpensesDTO {

    private int id;
    private String name;
    private String receipt;

}
