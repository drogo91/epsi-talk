package fr.psytalknantes.psytalknantesserver.service;

import fr.psytalknantes.psytalknantesserver.repository.dao.EventDAO;
import fr.psytalknantes.psytalknantesserver.repository.EventRepository;
import fr.psytalknantes.psytalknantesserver.service.converter.EventConverter;
import fr.psytalknantes.psytalknantesserver.service.dto.EventDTO;

import java.util.List;

public class EventServices {

    EventRepository _EventRepository = new EventRepository();
    EventConverter _EventConverter = new EventConverter();

    public List<EventDTO> getAllEvent() {
        List<EventDAO> EventDaos = this._EventRepository.getDAOEvent();
        return this._EventConverter.ConvertEvent(EventDaos);
    }

    public void postEvent(String req){
        this._EventRepository.postEvent(req);
    }

    public void putEvent(final String req){
        this._EventRepository.putEvent(req);
    }

    public void deleteEvent(final int id){
        this._EventRepository.deleteEvent(id);
    }

}
