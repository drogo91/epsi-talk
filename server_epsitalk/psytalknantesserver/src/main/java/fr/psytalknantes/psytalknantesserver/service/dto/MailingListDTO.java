package fr.psytalknantes.psytalknantesserver.service.dto;

import lombok.Data;

@Data
public class MailingListDTO {

    private int id;
    private String title;

}
