package fr.psytalknantes.psytalknantesserver.service.converter;

import fr.psytalknantes.psytalknantesserver.repository.dao.RoomDAO;
import fr.psytalknantes.psytalknantesserver.service.dto.RoomDTO;
import java.util.ArrayList;
import java.util.List;

public class RoomConverter {

    public List<RoomDTO> ConvertRoom(List<RoomDAO> roomDaos){
        List<RoomDTO> roomDtos = new ArrayList<RoomDTO>();
        for(final RoomDAO roomDao : roomDaos) {
            RoomDTO roomDto = new RoomDTO(){{
                this.setId(roomDao.getId());
                this.setName(roomDao.getName());
            }};
            roomDtos.add(roomDto);
        }

        return roomDtos;
    }

}
