package fr.psytalknantes.psytalknantesserver.service.converter;

import java.util.ArrayList;
import java.util.List;
import fr.psytalknantes.psytalknantesserver.repository.dao.OrganizationDAO;
import fr.psytalknantes.psytalknantesserver.service.dto.OrganizationDTO;

public class OrganizationConverter {

    public List<OrganizationDTO> ConvertOrganization(List<OrganizationDAO> OrganizationDaos){
        List<OrganizationDTO> OrganizationDtos = new ArrayList<OrganizationDTO>();
        for(final OrganizationDAO OrganizationDao : OrganizationDaos) {
            OrganizationDTO OrganizationDto = new OrganizationDTO(){{
               this.setEffective(OrganizationDao.getEffective());
               this.setId_organization(OrganizationDao.getId_organization());
               this.setName(OrganizationDao.getName());
               this.setSponsor(OrganizationDao.getSponsor());
               this.setMail(OrganizationDao.getMail());
            }};
            OrganizationDtos.add(OrganizationDto);
        }

        return OrganizationDtos;
    }

    public OrganizationDTO ConvertOrganization(OrganizationDAO OrganizationDao) {
        OrganizationDTO OrganizationDto = new OrganizationDTO(){{
            this.setName(OrganizationDao.getName());
            this.setMail(OrganizationDao.getMail());
            this.setId_organization(OrganizationDao.getId_organization());
            this.setEffective(OrganizationDao.getEffective());
            this.setSponsor(OrganizationDao.getSponsor());
        }};

        return OrganizationDto;
    }

}
