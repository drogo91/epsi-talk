package fr.psytalknantes.psytalknantesserver.service.converter;

import java.util.ArrayList;
import java.util.List;
import fr.psytalknantes.psytalknantesserver.repository.dao.UserDAO;
import fr.psytalknantes.psytalknantesserver.service.dto.UserDTO;

public class UserConverter {

    public List<UserDTO> ConvertUser(List<UserDAO> userDaos) {
        List<UserDTO> userDtos = new ArrayList<UserDTO>();
        for(final UserDAO userDao : userDaos){
            UserDTO userDto = new UserDTO(){{
               this.setSurname(userDao.getSurname());
               this.setId(userDao.getId());
               this.setName(userDao.getName());
               this.setPassword(userDao.getPassword());
               this.setMail(userDao.getMail());
            }};
            userDtos.add(userDto);
        }

        return userDtos;
    }
    
}
