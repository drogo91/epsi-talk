package fr.psytalknantes.psytalknantesserver.repository;

import fr.psytalknantes.psytalknantesserver.repository.dao.RoomDAO;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.*;
public class RoomRepository {

    public List<RoomDAO> getDAORoom() {
        final Logger logger = LogManager.getLogger(RoomRepository.class);
        logger.info("Get rooms : on.");
        List<RoomDAO> RoomDaos = new ArrayList<RoomDAO>();

        /*
            Here we call the Repository to get the Rooms
            The repo return a ResultSet that we interpret to get the data
        */
        try
        {
            final ResultSet rs = Connect.Query("Select * from room;");
            while(rs.next()) {
                RoomDAO roomDAO= new RoomDAO(){{
                    this.setId(rs.getInt("id_room"));
                    this.setName(rs.getString("name"));
                }};
                RoomDaos.add(roomDAO);

            }
            logger.info("Get rooms : done.");
            return RoomDaos;
        }
        catch (SQLException e)
        {
            e.printStackTrace();

            logger.error("Get rooms : failed.");
            return null;
        }
    }

}
