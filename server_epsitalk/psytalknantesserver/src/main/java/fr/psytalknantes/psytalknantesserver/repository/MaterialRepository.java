package fr.psytalknantes.psytalknantesserver.repository;

import fr.psytalknantes.psytalknantesserver.repository.dao.MaterialDAO;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.*;
public class MaterialRepository {

    public List<MaterialDAO> getDAOMaterial() {
        final Logger logger = LogManager.getLogger(MaterialRepository.class);
        logger.info("Get materials : on.");
        List<MaterialDAO> MaterialDaos = new ArrayList<MaterialDAO>();

        /*
            Here we call the Repository to get the Materials
            The repo return a ResultSet that we interpret to get the data
        */
        try
        {
            final ResultSet rs = Connect.Query("Select * from material;");
            while(rs.next()) {
                MaterialDAO MaterialDao = new MaterialDAO(){{
                    this.setId(rs.getInt("id_Material"));
                    this.setName(rs.getString("name"));
                }};
                MaterialDaos.add(MaterialDao);

            }
            logger.info("Get materials : done.");
            return MaterialDaos;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            logger.error("Get materials : failed.");
            return null;
        }
    }

}

