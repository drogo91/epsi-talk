package fr.psytalknantes.psytalknantesserver.repository;

import fr.psytalknantes.psytalknantesserver.repository.dao.UserDAO;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.*;
public class UserRepository {
    
    public List<UserDAO> getDAOUser() {
        final Logger logger = LogManager.getLogger(UserRepository.class);

        List<UserDAO> userDaos = new ArrayList<UserDAO>();
        logger.info("Get users : on.");
        /*
            Here we call the Repository to get the users
            The repo return a ResultSet that we interpret to get the data
        */
        try
        {
            final ResultSet rs = Connect.Query("Select * from Public.user;");
            while(rs.next()) {
                UserDAO userDao = new UserDAO(){{
                    this.setId(rs.getInt("id"));
                    this.setName(rs.getString("name"));
                    this.setSurname(rs.getString("surname"));
                    this.setMail(rs.getString("mail"));
                    this.setPassword(rs.getString("password"));
                }};
                userDaos.add(userDao);

            }
            logger.info("Get users : done.");
            return userDaos;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            logger.error("Get users : failed.");
            return null;
        }
    }

}

