package fr.psytalknantes.psytalknantesserver.service.dto;

import lombok.Data;

@Data
public class OrganizationDTO {

    private int id_organization;
    private String sponsor;
    private int effective;
    private String name;
    private String mail;

}
