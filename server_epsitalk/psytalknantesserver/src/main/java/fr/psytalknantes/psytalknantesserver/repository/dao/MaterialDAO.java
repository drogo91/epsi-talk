package fr.psytalknantes.psytalknantesserver.repository.dao;

import lombok.Data;

@Data
public class MaterialDAO {

    private int id;
    private String name;

}
