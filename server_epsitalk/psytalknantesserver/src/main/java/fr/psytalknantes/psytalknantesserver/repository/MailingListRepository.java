package fr.psytalknantes.psytalknantesserver.repository;

import fr.psytalknantes.psytalknantesserver.repository.dao.MailingListDAO;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.*;
public class MailingListRepository {

    public List<MailingListDAO> getDAOMailingList() {
        final Logger logger = LogManager.getLogger(MaterialRepository.class);
        logger.info("Get mailing lists : on.");
        List<MailingListDAO> MailingListDaos = new ArrayList<MailingListDAO>();

        /*
            Here we call the Repository to get the MailingLists
            The repo return a Re1 sultSet that we interpret to get the data
        */
        try
        {
            final ResultSet rs = Connect.Query("Select * from \"mailingList\";");
            while(rs.next()) {
                MailingListDAO MailingListDao = new MailingListDAO(){{
                    this.setId(rs.getInt("id_mailingList"));
                    this.setTitle(rs.getString("title"));
                }};
                MailingListDaos.add(MailingListDao);

            }
            logger.info("Get mailing lists : done.");
            return MailingListDaos;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            logger.error("Get mailing lists : failed.");
            return null;
        }
    }

}

