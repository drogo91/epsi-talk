package fr.psytalknantes.psytalknantesserver.service;

import fr.psytalknantes.psytalknantesserver.repository.dao.ArticleDAO;
import fr.psytalknantes.psytalknantesserver.repository.ArticleRepository;
import fr.psytalknantes.psytalknantesserver.service.converter.ArticleConverter;
import fr.psytalknantes.psytalknantesserver.service.dto.ArticleDTO;

import java.util.List;

public class ArticleServices {

    ArticleRepository _ArticleRepository = new ArticleRepository();
    ArticleConverter _ArticleConverter = new ArticleConverter();

    public List<ArticleDTO> getAllArticle() {
        List<ArticleDAO> ArticleDaos = this._ArticleRepository.getDAOArticle();
        return this._ArticleConverter.ConvertArticle(ArticleDaos);
    }

}
