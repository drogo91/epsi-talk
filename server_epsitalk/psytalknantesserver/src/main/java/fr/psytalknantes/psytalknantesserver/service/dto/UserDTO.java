package fr.psytalknantes.psytalknantesserver.service.dto;

import lombok.Data;

@Data
public class UserDTO {

    private int id;
    private String name;
    private String surname;
    private String password;
    private String mail;

}
