package fr.psytalknantes.psytalknantesserver.service.converter;

import java.util.ArrayList;
import java.util.List;
import fr.psytalknantes.psytalknantesserver.repository.dao.EventDAO;
import fr.psytalknantes.psytalknantesserver.repository.dao.SpeakerDAO;
import fr.psytalknantes.psytalknantesserver.service.SpeakerServices;
import fr.psytalknantes.psytalknantesserver.service.dto.EventDTO;

public class EventConverter {

    public List<EventDTO> ConvertEvent(List<EventDAO> EventDaos) {
        List<EventDTO> EventDtos = new ArrayList<EventDTO>();
        for(final EventDAO EventDao : EventDaos) {
            SpeakerServices _SpeakerServices = new SpeakerServices();
            EventDTO EventDto = new EventDTO(){{
               this.setId_event(EventDao.getId_event());
               this.setSpeaker(_SpeakerServices.getSpeaker(EventDao.getId_speaker()));
               this.setName(EventDao.getName());
               this.setDateStart(EventDao.getDateStart());
               this.setDateEnd(EventDao.getDateEnd());
               this.setDescription(EventDao.getDescription());
               this.setHourStart(EventDao.getHourStart());
               this.setHourEnd(EventDao.getHourEnd());
            }};
            EventDtos.add(EventDto);
        }

        return EventDtos;
    }

}
