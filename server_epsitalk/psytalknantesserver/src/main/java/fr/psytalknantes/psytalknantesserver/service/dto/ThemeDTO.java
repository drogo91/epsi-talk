package fr.psytalknantes.psytalknantesserver.service.dto;

import lombok.Data;

@Data
public class ThemeDTO {

    private int id;
    private String name;

}
