package fr.psytalknantes.psytalknantesserver.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import fr.psytalknantes.psytalknantesserver.service.dto.MaterialDTO;
import fr.psytalknantes.psytalknantesserver.service.MaterialServices;
import java.util.List;

@Api( description="Material class, GET")
@RestController
public class MaterialController {

    MaterialServices _MaterialServices = new MaterialServices();

    @ApiOperation(value = "Get all materials")
    @CrossOrigin(origins = "*" , allowedHeaders = "*")
    @RequestMapping(value="/materials", method= RequestMethod.GET)
    public List<MaterialDTO> getMaterials() {
        return this._MaterialServices.getAllMaterial();
    }

}
