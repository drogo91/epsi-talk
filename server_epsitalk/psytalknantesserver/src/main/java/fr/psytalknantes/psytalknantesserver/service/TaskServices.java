package fr.psytalknantes.psytalknantesserver.service;

import fr.psytalknantes.psytalknantesserver.repository.dao.TaskDAO;
import fr.psytalknantes.psytalknantesserver.repository.TaskRepository;
import fr.psytalknantes.psytalknantesserver.service.converter.TaskConverter;
import fr.psytalknantes.psytalknantesserver.service.dto.TaskDTO;

import java.util.List;

public class TaskServices {

    TaskRepository _TaskRepository = new TaskRepository();
    TaskConverter _TaskConverter = new TaskConverter();

    public List<TaskDTO> getAllTask() {
        List<TaskDAO> TaskDaos = this._TaskRepository.getDAOTask();
        return this._TaskConverter.ConvertTask(TaskDaos);
    }

}
