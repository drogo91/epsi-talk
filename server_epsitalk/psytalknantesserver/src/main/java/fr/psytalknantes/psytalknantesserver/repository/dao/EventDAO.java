package fr.psytalknantes.psytalknantesserver.repository.dao;

import lombok.Data;

@Data
public class EventDAO {

    private int id_event;
    private int id_speaker;
    private String dateStart;
    private String dateEnd;
    private String description;
    private String hourStart;
    private String hourEnd;
    private String name;

}
