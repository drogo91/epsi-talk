package fr.psytalknantes.psytalknantesserver.service;

import fr.psytalknantes.psytalknantesserver.repository.dao.MailingListDAO;
import fr.psytalknantes.psytalknantesserver.repository.MailingListRepository;
import fr.psytalknantes.psytalknantesserver.service.converter.MailingListConverter;
import fr.psytalknantes.psytalknantesserver.service.dto.MailingListDTO;

import java.util.List;

public class MailingListServices {

    MailingListConverter _MailingListConverter = new MailingListConverter();
    MailingListRepository _MailingListRepository = new MailingListRepository();

    public List<MailingListDTO> getAllMailingList() {
        List<MailingListDAO> MailingListDaos = this._MailingListRepository.getDAOMailingList();
        return this._MailingListConverter.ConvertMailingList(MailingListDaos);
    }

}
