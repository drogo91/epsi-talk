package fr.psytalknantes.psytalknantesserver.repository.dao;

import lombok.Data;

@Data
public class ThemeDAO {

    private int id;
    private String name;

}
