package fr.psytalknantes.psytalknantesserver.service.dto;

import lombok.Data;

@Data
public class EventDTO {

    private int id_event;
    private SpeakerDTO speaker;
    private String dateStart;
    private String dateEnd;
    private String description;
    private String hourStart;
    private String hourEnd;
    private String name;

}
