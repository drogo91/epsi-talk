package fr.psytalknantes.psytalknantesserver.service;

import fr.psytalknantes.psytalknantesserver.repository.dao.RoomDAO;
import fr.psytalknantes.psytalknantesserver.repository.RoomRepository;
import fr.psytalknantes.psytalknantesserver.service.converter.RoomConverter;
import fr.psytalknantes.psytalknantesserver.service.dto.RoomDTO;

import java.util.List;

public class RoomServices {

    RoomRepository _RoomRepository = new RoomRepository();
    RoomConverter _RoomConverter = new RoomConverter();

    public List<RoomDTO> getAllRoom() {
        List<RoomDAO> roomDaos = this._RoomRepository.getDAORoom();
        return this._RoomConverter.ConvertRoom(roomDaos);
    }

}
