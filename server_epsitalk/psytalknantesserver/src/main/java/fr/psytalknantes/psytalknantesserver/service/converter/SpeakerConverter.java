package fr.psytalknantes.psytalknantesserver.service.converter;

import java.util.ArrayList;
import java.util.List;
import fr.psytalknantes.psytalknantesserver.repository.dao.SpeakerDAO;
import fr.psytalknantes.psytalknantesserver.service.OrganizationServices;
import fr.psytalknantes.psytalknantesserver.service.dto.SpeakerDTO;

public class SpeakerConverter {

    public List<SpeakerDTO> ConvertSpeaker(List<SpeakerDAO> SpeakerDaos){
        List<SpeakerDTO> SpeakerDtos = new ArrayList<SpeakerDTO>();
        for(final SpeakerDAO SpeakerDao : SpeakerDaos) {
            OrganizationServices _OrganizationServices = new OrganizationServices();
            SpeakerDTO SpeakerDto = new SpeakerDTO(){{
               this.setSurname(SpeakerDao.getSurname());
               this.setId_speaker(SpeakerDao.getId_speaker());
               this.setName(SpeakerDao.getName());
               this.setMail(SpeakerDao.getMail());
               this.setOrganization(_OrganizationServices.getOrganization(SpeakerDao.getId_organization()));
            }};
            SpeakerDtos.add(SpeakerDto);
        }

        return SpeakerDtos;
    }

    public SpeakerDTO ConvertSpeaker(SpeakerDAO SpeakerDao){
        SpeakerDTO speakerDto = new SpeakerDTO(){{
            OrganizationServices _OrganizationServices = new OrganizationServices();
            this.setSurname(SpeakerDao.getSurname());
            this.setName(SpeakerDao.getName());
            this.setMail(SpeakerDao.getMail());
            this.setId_speaker(SpeakerDao.getId_speaker());
            this.setOrganization(_OrganizationServices.getOrganization(SpeakerDao.getId_organization()));
        }};

        return speakerDto;
    }

}
