package fr.psytalknantes.psytalknantesserver.service.converter;

import java.util.ArrayList;
import java.util.List;
import fr.psytalknantes.psytalknantesserver.repository.dao.ExpensesDAO;
import fr.psytalknantes.psytalknantesserver.service.dto.ExpensesDTO;

public class ExpensesConverter {

    public List<ExpensesDTO> ConvertExpenses(List<ExpensesDAO> ExpensesDaos){
        List<ExpensesDTO> ExpensesDtos = new ArrayList<ExpensesDTO>();
        for(final ExpensesDAO ExpensesDao : ExpensesDaos) {
            ExpensesDTO ExpensesDto = new ExpensesDTO(){{
               this.setId(ExpensesDao.getId());
               this.setName(ExpensesDao.getName());
               this.setReceipt(ExpensesDao.getReceipt());
            }};
            ExpensesDtos.add(ExpensesDto);
        }

        return ExpensesDtos;
    }

}
