package fr.psytalknantes.psytalknantesserver.service;

import fr.psytalknantes.psytalknantesserver.repository.dao.UserDAO;
import fr.psytalknantes.psytalknantesserver.service.converter.UserConverter;
import fr.psytalknantes.psytalknantesserver.service.dto.UserDTO;
import fr.psytalknantes.psytalknantesserver.repository.UserRepository;
import java.util.List;

public class UserServices {

    UserRepository _UserRepository = new UserRepository();
    UserConverter _UserConverter = new UserConverter();

    public List<UserDTO> getAllUsers() {
        List<UserDAO> userDaos = this._UserRepository.getDAOUser();
        return this._UserConverter.ConvertUser(userDaos);
    }
}
