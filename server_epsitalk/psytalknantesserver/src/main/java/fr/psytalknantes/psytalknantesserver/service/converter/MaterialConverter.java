package fr.psytalknantes.psytalknantesserver.service.converter;

import java.util.ArrayList;
import java.util.List;
import fr.psytalknantes.psytalknantesserver.repository.dao.MaterialDAO;
import fr.psytalknantes.psytalknantesserver.service.dto.MaterialDTO;

public class MaterialConverter {

    public List<MaterialDTO> ConvertMaterial(List<MaterialDAO> MaterialDaos){
        List<MaterialDTO> MaterialDtos = new ArrayList<MaterialDTO>();
        for(final MaterialDAO MaterialDao : MaterialDaos) {
            MaterialDTO MaterialDto = new MaterialDTO(){{
               this.setId(MaterialDao.getId());
               this.setName(MaterialDao.getName());
            }};
            MaterialDtos.add(MaterialDto);
        }

        return MaterialDtos;
    }

}
