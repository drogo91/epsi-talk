package fr.psytalknantes.psytalknantesserver.service;

import fr.psytalknantes.psytalknantesserver.repository.dao.ThemeDAO;
import fr.psytalknantes.psytalknantesserver.repository.ThemeRepository;
import fr.psytalknantes.psytalknantesserver.service.converter.ThemeConverter;
import fr.psytalknantes.psytalknantesserver.service.dto.ThemeDTO;

import java.util.List;

public class ThemeServices {

    ThemeRepository _ThemeRepository = new ThemeRepository();
    ThemeConverter _ThemeConverter = new ThemeConverter();

    public List<ThemeDTO> getAllTheme() {
        List<ThemeDAO> ThemeDaos = this._ThemeRepository.getDAOTheme();
        return this._ThemeConverter.ConvertTheme(ThemeDaos);
    }

}
