package fr.psytalknantes.psytalknantesserver.service.dto;

import lombok.Data;

@Data
public class ArticleDTO {

    private int id;
    private String title;
    private String containt;

}
