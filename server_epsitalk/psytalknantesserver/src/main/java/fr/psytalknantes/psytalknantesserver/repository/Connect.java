package fr.psytalknantes.psytalknantesserver.repository;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import org.apache.logging.log4j.*;
public class Connect {

    // Here a method to send a void return's query (insert, delete, update)
    public static void voidQuery(final String request) {
        final Logger logger = LogManager.getLogger(Connect.class);
        logger.info("Get connection : on.");
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/epsitalk",
                            "postgres", "epsitalk2019!");
            System.out.println("Opened database successfully");
            stmt = c.createStatement();
            stmt.executeUpdate(request);
            logger.info("Get connection : done.");
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Get connection : failed.");
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
        }

    }

    /*
        And here a method to send a result return's query (select)
        This method return a ResultSet object
    */
    public static ResultSet Query(final String request) {
        final Logger logger = LogManager.getLogger(Connect.class);
        logger.info("Get connection : on.");
        Connection c = null;
        Statement stmt = null;
        try {
            Class.forName("org.postgresql.Driver");
            c = DriverManager
                    .getConnection("jdbc:postgresql://localhost:5432/epsitalk",
                            "postgres", "epsitalk2019!");
            System.out.println("Opened database successfully");
            stmt = c.createStatement();
            ResultSet rs = stmt.executeQuery(request);
            logger.info("Get connection : done.");
            c.close();
            return rs;
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("Get connection : failed.");
            System.err.println(e.getClass().getName() + ": " + e.getMessage());
            System.exit(0);
            return null;
        }
    }

}
