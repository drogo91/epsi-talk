package fr.psytalknantes.psytalknantesserver.repository.dao;

import lombok.Data;

@Data
public class UserDAO {

    private int id;
    private String name;
    private String surname;
    private String password;
    private String mail;
    
}
