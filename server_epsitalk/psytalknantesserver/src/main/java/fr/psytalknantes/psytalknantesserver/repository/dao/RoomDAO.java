package fr.psytalknantes.psytalknantesserver.repository.dao;

import lombok.Data;

@Data
public class RoomDAO {

    private int id;
    private String name;

}
