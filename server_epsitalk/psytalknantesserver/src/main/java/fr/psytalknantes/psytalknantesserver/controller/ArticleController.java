package fr.psytalknantes.psytalknantesserver.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import fr.psytalknantes.psytalknantesserver.service.dto.ArticleDTO;
import fr.psytalknantes.psytalknantesserver.service.ArticleServices;
import java.util.List;

@Api( description="Article class, GET")
@RestController
public class ArticleController {

    ArticleServices _ArticleService = new ArticleServices();

    @ApiOperation(value = "Get all articles")
    @CrossOrigin(origins = "*" , allowedHeaders = "*")
    @RequestMapping(value="/articles", method= RequestMethod.GET)
    public List<ArticleDTO> getArticles() {
        return this._ArticleService.getAllArticle();
    }

}
