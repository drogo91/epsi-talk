package fr.psytalknantes.psytalknantesserver.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import fr.psytalknantes.psytalknantesserver.service.dto.SpeakerDTO;
import fr.psytalknantes.psytalknantesserver.service.SpeakerServices;
import java.util.List;

@Api( description="Speaker class, GET")
@RestController
public class SpeakerController {

    SpeakerServices _SpeakerServices = new SpeakerServices();

    @ApiOperation(value = "Get all speakers")
    @CrossOrigin(origins = "*" , allowedHeaders = "*")
    @RequestMapping(value="/speakers", method= RequestMethod.GET)
    public List<SpeakerDTO> getSpeakers() {
        return this._SpeakerServices.getAllSpeaker();
    }

    @ApiOperation(value = "POST speaker")
    @CrossOrigin(origins = "*" , allowedHeaders = "*")
    @RequestMapping(value="/speakers", method= RequestMethod.POST)
    public void postUser(@RequestBody final String req){
        this._SpeakerServices.postSpeaker(req);
    }

    @ApiOperation(value = "PUT speaker")
    @CrossOrigin(origins = "*" , allowedHeaders = "*")
    @RequestMapping(value="/speakers", method= RequestMethod.PUT)
    public void putUser(@RequestBody final String req){
        this._SpeakerServices.putSpeaker(req);
    }

    @ApiOperation(value = "DELETE event")
    @CrossOrigin(origins = "*" , allowedHeaders = "*")
    @RequestMapping(value="/speakers/{id}", method= RequestMethod.DELETE)
    public void deleteEvent(@PathVariable("id") final int id){
        this._SpeakerServices.deleteSpeaker(id);
    }

}
