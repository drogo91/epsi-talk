package fr.psytalknantes.psytalknantesserver.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import fr.psytalknantes.psytalknantesserver.service.dto.OrganizationDTO;
import fr.psytalknantes.psytalknantesserver.service.OrganizationServices;
import java.util.List;

@Api( description="Organization class, GET")
@RestController
public class OrganizationController {

    OrganizationServices _OrganizationServices = new OrganizationServices();

    @ApiOperation(value = "Get all organizations")
    @CrossOrigin(origins = "*" , allowedHeaders = "*")
    @RequestMapping(value="/organizations", method= RequestMethod.GET)
    public List<OrganizationDTO> getOrganizations() {
        return this._OrganizationServices.getAllOrganization();
    }

}
