package fr.psytalknantes.psytalknantesserver.controller;

import fr.psytalknantes.psytalknantesserver.service.dto.RoomDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import fr.psytalknantes.psytalknantesserver.service.RoomServices;
import java.util.List;

@Api( description="Room class, GET")
@RestController
public class RoomController{

    RoomServices _RoomServices = new RoomServices();

    @ApiOperation(value = "Get all rooms")
    @CrossOrigin(origins = "*" , allowedHeaders = "*")
    @RequestMapping(value="/rooms", method= RequestMethod.GET)
    public List<RoomDTO> getUsers() {
        return this._RoomServices.getAllRoom();
    }

}

