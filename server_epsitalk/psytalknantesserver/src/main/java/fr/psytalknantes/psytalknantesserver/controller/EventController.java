package fr.psytalknantes.psytalknantesserver.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;
import fr.psytalknantes.psytalknantesserver.service.dto.EventDTO;
import fr.psytalknantes.psytalknantesserver.service.EventServices;
import java.util.List;

@Api( description="Event class, GET")
@RestController
public class EventController {

    EventServices _EventServices = new EventServices();

    @ApiOperation(value = "Get all event")
    @CrossOrigin(origins = "*" , allowedHeaders = "*")
    @RequestMapping(value="/events", method= RequestMethod.GET)
    public List<EventDTO> getEvents() {
        return this._EventServices.getAllEvent();
    }

    @ApiOperation(value = "POST event")
    @CrossOrigin(origins = "*" , allowedHeaders = "*")
    @RequestMapping(value="/events", method= RequestMethod.POST)
    public void postEvent(@RequestBody final String req){
        this._EventServices.postEvent(req);
    }

    @ApiOperation(value = "PUT event")
    @CrossOrigin(origins = "*" , allowedHeaders = "*")
    @RequestMapping(value="/events", method= RequestMethod.PUT)
    public void putEvent(@RequestBody final String req){
        this._EventServices.putEvent(req);
    }

    @ApiOperation(value = "DELETE event")
    @CrossOrigin(origins = "*" , allowedHeaders = "*")
    @RequestMapping(value="/events/{id}", method= RequestMethod.DELETE)
    public void deleteEvent(@PathVariable("id") final int id){
        this._EventServices.deleteEvent(id);
    }
}
