package fr.psytalknantes.psytalknantesserver.repository;

import fr.psytalknantes.psytalknantesserver.repository.dao.SpeakerDAO;
import lombok.extern.java.Log;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONArray;
import org.json.JSONObject;
import org.postgresql.util.PSQLException;
import org.apache.logging.log4j.*;

import java.io.Console;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.sql.*;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.List;



public class SpeakerRepository {


    public List<SpeakerDAO> getDAOSpeaker() {

        /*
        * logger is here to get the trace of the query to the database
        */

        final Logger logger = LogManager.getLogger(SpeakerRepository.class);
        logger.info("Get speakers : on.");
        List<SpeakerDAO> SpeakerDaos = new ArrayList<SpeakerDAO>();
        logger.info("Get speakers : ok.");
        /*
            Here we call the Repository to get the Speakers
            The repo return a ResultSet that we interpret to get the data
        */
        try
        {
            final ResultSet rs = Connect.Query("Select * from speaker;");
            while(rs.next()) {
                SpeakerDAO SpeakerDao = new SpeakerDAO(){{
                    this.setId_speaker(rs.getInt("id_Speaker"));
                    this.setName(rs.getString("name"));
                    this.setSurname(rs.getString("surname"));
                    this.setMail(rs.getString("mail"));
                    this.setId_organization(rs.getInt("id_organization"));
                }};
                SpeakerDaos.add(SpeakerDao);

            }

            logger.info("Get speakers : done.");
            return SpeakerDaos;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            logger.info("Check in file SpeakerRepository.java.");
            logger.error("Get speakers : error.");
            return null;
        }
    }

    public SpeakerDAO getDaoSpeaker(int id){

        final Logger logger = LogManager.getLogger(SpeakerRepository.class);
        logger.info("Get speaker : on.");

        SpeakerDAO SpeakerDao = new SpeakerDAO();
        /*
            Here we call the Repository to get the Speakers
            The repo return a ResultSet that we interpret to get the data
        */
        try
        {
            final ResultSet rs = Connect.Query("Select * from speaker where id_speaker=" + id + ";");
            while(rs.next()) {
                SpeakerDao.setId_speaker(rs.getInt("id_Speaker"));
                SpeakerDao.setMail(rs.getString("mail"));
                SpeakerDao.setName(rs.getString("name"));
                SpeakerDao.setSurname(rs.getString("surname"));
                SpeakerDao.setId_organization(rs.getInt("id_organization"));
            };
            logger.info("Get speaker : done.");
            return SpeakerDao;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            logger.error("Get speaker : error.");
            return SpeakerDao;
        }
    }

    /*
        Here we create a JSON from the string request
        and bind the content of the JSON to the correct parameters
     */
    public void postSpeaker(String req){
        JSONObject params = new JSONObject(req);
        JSONObject orga = params.getJSONObject("organization");
        Connect.voidQuery("INSERT INTO public.speaker(" +
                                "id_organization, name, surname, mail)" +
                                "VALUES (" +
                                orga.get("id_organization") + ", '" +
                                params.get("name") + "', '" +
                                params.get("surname") + "', '" +
                                params.get("mail") + "');");
    }

    public void putSpeaker(String req){
        JSONObject params = new JSONObject(req);
        JSONObject orga = params.getJSONObject("organization");
        Connect.voidQuery("UPDATE public.speaker " +
                                "SET id_organization= " + orga.get("id_organization") +
                                ", name='" + params.get("name") +
                                "', surname='" + params.get("surname") + "" +
                                "', mail='" + params.get("mail") + "' " +
                                "WHERE id_speaker=" + params.get("id_speaker") + ";");
    }

    public void deleteSpeaker(int id){
        Connect.voidQuery("DELETE FROM public.speaker " +
                                "WHERE id_speaker=" + id + ";");
    }

}

