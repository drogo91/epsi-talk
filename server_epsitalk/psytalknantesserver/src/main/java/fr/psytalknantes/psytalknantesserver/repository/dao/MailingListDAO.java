package fr.psytalknantes.psytalknantesserver.repository.dao;

import lombok.Data;

@Data
public class MailingListDAO {

    private int id;
    private String title;

}
