package fr.psytalknantes.psytalknantesserver.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import fr.psytalknantes.psytalknantesserver.service.dto.ExpensesDTO;
import fr.psytalknantes.psytalknantesserver.service.ExpensesServices;
import java.util.List;

@Api( description="Expenses class, GET")
@RestController
public class ExpensesController {

    ExpensesServices _ExpensesServices = new ExpensesServices();

    @ApiOperation(value = "Get all expenses")
    @CrossOrigin(origins = "*" , allowedHeaders = "*")
    @RequestMapping(value="/expenses", method= RequestMethod.GET)
    public List<ExpensesDTO> getExpensess() {
        return this._ExpensesServices.getAllExpenses();
    }

}
