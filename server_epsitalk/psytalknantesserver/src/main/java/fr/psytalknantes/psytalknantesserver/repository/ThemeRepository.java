package fr.psytalknantes.psytalknantesserver.repository;

import fr.psytalknantes.psytalknantesserver.repository.dao.ThemeDAO;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import org.apache.logging.log4j.*;
public class ThemeRepository {

    public List<ThemeDAO> getDAOTheme() {
        final Logger logger = LogManager.getLogger(ThemeRepository.class);
        logger.info("Get themes : on.");
        List<ThemeDAO> ThemeDaos = new ArrayList<ThemeDAO>();

        /*
            Here we call the Repository to get the Themes
            The repo return a ResultSet that we interpret to get the data
        */
        try
        {
            final ResultSet rs = Connect.Query("Select * from theme;");
            while(rs.next()) {
                ThemeDAO ThemeDao = new ThemeDAO(){{
                    this.setId(rs.getInt("id_theme"));
                    this.setName(rs.getString("name"));
                }};
                ThemeDaos.add(ThemeDao);

            }
            logger.info("Get themes : done.");
            return ThemeDaos;
        }
        catch (SQLException e)
        {
            e.printStackTrace();
            logger.error("Get themes : failed.");
            return null;
        }
    }

}

