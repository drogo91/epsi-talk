package fr.psytalknantes.psytalknantesserver.repository.dao;

import lombok.Data;

@Data
public class SpeakerDAO {

    private int id_speaker;
    private String name;
    private String surname;
    private String mail;
    private int id_organization;
}
