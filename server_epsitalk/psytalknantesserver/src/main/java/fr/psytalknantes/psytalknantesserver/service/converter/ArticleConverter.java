package fr.psytalknantes.psytalknantesserver.service.converter;

import java.util.ArrayList;
import java.util.List;
import fr.psytalknantes.psytalknantesserver.repository.dao.ArticleDAO;
import fr.psytalknantes.psytalknantesserver.service.dto.ArticleDTO;

public class ArticleConverter {

    public List<ArticleDTO> ConvertArticle(List<ArticleDAO> ArticleDaos){
        List<ArticleDTO> ArticleDtos = new ArrayList<ArticleDTO>();
        for(final ArticleDAO ArticleDao : ArticleDaos) {
            ArticleDTO ArticleDto = new ArticleDTO(){{
               this.setContaint(ArticleDao.getContaint());
               this.setId(ArticleDao.getId());
               this.setTitle(ArticleDao.getTitle());
            }};
            ArticleDtos.add(ArticleDto);
        }

        return ArticleDtos;
    }

}
