package fr.psytalknantes.psytalknantesserver.repository.dao;

import lombok.Data;

@Data
public class OrganizationDAO {

    private int id_organization;
    private String sponsor;
    private int effective;
    private String name;
    private String mail;

}
