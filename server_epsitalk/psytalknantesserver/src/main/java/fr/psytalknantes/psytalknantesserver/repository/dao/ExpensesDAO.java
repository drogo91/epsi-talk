package fr.psytalknantes.psytalknantesserver.repository.dao;

import lombok.Data;

@Data
public class ExpensesDAO {

    private int id;
    private String name;
    private String receipt;

}
