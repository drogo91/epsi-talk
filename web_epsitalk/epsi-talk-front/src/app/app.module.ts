import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/public/home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { forOwn } from 'lodash';
import {
  MatButtonModule,
  MatGridListModule,
  MatButtonToggleModule,
  MatFormFieldModule,
  MatRadioModule,
  MatInputModule,
  MatDatepickerModule,
  MatOptionModule,
  MatIconModule,
  MatSidenavModule,
  MatSortModule,
  MatTableModule,
  MatTabsModule,
  MatSelectModule,
  MatNativeDateModule,
  MatToolbarModule,
  MatPaginatorModule,
  MatSnackBarModule
} from '@angular/material';
import { SidebarComponent } from './shared/sidebar/sidebar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatMenuModule } from '@angular/material/menu';
import { EventsComponent } from './pages/private/events/events.component';
import { PlanningComponent } from './pages/private/planning/planning.component';
import { CommonModule } from '@angular/common';
import { SpeakersComponent } from './pages/private/speakers/speakers.component';
import localeFr from '@angular/common/locales/fr';
import { registerLocaleData} from '@angular/common';
import { EmailRegexCheckerDirective } from './directives/email-regex-directive/email-regex-checker.directive';
registerLocaleData(localeFr);

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    NavbarComponent,
    SidebarComponent,
    EventsComponent,
    SpeakersComponent,
    PlanningComponent,
    EmailRegexCheckerDirective,
  ],
  imports: [
    CommonModule,
    MatGridListModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonToggleModule,
    BrowserModule,
    MatToolbarModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatOptionModule,
    FormsModule,
    ReactiveFormsModule,
    MatInputModule,
    MatMenuModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatRadioModule,
    MatIconModule,
    MatSidenavModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatSelectModule,
    BrowserAnimationsModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory
    }),
    MatPaginatorModule,
    MatSnackBarModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
