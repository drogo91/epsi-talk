import { Directive } from '@angular/core';
import { Validator,  NG_VALIDATORS } from '@angular/forms';

// noinspection JSAnnotator
@Directive({
  selector: '[appEmailRegexChecker]',
  providers: [{provide: NG_VALIDATORS, useExisting: EmailRegexCheckerDirective, multi: true}]
})
export class EmailRegexCheckerDirective {
/*
  emailPattern = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/) ;

  validate(c: Control): {[key: string]: any} {
    if(c.value == null)
      return null;

    if(!this.emailPattern.test(c.value)){
      return {"pattern": true};
    }
    return null;
  }*/
}
