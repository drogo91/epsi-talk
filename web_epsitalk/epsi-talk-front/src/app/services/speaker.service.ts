import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Speaker } from '../model/speaker';

@Injectable({
  providedIn: 'root'
})
export class SpeakerService {

  constructor(private _httpClient: HttpClient) { }

  private readonly _url = 'http://localhost:8080/speakers';

  public getSpeakers(): Observable<Speaker[]> {
    return this._httpClient.get<Speaker[]>(this._url);
  }

  public addSpeaker(speaker: Speaker): void {
    this._httpClient.post(this._url, JSON.stringify({
      name: speaker.name,
      surname: speaker.surname,
      mail: speaker.mail,
      organization: speaker.organization
    }))
      .subscribe (
        res => {
          console.log (res);
        },
        err => {
          console.log ('Une erreur s\'est produite');
        }
      );
  }

  public updateSpeaker(speaker: Speaker): void {
    const req = this._httpClient.put(this._url, JSON.stringify({
      id_speaker: speaker.id_speaker,
      name: speaker.name,
      surname: speaker.surname,
      mail: speaker.mail,
      organization: speaker.organization
    })).subscribe (
      res => {
        console.log (res);
      },
      err => {
        console.log ('Une erreur s\'est produite');
      }
    );
  }

  public deleteSpeaker(id: number): void {
    const dUrl = this._url + '/' + id;
    const req = this._httpClient.delete(dUrl).subscribe (
      res => {
        console.log (res);
      },
      err => {
        console.log ('Une erreur s\'est produite');
      }
    );
  }
}
