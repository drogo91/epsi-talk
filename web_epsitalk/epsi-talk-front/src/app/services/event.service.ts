import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Event } from '../model/event';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  constructor(private _httpClient: HttpClient) { }

  private readonly _url = 'http://localhost:8080/events';

  public getEvents(): Observable<Event[]> {
    return this._httpClient.get<Event[]>(this._url);
  }

  public addEvent(event: Event): void {
    if (event.speaker != null){
      this._httpClient.post(this._url, JSON.stringify({
        dateStart: event.dateStart,
        speaker: event.speaker,
        name: event.name,
        description: event.description,
        hourStart: event.hourStart,
        hourEnd: event.hourEnd
      }))
        .subscribe (
          res => {
              console.log (res);
              },
          err => {
            console.log ('Une erreur s\'est produite');
          }
          );
    } else {
      this._httpClient.post(this._url, JSON.stringify({
        dateStart: event.dateStart,
        dateEnd: event.dateEnd,
        name: event.name,
        description: event.description,
        hourStart: event.hourStart,
        hourEnd: event.hourEnd
      }))
        .subscribe (
          res => {
            console.log (res);
          },
          err => {
            console.log ('Une erreur s\'est produite');
          }
        );
    }
  }

  public updateEvent(event: Event): void {
    this._httpClient.put(this._url, JSON.stringify({
      id_event: event.id_event,
      dateStart: event.dateStart,
      dateEnd: event.dateEnd,
      id_speaker: event.speaker,
      name: event.name,
      description: event.description,
      hourStart: event.hourStart,
      hourEnd: event.hourEnd
    })).subscribe (
      res => {
        console.log (res);
      },
      err => {
        console.log ('Une erreur s\'est produite');
      }
    );
  }

  public deleteEvent(id: number): void {
    const dUrl = this._url + '/' + id;
    this._httpClient.delete(dUrl).subscribe (
      res => {
        console.log (res);
      },
      err => {
        console.log ('Une erreur s\'est produite');
      }
    );
  }
}
