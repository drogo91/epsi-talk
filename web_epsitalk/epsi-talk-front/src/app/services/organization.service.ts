import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Observable} from 'rxjs';
import {Organization} from '../model/organization';

@Injectable({
  providedIn: 'root'
})
export class OrganizationService {

  constructor(private _httpClient: HttpClient) { }

  private readonly _url = 'http://localhost:8080/organizations';

  public getOrganizations(): Observable<Organization[]> {
    return this._httpClient.get<Organization[]>(this._url);
  }

}
