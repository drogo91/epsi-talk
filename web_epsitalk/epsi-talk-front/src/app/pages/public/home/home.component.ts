import { Component } from '@angular/core';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css', '../../../shared/sidebar/sidebar.component.css'],
  })
export class HomeComponent {
  constructor() { }
}
