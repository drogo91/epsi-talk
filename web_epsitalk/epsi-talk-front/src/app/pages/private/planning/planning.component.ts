import {
  CalendarEvent, CalendarView
} from 'angular-calendar';
import {
  Component,
  OnInit,
  ChangeDetectionStrategy, ViewChild,
} from '@angular/core';
import { MatIconRegistry, MatTable, MatTableDataSource, MatPaginator } from '@angular/material';
import { DomSanitizer } from '@angular/platform-browser';
import { EventService } from '../../../services/event.service';
import { Subject } from 'rxjs';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  }
};
let calendarcolors: any = colors.blue;

@Component({
  selector: 'app-planning',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './planning.component.html',
  styleUrls: ['./planning.component.css', '../../../shared/sidebar/sidebar.component.css']
})

export class PlanningComponent implements OnInit {
  view: CalendarView = CalendarView.Month;
  displayedColumns: string[] = ['title', 'start', 'hourStart', 'end', 'hourEnd'];
  viewDate: Date = new Date();
  locale: String = 'fr';
  callEvents: CalendarEvent[] = [];
  dataSource = new MatTableDataSource<Event>();
  refresh: Subject<any> = new Subject<any>();

  constructor(iconRegistry: MatIconRegistry, sanitizer: DomSanitizer, private _EventService: EventService) {}

  @ViewChild(MatTable) table: MatTable<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  public ngOnInit(): void {
    this.setEvents();
    this.dataSource.paginator = this.paginator;
  }

  public setEvents(): void {
    this._EventService.getEvents()
      .subscribe(events => {
        // @ts-ignore
        this.dataSource.data = events;
        events.forEach(event => {
          const SplitedDateStart: string[] = event.dateStart.split('-');
          const SplitedHourStart: string[] = event.hourStart.split(':');
          const SplitedHourEnd: string[] = event.hourEnd.split(':');
          let dateEnd;
          if (event.dateEnd) {
            const SplitedDateEnd: string[] = event.dateEnd.split('-');
            dateEnd = new Date(+SplitedDateEnd[0], +SplitedDateEnd[1] - 1, +SplitedDateEnd[2],
              +SplitedHourEnd[0] + 1, +SplitedHourEnd[1]);
          } else {
            dateEnd = new Date(+SplitedDateStart[0], +SplitedDateStart[1] - 1, +SplitedDateStart[2],
              +SplitedHourStart[0] + 1, +SplitedHourStart[1]);
          }
          if(event.dateEnd === null){
            calendarcolors = colors.red;
          } else {
            calendarcolors = colors.blue;
          }
          const newCallEvent: CalendarEvent = {
            start: new Date(+SplitedDateStart[0], +SplitedDateStart[1] - 1, +SplitedDateStart[2],
              +SplitedHourStart[0] + 1, +SplitedHourStart[1]),
            end: dateEnd,
            title: event.name,
            color: calendarcolors,
            resizable: {
              beforeStart: true,
              afterEnd: true
            },
            draggable: true
          } as CalendarEvent;
          this.callEvents.push(newCallEvent);
          this.refresh.next();
          this.table.renderRows();
        });
      });
  }
}
