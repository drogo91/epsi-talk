import { Component, OnInit, ViewChild } from '@angular/core';
import { SpeakerService } from '../../../services/speaker.service';
import * as _ from 'lodash';
import { Speaker } from '../../../model/speaker';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Organization } from '../../../model/organization';
import { OrganizationService } from '../../../services/organization.service';
import { MatPaginator } from '@angular/material';

@Component({
  selector: 'app-speakers',
  templateUrl: './speakers.component.html',
  styleUrls: ['./speakers.component.css', '../../../shared/sidebar/sidebar.component.css']
})
export class SpeakersComponent implements OnInit {
  editingSpeaker: Speaker;
  saveSpeaker: Speaker;
  organizations: Organization[] = [];
  selectedOrganization: Organization;
  displayedColumns: string[] = ['name', 'surname', 'mail', 'organization', 'delete'];
  mailRegExp = new RegExp(/^(([^<>()\\[\\]\\\\.,;:\\s@"]+(\\.[^<>()\\[\\]\\\\.,;:\\s@\"]+)*)|(\".+\"))@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}])|(([a-zA-Z-0-9]+\\.)+[a-zA-Z]{2,}))$/);
  dataSource = new MatTableDataSource<Speaker>();
  tableMailControl = new FormControl('', [
    Validators.required,
    Validators.email,
    Validators.maxLength(300)
  ]);
  tableNameControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(100)
  ]);
  tableSurnameControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(100)
  ]);
  speakerForm = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.maxLength(100)]),
    surname: new FormControl('', [Validators.required, Validators.maxLength(100)]),
    mail: new FormControl('', [Validators.required, Validators.email, Validators.maxLength(300)]),
    organization: new FormControl('', [Validators.required])
  });

  constructor(private _SpeakerService: SpeakerService, private _OrganizationService: OrganizationService) { }

  // Used to update dynamically the table
  @ViewChild(MatTable) table: MatTable<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  public hasError = (controlName: string, errorName: string) => {
    return this.speakerForm.controls[controlName].hasError(errorName);
  }

  public ngOnInit(): void {
    this.getSpeakers();
    this.dataSource.paginator = this.paginator;
    this.getOrganizations();
  }

  public updateOrganization(): void {
    this.editingSpeaker.organization = this.selectedOrganization;
  }

  public updateSpeaker(): void {
    if (this.editingSpeaker) {
      if (this.inputIsValid() === true) {
        this._SpeakerService.updateSpeaker(this.editingSpeaker);
      } else {
        const spe = this.dataSource.data.find( speaker => speaker.id_speaker === this.editingSpeaker.id_speaker);
        const index = this.dataSource.data.indexOf(spe);
        this.dataSource.data[index] = this.saveSpeaker;
      }
      this.table.renderRows();
      this.editingSpeaker = undefined;
    }
  }

  public editSpeaker(speaker: Speaker): void {
    // set this event in the editEvent
    this.editingSpeaker = speaker;
    this.saveSpeaker = {
      id_speaker: speaker.id_speaker,
      mail: speaker.mail,
      surname: speaker.surname,
      name: speaker.name,
      organization: speaker.organization
    } as Speaker;
  }

  public getSpeakers(): void {
    this._SpeakerService.getSpeakers()
      .subscribe(speaker => this.dataSource.data = speaker);
  }

  public getOrganizations(): void {
    this._OrganizationService.getOrganizations()
      .subscribe(organization => this.organizations = organization);
  }

  public addSpeaker(): void {
    if (this.speakerForm.valid) {
      const form = this.speakerForm.value;

      // Find the speaker chosen by the user
      const TheOrganization = this.organizations.find(organization => organization.id_organization = form.organization);

      const newSpeaker: Speaker = {
        name: form.name,
        mail: form.mail,
        surname: form.surname,
        organization: TheOrganization
      } as Speaker;
      this.speakerForm.reset();
      Object.keys(this.speakerForm.controls).forEach(key => {
        this.speakerForm.controls[key].setErrors(null);
      });
      this._SpeakerService.addSpeaker(newSpeaker);
      this.dataSource.data.push(newSpeaker);
      this.table.renderRows();
    }

  }

  public deleteSpeaker(speaker: Speaker): void {
    this._SpeakerService.deleteSpeaker(speaker.id_speaker);
    const index = this.dataSource.data.indexOf(speaker, 0);
    this.dataSource.data.splice(index, 1);
    this.table.renderRows();
  }

  private inputIsValid(): boolean {
    const inputStatus: string[] = [
      this.tableNameControl.status,
      this.tableSurnameControl.status,
      this.tableMailControl.status,
    ];
    return _.includes(inputStatus, 'INVALID') === false;
  }
}
