import { Component, OnInit, ViewChild } from '@angular/core';
import { EventService } from '../../../services/event.service';
import { SpeakerService } from '../../../services/speaker.service';
import { Event } from '../../../model/event';
import { Speaker } from '../../../model/speaker';
import * as _ from 'lodash';
import * as moment from 'moment';
import { MatTableDataSource, MatTable } from '@angular/material/table';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { DateAdapter, MatRadioChange, MatPaginator, MatSnackBar } from '@angular/material';


@Component({
  selector: 'app-event',
  templateUrl: './events.component.html',
  providers: [EventService],
  styleUrls: ['./events.component.css', '../../../shared/sidebar/sidebar.component.css']
})
export class EventsComponent implements OnInit {
  today: Date = moment().toDate();
  todaybutlater : number = moment().toDate().setHours(this.today.getHours()+1);

  editingEvent: Event;
  editingStartHour: number;
  editingStartMin: number;
  editingEndHour: number;
  editingEndMin: number;
  speakers: Speaker[] = [];
  selectedSpeaker: Speaker;
  displayedColumns: string[] = ['name', 'description', 'dateStart', 'hourStart', 'dateEnd', 'hourEnd', 'speaker', 'delete','update'];
  dataSource = new MatTableDataSource<Event>();
  eventFormHidden = true;
  salonFormHidden = true;
  saveEvent: Event;
  eventForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
    date: new FormControl('', [Validators.required]),
    hourStart: new FormControl('', [Validators.required]),
    minStart: new FormControl('', [Validators.required]),
    hourEnd: new FormControl('', [Validators.required]),
    minEnd: new FormControl('', [Validators.required]),
    speaker: new FormControl('', [Validators.required])
  });
  salonForm = new FormGroup({
    name: new FormControl('', [Validators.required]),
    description: new FormControl('', [Validators.required]),
    dateStart: new FormControl('', [Validators.required]),
    hourStart: new FormControl('', [Validators.required]),
    minStart: new FormControl('', [Validators.required]),
    dateEnd: new FormControl('', [Validators.required]),
    hourEnd: new FormControl('', [Validators.required]),
    minEnd: new FormControl('', [Validators.required])
  });
  tableNameControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(100)
  ]);
  tableDescControl = new FormControl('', [
    Validators.required,
    Validators.maxLength(100)
  ]);
  tableDateStartControl = new FormControl('', [
    Validators.required
  ]);
  tableHourStartControl = new FormControl('', [
    Validators.required
  ]);
  tableMinStartControl = new FormControl('', [
    Validators.required
  ]);
  tableHourEndControl = new FormControl('', [
    Validators.required
  ]);
  tableMinEndControl = new FormControl('', [
    Validators.required
  ]);



  constructor(private snackBar: MatSnackBar, private _EventService: EventService, private _SpeakerService: SpeakerService, private adapter: DateAdapter<any>) {
  }

  // Used to update dynamically the table
  @ViewChild(MatTable) table: MatTable<any>;
  @ViewChild(MatPaginator) paginator: MatPaginator;

  public ngOnInit(): void {
    this.getEvents();
    this.dataSource.paginator = this.paginator;
    this.getSpeakers();
    this.adapter.setLocale('fr');
    this.initHourInput();
  }

  public openSnackBar(message: string, action: string, event) : boolean{
    let snackBarRef = this.snackBar.open(message, action, {
      duration: 20000,
    });
    snackBarRef.onAction().subscribe(() => {
      this.deleteEvent(event);
      return true;
    });
    return false;
  }

  public initHourInput(): void {
    this.eventForm.controls['hourStart'].patchValue(this.today.getHours());
    this.eventForm.controls['minStart'].patchValue(_.round(this.today.getMinutes(), -1));
    this.eventForm.controls['hourEnd'].patchValue(this.today.getHours() + 1);
    this.eventForm.controls['minEnd'].patchValue(_.round(this.today.getMinutes() + 1, -1));
    this.salonForm.controls['hourStart'].patchValue(this.today.getHours());
    this.salonForm.controls['minStart'].patchValue(_.round(this.today.getMinutes(), -1));
    this.salonForm.controls['hourEnd'].patchValue(this.today.getHours() + 1);
    this.salonForm.controls['minEnd'].patchValue(_.round(this.today.getMinutes() + 1, -1));
  }


  public hasErrorSalon = (controlName: string, errorName: string) => {
    return this.salonForm.controls[controlName].hasError(errorName);
  }

  public hasErrorEvent = (controlName: string, errorName: string) => {
    return this.eventForm.controls[controlName].hasError(errorName);
  }

  public selectForm(type: MatRadioChange): void {
    if (type.value === 'conf') {
      this.eventFormHidden = false;
      this.salonFormHidden = true;
    } else {
      this.eventFormHidden = true;
      this.salonFormHidden = false;
    }
  }

  public updateSpeaker(): void {
    this.editingEvent.speaker = this.selectedSpeaker;
  }

  public updateHour(type, operator): void {
    if (type === 'tableHourStart') {
      if (operator === '+') {
        this.editingStartHour++;
      } else {
        this.editingStartHour--;
      }
    }
    if (type === 'tableHourEnd' && this.editingStartHour < this.editingEndHour) {
      if (operator === '+') {
        this.editingEndHour++;
      } else {
        this.editingEndHour--;
      }
    }
    if (type === 'tableMinStart' && 0 <= this.editingStartMin && this.editingStartMin <= 44) {
      if (operator === '+') {
        this.editingStartMin = this.editingStartMin + 10;
      } else {
        this.editingStartMin = this.editingStartMin - 10;
      }
    }
    if (type === 'tableMinEnd' && 0 <= this.editingEndMin && this.editingStartMin <= 44) {
      if (operator === '+') {
        this.editingEndMin = this.editingEndMin + 10;
      } else {
        this.editingEndMin = this.editingEndMin - 10;
      }
    }
  }

  public updateEvent(): void {
    if (this.editingEvent) {
      if (this.inputIsValid() === true) {
        let editedEvent = this.editingEvent;
        editedEvent.hourStart = this.convertHour(this.editingStartHour.toString(), this.editingStartMin.toString());
        editedEvent.hourEnd = this.convertHour(this.editingEndHour.toString(), this.editingEndMin.toString());
        if (this.selectedSpeaker !== null) {
          editedEvent.speaker = this.selectedSpeaker;
        }
        if (typeof editedEvent.dateStart === 'object') {
          editedEvent.dateStart = this.convertDate(editedEvent.dateStart);
        }
        if (editedEvent.dateEnd !== undefined && editedEvent.dateEnd === 'object') {
          editedEvent.dateEnd = this.convertDate(editedEvent.dateEnd);
        } else {
          editedEvent.dateEnd = null;
        }
        this._EventService.updateEvent(this.editingEvent);
      } else {
        const spe = this.dataSource.data.find(event => event.id_event === this.editingEvent.id_event);
        const index = this.dataSource.data.indexOf(spe);
        this.dataSource.data[index] = this.saveEvent;
      }
      this.table.renderRows();
      this.editingEvent = undefined;
    }
  }

  public editEvent(event: Event): void {
    // set this event in the editEvent
    this.selectedSpeaker = event.speaker;
    this.editingEvent = event;
    const splitedHourStart = event.hourStart.split(':');
    const splitedHourEnd = event.hourEnd.split(':');
    this.editingStartHour = parseInt(splitedHourStart[0], 10);
    this.editingStartMin = parseInt(splitedHourStart[1], 10);
    this.editingEndHour = parseInt(splitedHourEnd[0], 10);
    this.editingEndMin = parseInt(splitedHourEnd[1], 10);
    this.saveEvent = {
      id_event: event.id_event,
      name: event.name,
      description: event.description,
      hourStart: event.hourStart,
      hourEnd: event.hourEnd,
      dateStart: event.dateStart,
      dateEnd: event.dateEnd
    } as Event;
  }

  public getEvents(): void {
    this._EventService.getEvents()
      .subscribe(events => this.dataSource.data = events);
  }

  public modifyHour(hour, operator): void {
    if (operator === '-') {
      if (parseInt(hour.min, 10) < parseInt(hour.value, 10)) {
        hour.value = parseInt(hour.value, 10) - 1;
        if (hour.form != null) {
          if (hour.form.id === 'eventForm') {
            this.eventForm.controls[hour.attributes.formControlName.value].patchValue(hour.value);
          } else {
            this.salonForm.controls[hour.attributes.formControlName.value].patchValue(hour.value);
          }
        }
      }
    } else {
      if (parseInt(hour.max, 10) > parseInt(hour.value, 10)) {
        hour.value = parseInt(hour.value, 10) + 1;
        if (hour.form != null) {
          if (hour.form && hour.form.id === 'eventForm') {
            this.eventForm.controls[hour.attributes.formControlName.value].patchValue(hour.value);
          } else if (hour.form.id === 'salonForm') {
            this.salonForm.controls[hour.attributes.formControlName.value].patchValue(hour.value);
          }
        }
      }
    }
  }

  public modifyMin(hour, operator): void {
    if (operator === '-') {
      if (parseInt(hour.min, 10) < parseInt(hour.value, 10)) {
        hour.value = parseInt(hour.value, 10) - 10;
        if (hour.form != null) {
          if (hour.form.id === 'eventForm') {
            this.eventForm.controls[hour.attributes.formControlName.value].patchValue(hour.value);
          } else {
            this.salonForm.controls[hour.attributes.formControlName.value].patchValue(hour.value);
          }
        }
      }
    } else {
      if (parseInt(hour.max, 10) > parseInt(hour.value, 10)) {
        hour.value = parseInt(hour.value, 10) + 10;
        if (hour.form != null) {
          if (hour.form && hour.form.id === 'eventForm') {
            this.eventForm.controls[hour.attributes.formControlName.value].patchValue(hour.value);
          } else if (hour.form.id === 'salonForm') {
            this.salonForm.controls[hour.attributes.formControlName.value].patchValue(hour.value);
          }
        }
      }
    }
  }

  public getSpeakers(): void {
    this._SpeakerService.getSpeakers()
      .subscribe(speaker => this.speakers = speaker);
  }

  public addEvent(): void {
    if (this.eventForm.valid) {
      const form = this.eventForm.value;
      const TheSpeaker = this.speakers.find(speaker => speaker.id_speaker = form.speaker);
      const newEvent: Event = {
        name: form.name,
        description: form.description,
        dateStart: this.convertDate(form.date),
        dateEnd: null,
        hourStart: this.convertHour(form.hourStart, form.minStart),
        hourEnd: this.convertHour(form.hourEnd, form.minEnd),
        speaker: TheSpeaker
      } as Event;
      this.eventForm.reset();
      Object.keys(this.eventForm.controls).forEach(key => {
        this.eventForm.controls[key].setErrors(null);
      });
      this._EventService.addEvent(newEvent);
      this.selectedSpeaker = null;
      this.dataSource.data.push(newEvent);
      this.table.renderRows();
      this.ngOnInit()
    }
  }

  public addSalon(): void {
    if (this.salonForm.valid) {
      const form = this.salonForm.value;
      const newEvent: Event = {
        name: form.name,
        description: form.description,
        dateStart: this.convertDate(form.dateStart),
        hourStart: this.convertHour(form.hourStart, form.minStart),
        dateEnd: this.convertDate(form.dateEnd),
        hourEnd: this.convertHour(form.hourEnd, form.minEnd),
      } as Event;
      this.salonForm.reset();
      Object.keys(this.salonForm.controls).forEach(key => {
        this.salonForm.controls[key].setErrors(null);
      });
      this._EventService.addEvent(newEvent);
      this.dataSource.data.push(newEvent);
      this.table.renderRows();
      this.ngOnInit()
    }
  }


  public deleteEvent(event: Event): void {
    this._EventService.deleteEvent(event.id_event);
    const index = this.dataSource.data.indexOf(event, 0);
    this.dataSource.data.splice(index, 1);
    this.table.renderRows();
    this.ngOnInit();
  }


  private inputIsValid(): boolean {
    const inputStatus: string[] = [
      this.tableDescControl.status,
      this.tableNameControl.status,
      this.tableDateStartControl.status,
      this.tableHourStartControl.status,
      this.tableMinStartControl.status,
      this.tableHourEndControl.status,
      this.tableMinEndControl.status
    ];
    return _.includes(inputStatus, 'INVALID') === false;
  }



  public convertDate(date: Date): string {
    let month: string = String(date.getMonth() + 1);
    let day: string = String(date.getDate());
    if (parseInt(month, 10) < 10) {
      month = '0' + month;
    }
    if (parseInt(day, 10) < 10) {
      day = '0' + day;
    }
    return date.getFullYear() +
      '-' + month +
      '-' + day;
  }

  public convertHour(hour: string, min: string): string {
    if (parseInt(hour, 10) < 10) {
      hour = '0' + hour;
    }
    if (parseInt(min, 10) < 10) {
      min = '0' + min;
    }
    return hour + ':' + min + ':' + '00';
  }

}
