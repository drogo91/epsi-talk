import { Component} from '@angular/core';
import { Router } from '@angular/router';

/**
 * @SidebarComponent
 * Only one parameter / zero constructor
 * This component is always used for the private website
 * The value showFiller (put on false) correspond to the display of an element linked in the html file
 */

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent {
  constructor(private router: Router) { }

  public naviguate(path: string): void {
    this.router.navigateByUrl(path);
  }
}
