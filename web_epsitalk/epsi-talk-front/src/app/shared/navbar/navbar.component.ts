import { Component } from '@angular/core';

/**
 * @NavbarComponent
 * Only one parameter / zero constructor
 * This component is always used for the public website
 * This component display the header as a showcase
 * It doesn't contain the background picture of the public site, it only get the logo, the navbar and the searchbar.
 */

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent {
  constructor() {}
}
