import { Organization } from './organization';

export interface Speaker {
  id_speaker: number;
  organization:  Organization;
  name: string;
  surname: string;
  mail: string;
}
