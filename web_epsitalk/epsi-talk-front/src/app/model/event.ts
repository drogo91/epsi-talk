import { Speaker } from './speaker';

export interface Event {
  id_event: number;
  name: string;
  description: string;
  dateStart;
  dateEnd;
  hourStart: string;
  hourEnd: string;
  speaker: Speaker;
}
