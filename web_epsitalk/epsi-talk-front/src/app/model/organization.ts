export interface Organization {
  id_organization: number;
  name: string;
  surname: string;
  mail: string;
}
