import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './pages/public/home/home.component';
import { EventsComponent } from './pages/private/events/events.component';
import { PlanningComponent } from './pages/private/planning/planning.component';
import { SpeakersComponent } from './pages/private/speakers/speakers.component';
import { SidebarComponent } from './shared/sidebar/sidebar.component';

const routes: Routes = [
  { path: 'private', component: HomeComponent },
  { path: 'test', component: SidebarComponent },
  { path: 'private/events', component: EventsComponent },
  { path: 'private/calendar', component: PlanningComponent },
  { path: 'private/speakers', component: SpeakersComponent }
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }


